use std::collections::{HashMap, HashSet};

#[derive(Hash, PartialEq, Eq, Clone, Copy)]
struct Point {
    x: isize,
    y: isize,
}

impl Point {
    fn from(i: usize, j: usize) -> Self {
        Self {
            x: i as isize,
            y: j as isize,
        }
    }
}

impl std::ops::Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl std::fmt::Debug for Point {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(f, "({}, {})", self.x, self.y)
    }
}

const AROUND: [Point; 4] = [
    Point { x: 1, y: 0 },
    Point { x: 0, y: 1 },
    Point { x: -1, y: 0 },
    Point { x: 0, y: -1 },
];
const DIAG: [Point; 4] = [
    Point { x: 1, y: 1 },
    Point { x: -1, y: 1 },
    Point { x: -1, y: -1 },
    Point { x: 1, y: -1 },
];

type Region = HashSet<Point>;

pub struct Garden {
    plots: Vec<Region>,
}

fn get_plot_price(plot: &Region) -> usize {
    let area = plot.len();
    let mut perimeter = 0;
    for p in plot {
        let r = AROUND
            .into_iter()
            .filter(|a| !plot.contains(&(*p + *a)))
            .count();
        perimeter += r;
    }
    area * perimeter
}

fn count_corners(p: &Point, plot: &Region) -> usize {
    DIAG.into_iter()
        .filter(|a| !plot.contains(&(*p + *a)))
        .count()
}

fn get_bulk_price(plot: &Region) -> usize {
    let area = plot.len();
    let mut corners = 0;
    for p in plot {
        let r = AROUND
            .into_iter()
            .filter(|a| !plot.contains(&(*p + *a)))
            .count();
        match r {
            0 => corners += count_corners(p, &plot),
            3 => corners += 2,
            4 => corners += 4,
            c if c > 5 => panic!("found too many neighbors"),
            _ => todo!(),
        }
    }
    area * corners
}

fn find_disjoint_regions(plot: &mut Region) -> Vec<Region> {
    let mut res = Vec::new();
    let mut next = plot.iter().cloned().next(); // get one element from set
    while let Some(p) = next {
        let mut r = Region::from([p]);
        plot.remove(&p);
        // find all neighbors in set
        let mut conn = Vec::from_iter(AROUND.into_iter().filter_map(|a| {
            let n = p + a;
            if plot.contains(&n) {
                Some(n)
            } else {
                None
            }
        }));
        while let Some(c) = conn.pop() {
            r.insert(c);
            plot.remove(&c);
            let mut other = Vec::from_iter(AROUND.into_iter().filter_map(|a| {
                let n = c + a;
                if plot.contains(&n) && !r.contains(&n) {
                    Some(n)
                } else {
                    None
                }
            }));
            conn.append(&mut other);
        }
        res.push(r);
        next = plot.iter().cloned().next();
    }
    res
}

impl Garden {
    pub fn from_input(s: &'static str) -> Self {
        let mut plots: Vec<Region> = Vec::new();
        let mut idx: HashMap<char, usize> = HashMap::new();
        for (i, l) in s.lines().enumerate() {
            for (j, c) in l.chars().enumerate() {
                let p = Point::from(i, j);
                if let Some(&n) = idx.get(&c) {
                    plots[n].insert(p);
                } else {
                    idx.insert(c, plots.len());
                    plots.push(HashSet::from([p]));
                }
            }
        }
        Self {
            plots: plots.iter_mut().flat_map(find_disjoint_regions).collect(),
        }
    }
    pub fn total_price(self) -> usize {
        self.plots.iter().map(get_plot_price).sum()
    }
}
