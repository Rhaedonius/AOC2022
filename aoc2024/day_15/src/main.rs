fn question1(input: &'static str) -> usize {
    return input.len();
}

fn question2(input: &'static str) -> usize {
    return input.len();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    #[ignore]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 0)
    }

    #[test]
    #[ignore]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 0)
    }
    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 0)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 0)
    }
}
