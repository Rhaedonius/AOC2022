use std::collections::{HashMap, VecDeque};

#[derive(Hash, PartialEq, Eq, Clone, Copy)]
struct Point {
    x: isize,
    y: isize,
}

pub struct TopographicMap {
    pos: HashMap<Point, usize>,
}

impl std::ops::Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

const AROUND: [Point; 4] = [
    Point { x: 1, y: 0 },
    Point { x: 0, y: 1 },
    Point { x: -1, y: 0 },
    Point { x: 0, y: -1 },
];

impl TopographicMap {
    pub fn from_input(s: &'static str) -> Self {
        let mut pos = HashMap::new();
        for (i, l) in s.lines().enumerate() {
            for (j, c) in l.chars().enumerate() {
                let p = Point {
                    x: i as isize,
                    y: j as isize,
                };
                let h = c.to_digit(10).expect("unable to parse input digit") as usize;
                pos.insert(p, h);
            }
        }
        Self { pos }
    }
    fn score_trail(&self, p: Point) -> usize {
        let mut res: Vec<Point> = Vec::new();
        let mut q = VecDeque::from([p]);
        while let Some(p) = q.pop_front() {
            for a in AROUND {
                let n = p + a;
                if !self.pos.contains_key(&n) {
                    // out of bounds
                    continue;
                }
                if self.pos[&p] + 1 != self.pos[&n] {
                    // increase too big
                    continue;
                }
                if self.pos[&n] == 9 && !res.contains(&n) {
                    // end of trail
                    res.push(n);
                } else {
                    q.push_back(n);
                }
            }
        }
        res.len()
    }
    fn rate_trail(&self, p: Point) -> usize {
        let mut res = 0;
        let mut q = VecDeque::from([p]);
        while let Some(p) = q.pop_front() {
            for a in AROUND {
                let n = p + a;
                if !self.pos.contains_key(&n) {
                    // out of bounds
                    continue;
                }
                if self.pos[&p] + 1 != self.pos[&n] {
                    // increase too big
                    continue;
                }
                if self.pos[&n] == 9 {
                    // end of trail
                    res += 1;
                } else {
                    q.push_back(n);
                }
            }
        }
        res
    }
    pub fn total_score(self) -> usize {
        self.pos
            .iter()
            .filter_map(|(&k, &v)| {
                if v == 0 {
                    Some(self.score_trail(k))
                } else {
                    None
                }
            })
            .sum()
    }
    pub fn total_rating(self) -> usize {
        self.pos
            .iter()
            .filter_map(|(&k, &v)| {
                if v == 0 {
                    Some(self.rate_trail(k))
                } else {
                    None
                }
            })
            .sum()
    }
}
