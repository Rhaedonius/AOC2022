fn try_mul(input: &str, i: usize) -> (usize, usize) {
    let n = input[i..].find("mul(");
    if n.is_none() {
        return (input.len(), 0);
    }
    let mut i = i + n.unwrap() + 4;
    let mut a = 0;
    let mut b = 0;
    for c in input[i..].chars() {
        if c.is_digit(10) {
            a = a * 10 + c.to_digit(10).unwrap();
        } else if c == ',' {
            break;
        } else {
            return (i + 1, 0);
        }
        i += 1;
    }
    for c in input[i + 1..].chars() {
        if c.is_digit(10) {
            b = b * 10 + c.to_digit(10).unwrap();
        } else if c == ')' {
            break;
        } else {
            return (i + 1, 0);
        }
        i += 1;
    }
    (i + 1, (a * b) as usize)
}
fn question1(input: &'static str) -> usize {
    let mut i = 0;
    let mut res = 0;
    while i < input.len() {
        let (next, val) = try_mul(&input, i);
        i = next;
        res += val;
    }
    res
}

fn question2(input: &'static str) -> usize {
    let mut i = 0;
    let mut res = 0;
    let n = input.len();
    while i < n {
        // find the start of the next don't
        let block = i + input[i..].find("don't()").unwrap_or(n - i);
        let mut j = i;
        // parse as before until the block
        while j < block {
            let (next, val) = try_mul(&input, j);
            if next > block {
                j = block;
                break;
            }
            j = next;
            res += val;
        }
        // find the next enable and continue the loop from there
        i = j + input[j..].find("do()").unwrap_or(n - j) + 4;
    }
    res
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 161)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 48)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 188741603)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 67269798)
    }
}
