mod point;

use crate::point::Point;
use std::collections::{HashMap, HashSet};

type MapValues = HashSet<Point>;
type AntennaGroups = HashMap<char, Vec<Point>>;
type Nodes = HashSet<Point>;

fn parse_input(input: &'static str) -> (MapValues, AntennaGroups) {
    let mut map = MapValues::new();
    let mut groups = AntennaGroups::new();
    for (i, l) in input.lines().enumerate() {
        for (j, c) in l.chars().enumerate() {
            let p = Point::from_coords(i, j);
            map.insert(p);
            if c == '.' {
                continue;
            }
            groups.entry(c).and_modify(|v| v.push(p)).or_insert(vec![p]);
        }
    }
    (map, groups)
}

fn find_antinodes(v: Vec<Point>) -> Nodes {
    let mut res = Nodes::new();
    for i in 0..v.len() - 1 {
        for j in i + 1..v.len() {
            let d = v[j] - v[i];
            res.insert(v[j] + d);
            res.insert(v[i] - d);
        }
    }
    res
}

fn find_resonant_nodes(v: Vec<Point>, map: &MapValues) -> Nodes {
    let mut res = Nodes::new();
    for i in 0..v.len() - 1 {
        for j in i + 1..v.len() {
            let d = v[j] - v[i];
            let mut n = v[i];
            while map.contains(&n) {
                res.insert(n);
                n = n + d;
            }
            let mut n = v[i];
            while map.contains(&n) {
                res.insert(n);
                n = n - d;
            }
        }
    }
    res
}

fn question1(input: &'static str) -> usize {
    let (map, groups) = parse_input(input);
    let mut res = Nodes::new();
    for v in groups.into_values() {
        let antinodes = find_antinodes(v);
        for a in antinodes.into_iter().filter(|a| map.contains(&a)) {
            res.insert(a);
        }
    }
    res.len()
}

fn question2(input: &'static str) -> usize {
    let (map, groups) = parse_input(input);
    let mut res = Nodes::new();
    for v in groups.into_values() {
        res.extend(find_resonant_nodes(v, &map).iter());
    }
    res.len()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 14)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 34)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 299)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 1032)
    }
}
