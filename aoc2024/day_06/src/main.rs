use std::collections::HashSet;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl Direction {
    fn turn_right(self) -> Direction {
        match self {
            Direction::Up => Direction::Right,
            Direction::Right => Direction::Down,
            Direction::Down => Direction::Left,
            Direction::Left => Direction::Up,
        }
    }
}

#[derive(Debug, Clone)]
struct Guard {
    map: Vec<char>,
    cols: usize,
    pos: usize,
    dir: Direction,
}

impl Guard {
    fn from_input(input: &'static str) -> Self {
        let map: Vec<char> = input.chars().collect();
        let cols = 1 + map
            .iter()
            .position(|&v| v == '\n')
            .expect("The input cannot consist of a single line");
        let pos = map
            .iter()
            .position(|&v| v == '^')
            .expect("Initial position must be known");
        Self {
            map,
            cols,
            pos,
            dir: Direction::Up,
        }
    }
    fn next_pos(&self) -> Option<(usize, Direction)> {
        let next = match self.dir {
            Direction::Up => self.pos.checked_sub(self.cols),
            Direction::Right => Some(self.pos + 1),
            Direction::Down => Some(self.pos + self.cols),
            Direction::Left => self.pos.checked_sub(1),
        };
        if next.is_none() {
            // out of bounds up or left
            return None;
        }
        let pos = next.unwrap();
        if pos > self.map.len() {
            // out of bounds down
            return None;
        }
        if self.map[pos] == '\n' {
            // out of bounds right
            return None;
        }
        if self.map[pos] == '#' {
            // found obstactle, stay and turn right
            let p = (self.pos, self.dir.turn_right());
            return Some(p);
        }
        // pos good, keep going
        let p = (pos, self.dir);
        Some(p)
    }
    fn get_visited(&mut self) -> HashSet<(usize, Direction)> {
        let mut visited = HashSet::new();
        while let Some(next) = self.next_pos() {
            if visited.contains(&next) {
                return visited;
            }
            visited.insert(next);
            let (pos, newdir) = next;
            self.pos = pos;
            self.dir = newdir;
        }
        visited
    }
    fn is_cycle(&mut self) -> bool {
        let visited = self.get_visited();
        let p = self.next_pos();
        p.is_some() && visited.contains(&p.unwrap())
    }
    fn get_path(&mut self) -> HashSet<usize> {
        self.get_visited()
            .into_iter()
            .map(|(i, _)| i)
            .collect::<HashSet<_>>()
    }
}

fn question1(input: &'static str) -> usize {
    Guard::from_input(input).get_path().len()
}

fn question2(input: &'static str) -> usize {
    let (tx, rx) = std::sync::mpsc::channel();

    let guard = Guard::from_input(input);
    let path = guard.clone().get_path();
    std::thread::spawn(move || {
        for i in path {
            let mut g = guard.clone();
            g.map[i] = '#';
            let res = g.is_cycle();
            tx.send(res).unwrap()
        }
    });
    rx.into_iter().filter(|&v| v).count()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 41)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 6)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 5269)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 1957)
    }
}
