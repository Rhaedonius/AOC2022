use std::collections::HashMap;

fn parse_input(input: &str) -> (Vec<usize>, Vec<usize>) {
    let mut a: Vec<usize> = Vec::new();
    let mut b: Vec<usize> = Vec::new();
    for line in input.lines() {
        let (n1, n2) = line.split_once("   ").unwrap();
        a.push(n1.parse::<usize>().unwrap());
        b.push(n2.parse::<usize>().unwrap());
    }
    a.sort();
    b.sort();
    (a, b)
}

fn question1(input: &'static str) -> usize {
    let (a, b) = parse_input(input);
    return a
        .iter()
        .zip(b.iter())
        .fold(0, |acc, e| acc + e.0.abs_diff(*e.1));
}

fn question2(input: &'static str) -> usize {
    let (a, b) = parse_input(input);
    let mut m: HashMap<usize, usize> = HashMap::new();
    for v in b {
        m.entry(v).and_modify(|e| *e += 1).or_insert(1);
    }
    return a
        .into_iter()
        .fold(0, |acc, e| acc + *m.entry(e).or_default() * e);
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 11)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 31)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 1223326)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 21070419)
    }
}
