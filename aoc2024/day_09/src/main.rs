mod disk;
use crate::disk::Disk;

fn parse_input(input: &'static str) -> Vec<usize> {
    input
        .trim()
        .chars()
        .map(|c| c.to_digit(10).expect("cannot parse input char as digit") as usize)
        .collect()
}

fn move_blocks(v: Vec<usize>) -> Vec<usize> {
    let blocks = v
        .iter()
        .step_by(2)
        .enumerate()
        .flat_map(|(i, &n)| vec![i; n])
        .collect::<Vec<_>>();
    let mut i = 0;
    let mut j = blocks.len();
    let mut res: Vec<usize> = Vec::new();
    for (k, &n) in v.iter().enumerate() {
        if i >= j {
            break;
        }
        let n = usize::min(n, j - i);
        if k % 2 == 0 {
            res.extend_from_slice(&blocks[i..i + n]);
            i += n;
        } else {
            res.extend(blocks[j - n..j].iter().rev());
            j -= n;
        }
    }
    res
}

fn checksum(v: Vec<usize>) -> usize {
    v.iter().enumerate().map(|(i, n)| i * n).sum()
}

fn question1(input: &'static str) -> usize {
    let v = parse_input(input);
    checksum(move_blocks(v))
}

fn question2(input: &'static str) -> usize {
    let mut d = Disk::from_input(input);
    d.defrag();
    d.checksum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 1928)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 2858)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 6154342787400)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 0)
    }
}
