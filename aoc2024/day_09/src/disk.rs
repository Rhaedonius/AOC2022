#[derive(Debug, Clone)]
pub struct Disk {
    files: Vec<File>,
    free: Vec<FreeSpace>,
}

#[derive(Clone, Copy)]
struct File {
    id: usize,
    len: usize,
    pos: usize,
}

#[derive(Debug, Clone, Copy)]
struct FreeSpace {
    pos: usize,
    len: usize,
}

impl File {
    fn checksum(&self) -> usize {
        // checksum is id * pos + id * (pos + 1) + ... + id * (pos + len - 1)
        // factor as id * (pos + pos + 1 + .. + pos + len - 1) = id * (pos * len + 0 + 1 .. len - 1)
        // which results in (id * pos * len) + id * (0 + 1 + .. + len - 1)
        let base = self.id * self.pos * self.len;
        let increment = (self.len * (self.len - 1)) / 2;
        base + self.id * increment
    }
}

impl std::fmt::Debug for File {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "File(id={}, pos={}, len={})",
            self.id, self.pos, self.len
        )
    }
}

impl Disk {
    pub fn from_input(s: &'static str) -> Self {
        let mut files = Vec::new();
        let mut free = Vec::new();
        let mut pos = 0;
        for (i, n) in s
            .trim()
            .chars()
            .map(|c| c.to_digit(10).expect("invalid digit in input") as usize)
            .enumerate()
        {
            if i % 2 == 0 {
                let id = i / 2;
                files.push(File { id, pos, len: n });
            } else {
                free.push(FreeSpace { pos, len: n });
            }
            pos += n;
        }
        Disk { files, free }
    }
    pub fn defrag(&mut self) {
        for f in self.files.iter_mut().rev() {
            for s in self.free.iter_mut() {
                if s.pos < f.pos && s.len >= f.len {
                    f.pos = s.pos;
                    s.len -= f.len;
                    s.pos += f.len;
                }
            }
        }
    }
    pub fn checksum(&self) -> usize {
        self.files.iter().map(|f| f.checksum()).sum()
    }
}
