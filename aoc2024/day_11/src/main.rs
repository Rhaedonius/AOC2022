use std::collections::HashMap;

type Stones = HashMap<usize, usize>;

fn parse_input(input: &'static str) -> Stones {
    let mut res = Stones::new();
    for s in input
        .split_whitespace()
        .map(|m| m.parse::<usize>().expect("unable to parse input value"))
    {
        res.entry(s).and_modify(|n| *n += 1).or_insert(1);
    }
    res
}

fn blink(stones: Stones) -> Stones {
    let mut res = Stones::new();
    for (s, v) in stones {
        if s == 0 {
            res.entry(1).and_modify(|n| *n += v).or_insert(v);
            continue;
        }
        let n = s.ilog10() + 1;
        if n % 2 == 0 {
            let half = 10_u64.pow(n / 2) as usize;
            let a = s / half;
            let b = s % half;
            res.entry(a).and_modify(|n| *n += v).or_insert(v);
            res.entry(b).and_modify(|n| *n += v).or_insert(v);
            continue;
        }
        res.entry(s * 2024).and_modify(|n| *n += v).or_insert(v);
    }
    res
}

fn question1(input: &'static str) -> usize {
    let mut s = parse_input(input);
    for _ in 0..25 {
        s = blink(s);
    }
    return s.values().sum();
}

fn question2(input: &'static str) -> usize {
    let mut s = parse_input(input);
    for _ in 0..75 {
        s = blink(s);
    }
    return s.values().sum();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 55312)
    }

    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 218956)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 259593838049805)
    }
}
