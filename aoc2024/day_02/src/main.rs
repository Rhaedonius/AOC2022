fn is_safe(report: &Vec<usize>) -> bool {
    if report[0] > report[1] {
        return report.windows(2).all(|w| w[0] > w[1] && w[0] - w[1] <= 3);
    } else {
        return report.windows(2).all(|w| w[0] < w[1] && w[1] - w[0] <= 3);
    }
}

fn damped_safe(report: &Vec<usize>) -> bool {
    if is_safe(report) {
        return true;
    }
    let n = report.len();
    for i in 0..n {
        if is_safe(&[&report[0..i], &report[i + 1..n]].concat()) {
            return true;
        }
    }
    false
}

fn parse_input(input: &'static str) -> Vec<Vec<usize>> {
    input
        .lines()
        .map(|l| l.split_whitespace().map(|n| n.parse().unwrap()).collect())
        .collect()
}

fn question1(input: &'static str) -> usize {
    let reports = parse_input(input);
    reports.into_iter().filter(is_safe).count()
}

fn question2(input: &'static str) -> usize {
    let reports = parse_input(input);
    reports.into_iter().filter(damped_safe).count()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 2)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 4)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 559)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 601)
    }
}
