fn question1(input: &'static str) -> usize {
    let s: Vec<char> = input.chars().collect();
    let cols: usize = s
        .iter()
        .position(|&c| c == '\n')
        .expect("Input should contain multiple lines");
    let mut counter = 0;
    for i in 0..s.len() {
        for n in [1, cols, cols + 1, cols + 2] {
            let word = s.iter().skip(i).step_by(n).take(4).collect::<String>();
            if word == "XMAS" || word == "SAMX" {
                counter += 1;
            }
        }
    }
    counter
}

fn question2(input: &'static str) -> usize {
    let s: Vec<char> = input.chars().collect();
    let cols: usize = s
        .iter()
        .position(|&c| c == '\n')
        .expect("Input should contain multiple lines");
    let mut counter = 0;
    for i in 0..s.len() - 2 {
        let word1 = s
            .iter()
            .skip(i)
            .step_by(cols + 2)
            .take(3)
            .collect::<String>();
        let word2 = s
            .iter()
            .skip(i + 2)
            .step_by(cols)
            .take(3)
            .collect::<String>();
        if (word1 == "MAS" || word1 == "SAM") && (word2 == "MAS" || word2 == "SAM") {
            counter += 1;
        }
    }
    counter
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 18)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 9)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 2633)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 0)
    }
}
