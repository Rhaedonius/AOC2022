fn parse_equation(input: &'static str) -> (usize, Vec<usize>) {
    let (a, b) = input
        .split_once(": ")
        .expect("Colon not found in equation input");
    let test = a.parse().expect("Value cannot be parsed as an integer");
    let vals = b
        .split_whitespace()
        .map(|s| s.parse().expect("Value cannot be parsed as an integer"))
        .collect();
    (test, vals)
}

fn is_valid<F>(test: usize, vals: &[usize], subtotals: Vec<usize>, ops: &[F]) -> bool
where
    F: Fn(usize, usize) -> usize,
{
    if vals.len() == 0 {
        return subtotals.contains(&test);
    }
    let next = vals[0];
    let mut res = Vec::new();
    for s in subtotals {
        for op in ops {
            let v = op(s, next);
            if v <= test {
                res.push(v);
            }
        }
    }
    is_valid(test, &vals[1..], res, ops)
}

fn concat_op(a: usize, b: usize) -> usize {
    let d = 1 + b.ilog10();
    a * 10_usize.pow(d) + b
}

fn question1(input: &'static str) -> usize {
    let ops = [usize::saturating_add, usize::saturating_mul];
    input
        .lines()
        .filter_map(|l| {
            let (t, v) = parse_equation(l);
            if is_valid(t, &v[1..], vec![v[0]], &ops) {
                return Some(t);
            } else {
                return None;
            }
        })
        .sum()
}

fn question2(input: &'static str) -> usize {
    let ops = [usize::saturating_add, usize::saturating_mul, concat_op];
    input
        .lines()
        .filter_map(|l| {
            let (t, v) = parse_equation(l);
            if is_valid(t, &v[1..], vec![v[0]], &ops) {
                return Some(t);
            } else {
                return None;
            }
        })
        .sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_concat_op() {
        assert_eq!(concat_op(12, 345), 12345);
        assert_eq!(concat_op(15, 6), 156);
        assert_eq!(concat_op(48, 6), 486);
        assert_eq!(concat_op(17, 8), 178);
    }

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 3749)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 11387)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 2314935962622)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 401477450831495)
    }
}
