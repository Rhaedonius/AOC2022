#[derive(Debug, Copy, Clone)]
struct Point {
    x: isize,
    y: isize,
}

#[derive(Debug)]
struct ClawMachine {
    a: Point,
    b: Point,
    prize: Point,
}

const M: isize = 10_000_000_000_000;

impl ClawMachine {
    fn find_presses(&self) -> Option<(usize, usize)> {
        let d = self.a.x * self.b.y - self.b.x * self.a.y;
        let a = self.prize.x * self.b.y - self.b.x * self.prize.y;
        let b = self.a.x * self.prize.y - self.prize.x * self.a.y;
        if (a % d) != 0 || (b % d) != 0 {
            return None;
        }
        let p1 = (a / d) as usize;
        let p2 = (b / d) as usize;
        return Some((p1, p2));
    }
    fn fix_prize(&self) -> Self {
        let prize = Point {
            x: self.prize.x + M,
            y: self.prize.y + M,
        };
        ClawMachine {
            a: self.a,
            b: self.b,
            prize,
        }
    }
}

impl<'a> From<&'a str> for ClawMachine {
    fn from(s: &'a str) -> Self {
        let (line_a, rem) = s.split_once("\n").unwrap();
        let (line_b, line_p) = rem.split_once("\n").unwrap();
        let (x, y) = line_a
            .strip_prefix("Button A: X+")
            .expect("prefix for button a not found")
            .split_once(", Y+")
            .expect("separator for button a not found");
        let a = Point {
            x: x.parse().expect("unable to parse x"),
            y: y.parse().expect("unable to parse y"),
        };
        let (x, y) = line_b
            .strip_prefix("Button B: X+")
            .expect("prefix for button b not found")
            .split_once(", Y+")
            .expect("separator for prize not found");
        let b = Point {
            x: x.parse().expect("unable to parse x"),
            y: y.parse().expect("unable to parse y"),
        };
        let (x, y) = line_p
            .strip_prefix("Prize: X=")
            .expect("prefix for prize not found")
            .split_once(", Y=")
            .expect("separator for prize not found");
        let prize = Point {
            x: x.parse().expect("unable to parse x"),
            y: y.parse().expect("unable to parse y"),
        };
        ClawMachine { a, b, prize }
    }
}

fn parse_input(s: &'static str) -> Vec<ClawMachine> {
    s.trim().split("\n\n").map(ClawMachine::from).collect()
}

fn question1(input: &'static str) -> usize {
    parse_input(input)
        .into_iter()
        .filter_map(|c| c.find_presses())
        .map(|p| p.0 * 3 + p.1 * 1)
        .sum()
}

fn question2(input: &'static str) -> usize {
    parse_input(input)
        .into_iter()
        .filter_map(|c| c.fix_prize().find_presses())
        .map(|p| p.0 * 3 + p.1 * 1)
        .sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 480)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 875318608908)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 29388)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 99548032866004)
    }
}
