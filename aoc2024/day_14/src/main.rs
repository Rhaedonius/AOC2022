mod point;
use crate::point::Point;

const SPACE: Point = Point { x: 101, y: 103 };

struct Robot {
    p: Point,
    v: Point,
}

impl<'a> From<&'a str> for Robot {
    fn from(s: &'a str) -> Self {
        let (p_str, v_str) = s.split_once(" ").expect("unable to separate p and v");
        let p = Point::from(&p_str[2..]);
        let v = Point::from(&v_str[2..]);
        Robot { p, v }
    }
}

impl std::fmt::Debug for Robot {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(
            f,
            "Robot(p={},{}, v={}, {})",
            self.p.x, self.p.y, self.v.x, self.v.y
        )
    }
}

fn parse_input(input: &'static str) -> Vec<Robot> {
    input.lines().map(Robot::from).collect()
}

fn safety_factor(robots: &[Robot], space: Point) -> usize {
    let h = space.x / 2;
    let v = space.y / 2;
    let mut q = vec![0, 0, 0, 0];
    for r in robots {
        match r.p {
            p if p.x < h && p.y < v => q[0] += 1,
            p if p.x < h && p.y > v => q[1] += 1,
            p if p.x > h && p.y < v => q[2] += 1,
            p if p.x > h && p.y > v => q[3] += 1,
            _ => continue,
        }
    }
    q.iter().product()
}

fn print_res(robots: &[Robot], space: Point) {
    let mut out = vec![vec![' '; space.x as usize]; space.y as usize];
    for r in robots.iter() {
        out[r.p.y as usize][r.p.x as usize] = 'X';
    }
    for s in out {
        println!("{}", s.into_iter().collect::<String>())
    }
}

fn question1(input: &'static str, space: Point) -> usize {
    let mut robots = parse_input(input);
    for r in robots.iter_mut() {
        r.p = (r.p + r.v * 100) % space;
    }
    safety_factor(&robots, space)
}

fn question2(input: &'static str, space: Point) -> usize {
    // turns out this is a minimization problem on the safety factor
    let mut robots = parse_input(input);
    let mut count = 0;
    let mut best_score = safety_factor(&robots, space);
    for i in 1.. {
        for r in robots.iter_mut() {
            r.p = (r.p + r.v) % space;
        }
        let score = safety_factor(&robots, space);
        if score > best_score {
            continue;
        }
        if score == best_score {
            print_res(&robots, space);
            break;
        }
        best_score = score;
        count = i;
    }
    count
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input, SPACE));
    println!("{:?}", question2(input, SPACE));
}

#[cfg(test)]
mod tests {
    use super::*;

    const TEST_SPACE: Point = Point { x: 11, y: 7 };

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input, TEST_SPACE), 12)
    }

    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input, SPACE), 211692000)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input, SPACE), 6587)
    }
}
