#[derive(Hash, PartialEq, Eq, Clone, Copy)]
pub struct Point {
    pub x: isize,
    pub y: isize,
}

impl std::ops::Add for Point {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl std::ops::Rem for Point {
    type Output = Self;

    fn rem(self, rhs: Point) -> Self::Output {
        Point {
            x: self.x.rem_euclid(rhs.x),
            y: self.y.rem_euclid(rhs.y),
        }
    }
}

impl std::ops::Mul<isize> for Point {
    type Output = Self;

    fn mul(self, rhs: isize) -> Self::Output {
        Point {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl<'a> From<&'a str> for Point {
    fn from(s: &'a str) -> Self {
        let (a, b) = s
            .split_once(",")
            .expect("unable to separate the two values");
        Point {
            x: a.parse().expect("unable to parse x value"),
            y: b.parse().expect("unable  to parse y value"),
        }
    }
}
