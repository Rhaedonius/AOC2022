use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};

type Rules = HashMap<usize, HashSet<usize>>;

fn parse_rules(rules: &'static str) -> Rules {
    let mut map: Rules = HashMap::new();
    for l in rules.lines() {
        let (a, b) = l.split_once("|").expect("'|' char not found in pattern.");
        let key = a
            .parse::<usize>()
            .expect("unable to parse the first value in the rule");
        let val = b
            .parse::<usize>()
            .expect("unable to parse the second value in the rule");
        map.entry(key)
            .and_modify(|v| {
                v.insert(val);
            })
            .or_insert(HashSet::from([val]));
    }
    map
}

fn check_update(update: &[usize], rules: &Rules) -> bool {
    let mut prev = HashSet::new();
    for p in update {
        let maybe_v = rules.get(&p);
        if maybe_v.is_none() {
            prev.insert(*p);
            continue;
        }
        let v = maybe_v.unwrap();
        if prev.intersection(&v).count() > 0 {
            return false;
        }
        prev.insert(*p);
    }
    true
}

fn compare(a: usize, b: usize, rules: &Rules) -> Ordering {
    let rule_a = rules.get(&a).unwrap_or(&HashSet::new()).contains(&b);
    let rule_b = rules.get(&b).unwrap_or(&HashSet::new()).contains(&a);
    match (rule_a, rule_b) {
        (true, false) => Ordering::Less,
        (false, true) => Ordering::Greater,
        (false, false) => Ordering::Equal,
        (true, true) => panic!("unsatisfiable rules"),
    }
}

fn question1(input: &'static str) -> usize {
    let (r, p) = input
        .split_once("\n\n")
        .expect("The two sections of the input must be separated by an empty line.");
    let rules = parse_rules(r);
    let mut res = 0;
    for l in p.lines() {
        let update: Vec<usize> = l
            .split(',')
            .map(|s| {
                s.parse::<usize>()
                    .expect("cannot parse page number as a valid integer")
            })
            .collect();
        if check_update(&update, &rules) {
            res += update[update.len() / 2]
        }
    }
    res
}

fn question2(input: &'static str) -> usize {
    let (r, p) = input
        .split_once("\n\n")
        .expect("The two sections of the input must be separated by an empty line.");
    let rules = parse_rules(r);
    let mut res = 0;
    for l in p.lines() {
        let mut update: Vec<usize> = l
            .split(',')
            .map(|s| {
                s.parse::<usize>()
                    .expect("cannot parse page number as a valid integer")
            })
            .collect();
        if !check_update(&update, &rules) {
            update.sort_by(|&a, &b| compare(a, b, &rules));
            res += update[update.len() / 2]
        }
    }
    res
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 143)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 123)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 4790)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 0)
    }
}
