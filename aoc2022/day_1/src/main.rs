use std::vec::Vec;
#[cfg(test)]
mod tests;

fn count_calories(input: &str) -> Vec<u32> {
    let mut calories: Vec<u32> = Vec::new();
    let mut calory_count: u32 = 0;
    for line in input.lines() {
        if line == "" {
            calories.push(calory_count);
            calory_count = 0;
        } else {
            if let Ok(cal) = u32::from_str_radix(line, 10) {
                calory_count += cal;
            }
        }
    }
    calories.sort_by(|a, b| b.cmp(a));
    calories
}

fn question1(calories: &Vec<u32>) -> u32 {
    let max_calories = calories.first().unwrap();
    return *max_calories;
}

fn question2(calories: &Vec<u32>) -> u32 {
    let top_3_calories: u32 = calories.iter().take(3).sum();
    return top_3_calories;
}

fn main() {
    let input = include_str!("../input.txt");
    let calories = count_calories(input);
    println!("Question 1: {}", question1(&calories));
    println!("Question 2: {}", question2(&calories));
}
