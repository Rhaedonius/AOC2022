use crate::{count_calories, question1, question2};

#[test]
fn test_question1() {
    let calories = count_calories(include_str!("../input.txt"));
    assert_eq!(question1(&calories), 69912);
}

#[test]
fn test_question2() {
    let calories = count_calories(include_str!("../input.txt"));
    assert_eq!(question2(&calories), 208180);
}
