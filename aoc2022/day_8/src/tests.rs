// use std::time::Instant;

use crate::{forest::Forest, question1, question2};

#[test]
fn test_question1() {
    let forest = Forest::from_input(include_str!("../input.txt"));
    /*
    let n = 100;
    let clock = Instant::now();
    (0..n).for_each(|_| {
        question1(&forest);
    });
    println!("Question 1 took {}", clock.elapsed().as_millis() / n);
    // takes around 120ms, 70ms with dp
    */
    assert_eq!(question1(&forest), 1695)
}

#[test]
fn test_question2() {
    let forest = Forest::from_input(include_str!("../input.txt"));
    /*
    let n = 100;
    let clock = Instant::now();
    (0..n).for_each(|_| {
        question2(&forest);
    });
    println!("Question 2 took {}", clock.elapsed().as_millis() / n);
    // around 50ms
    */
    assert_eq!(question2(&forest), 287040)
}
