#[derive(Debug)]
pub struct Forest {
    pub shape: (usize, usize),
    values: Vec<Vec<u8>>,
}

impl Forest {
    #[inline]
    pub fn from_input(input: &str) -> Self {
        let lines = input.lines();
        let values: Vec<Vec<u8>> = lines
            .map(|l| l.bytes().map(|d| d - b'0').collect())
            .collect();
        let cols = values[0].len();
        let rows = values.len();
        Forest {
            shape: (rows, cols),
            values,
        }
    }

    #[inline]
    pub fn get(&self, row: usize, col: usize) -> &u8 {
        &self.values[row][col]
    }

    #[inline]
    pub fn get_row(&self, row: usize) -> &Vec<u8> {
        &self.values[row]
    }

    #[inline]
    pub fn get_col(&self, col: usize) -> Vec<u8> {
        self.values.iter().map(|r| r[col]).collect()
    }
}
