mod forest;

#[cfg(test)]
mod tests;

fn is_visible(tree: &u8, others: &[u8]) -> bool {
    tree > others.iter().max().unwrap()
}

fn visibility_score(i: usize, j: usize, forest: &forest::Forest) -> usize {
    fn score(tree: &u8, others: &[u8]) -> usize {
        let distance = others
            .iter()
            .position(|o| o >= tree)
            .unwrap_or(others.len() - 1);
        distance + 1
    }
    fn score_rev(tree: &u8, others: &[u8]) -> usize {
        let distance = others
            .iter()
            .rev()
            .position(|o| o >= tree)
            .unwrap_or(others.len() - 1);
        distance + 1
    }
    let tree = forest.get(i, j);
    let row = forest.get_row(i);
    let col = forest.get_col(j);
    score_rev(tree, &row[..j])
        * score_rev(tree, &col[..i])
        * score(tree, &row[j + 1..])
        * score(tree, &col[i + 1..])
}

fn question1(forest: &forest::Forest) -> usize {
    let (rows, cols) = forest.shape;
    let mut visible: usize = cols * 2 + rows * 2 - 4;
    let mut dp_row: Vec<u8> = forest.get_col(0);
    let mut dp_col: Vec<u8> = forest.get_row(0).clone();
    for i in 1..rows - 1 {
        for j in 1..cols - 1 {
            let tree = *forest.get(i, j);
            // left check
            if tree > dp_row[i] {
                visible += 1;
                dp_row[i] = tree;
                // still need to check top before moving on for dp
                dp_col[j] = u8::max(dp_col[j], tree);
                continue;
            }
            // up check
            if tree > dp_col[j] {
                visible += 1;
                dp_col[j] = tree;
                continue;
            }
            // right check and down check
            if is_visible(&tree, &forest.get_row(i)[j + 1..])
                || is_visible(&tree, &forest.get_col(j)[i + 1..])
            {
                visible += 1;
            }
        }
    }
    return visible;
}

fn question2(forest: &forest::Forest) -> usize {
    let (rows, cols) = forest.shape;
    let mut top_score: usize = 0;
    for i in 1..rows - 1 {
        for j in 1..cols - 1 {
            let score = visibility_score(i, j, forest);
            if score > top_score {
                top_score = score;
            }
        }
    }
    return top_score;
}

fn main() {
    let input = include_str!("../input.txt");
    let forest = forest::Forest::from_input(input);
    //dbg!(&forest);
    println!("{:?}", question1(&forest));
    println!("{:?}", question2(&forest));
}
