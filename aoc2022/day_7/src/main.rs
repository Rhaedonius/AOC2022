use std::collections::BTreeMap;
mod utils;

#[cfg(test)]
mod tests;

type DirSizes = BTreeMap<String, usize>;

fn question1(dir_sizes: &DirSizes) -> usize {
    let answer: usize = dir_sizes.values().filter(|&&size| size < 100000).sum();
    return answer;
}

fn question2(dir_sizes: DirSizes) -> usize {
    let root_size = dir_sizes.get("/").unwrap();
    let free_space = 70000000 - root_size;
    return dir_sizes
        .into_values()
        .filter(|size| free_space + size > 30000000)
        .min()
        .unwrap();
}

fn main() {
    let input = include_str!("../input.txt");
    let (dirs, files) = utils::parse_input(input);
    let mut dir_sizes = DirSizes::new();
    for d in dirs.iter() {
        let size = utils::get_dir_size(&d, &files);
        dir_sizes.entry(String::from(d)).or_insert(size);
    }
    println!("Question 1: {}", question1(&dir_sizes));
    println!("Question 2: {}", question2(dir_sizes));
}
