use crate::{question1, question2, utils, DirSizes};

#[test]
fn test_question1() {
    let input = include_str!("../input.txt");
    let (dirs, files) = utils::parse_input(input);
    let mut dir_sizes = DirSizes::new();
    for d in dirs.iter() {
        let size = utils::get_dir_size(&d, &files);
        dir_sizes.entry(String::from(d)).or_insert(size);
    }
    assert_eq!(question1(&dir_sizes), 1243729);
}

#[test]
fn test_question2() {
    let input = include_str!("../input.txt");
    let (dirs, files) = utils::parse_input(input);
    let mut dir_sizes = DirSizes::new();
    for d in dirs.iter() {
        let size = utils::get_dir_size(&d, &files);
        dir_sizes.entry(String::from(d)).or_insert(size);
    }
    assert_eq!(question2(dir_sizes), 4443914);
}
