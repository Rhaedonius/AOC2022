use std::collections::BTreeMap;

#[derive(Debug)]
struct Path {
    path: Vec<String>,
}

impl Path {
    fn new() -> Self {
        let root = String::from("/");
        Path {
            path: Vec::from([root]),
        }
    }

    fn cd(&mut self, new_dir: &str) {
        match new_dir {
            ".." => {
                self.path.pop();
            }
            "/" => {}
            d => {
                let dir_owned = String::from(d);
                self.path.push(dir_owned);
            }
        }
    }

    fn get_pwd(&self) -> String {
        let mut pwd = String::from("/");
        for p in self.path.iter().skip(1) {
            pwd = format!("{}{}/", pwd, p);
        }
        pwd
    }
}

pub fn get_dir_size(dir: &str, file_sizes: &BTreeMap<String, usize>) -> usize {
    file_sizes
        .iter()
        .filter_map(|(name, size)| {
            if name.starts_with(dir) {
                Some(size)
            } else {
                None
            }
        })
        .sum::<usize>()
}

pub fn parse_input(input: &str) -> (Vec<String>, BTreeMap<String, usize>) {
    let mut path = Path::new();
    let mut dirs = Vec::<String>::new();
    let mut files = BTreeMap::<String, usize>::new();
    for line in input.lines() {
        let mut tokens = line.split_whitespace();
        if line.starts_with("$") {
            // command
            tokens.next(); // drop the "$"
            match tokens.next() {
                Some("cd") => path.cd(tokens.next().unwrap()),
                Some("ls") => dirs.push(path.get_pwd()),
                t => {
                    dbg!(tokens.collect::<Vec<_>>());
                    dbg!(t);
                    dbg!(path);
                    dbg!(dirs);
                    dbg!(files);
                    panic!("Unknown command: {}", &line);
                }
            }
        } else {
            // ls output
            if let Ok(size) = tokens.next().unwrap().parse::<usize>() {
                let file_name = tokens.next().unwrap();
                let full_path = format!("{}{}", path.get_pwd(), file_name);
                files.insert(full_path, size);
            }
        }
    }
    (dirs, files)
}
