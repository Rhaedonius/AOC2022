use std::collections::HashSet;
use std::collections::VecDeque;
#[cfg(test)]
mod tests;

type Buffer = VecDeque<char>;

fn found_marker(b: &Buffer) -> bool {
    let h: HashSet<&char> = HashSet::from_iter(b.iter());
    b.len() == h.len()
}

fn question1(input: &str) -> u32 {
    let mut counter: u32 = 4;
    let mut buf = Buffer::from_iter(input.chars().take(4));
    for c in input.chars().skip(4) {
        if found_marker(&buf) {
            return counter;
        }
        buf.pop_front();
        buf.push_back(c);
        counter += 1
    }
    0
}

fn question2(input: &str) -> u32 {
    let mut counter: u32 = 14;
    let mut buf = Buffer::from_iter(input.chars().take(14));
    for c in input.chars().skip(14) {
        if found_marker(&buf) {
            return counter;
        }
        buf.pop_front();
        buf.push_back(c);
        counter += 1
    }
    0
}

// https://twitter.com/ThePrimeagen/status/1600222591079227397
fn fast_solution(input: &str, n: usize) -> usize {
    input
        .as_bytes()
        .windows(n)
        .position(|buf| {
            let mut bitset: u32 = 0;
            for &c in buf {
                let prev = bitset;
                bitset = bitset | 1 << (c - b'a');
                if prev == bitset {
                    return false;
                }
            }
            true
        })
        .unwrap()
        + n
}

fn main() {
    let input = include_str!("../input.txt");
    println!("Question 1: {}", question1(input));
    println!("Question 2: {}", question2(input));
    println!("{}", fast_solution(input, 14));
}
