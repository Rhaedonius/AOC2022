use std::collections::HashMap;

use valve::{Action, State, Valve};

mod valve;

#[cfg(test)]
mod tests;

fn question1(input: &str) -> i64 {
    let mut timer: i64 = 30;
    let valves: Vec<Valve> = input.lines().map(|l| l.parse::<Valve>().unwrap()).collect();
    let mut dp = HashMap::from([(State::new(), 0)]);
    let mut best = 0;
    while timer > 0 {
        let possible_states = dp.clone();
        dp = HashMap::new();
        for state in possible_states.keys() {
            let current_node = valves.iter().find(|v| v.name == *state.position).unwrap();
            for action in current_node.get_actions(&state) {
                match action {
                    Action::MOVE(n) => {
                        let (new_state, new_flow) = state.move_to(n, possible_states[&state]);
                        let max_remaining_flow = &valves
                            .iter()
                            .filter(|v| !k.open_valves.contains(&v.name))
                            .map(|v| v.flow_rate)
                            .max()
                            .unwrap_or_default();

                        if max_remaining_flow * timer + new_flow >= best {
                            dp.entry(new_state)
                                .and_modify(|flow| *flow = i64::max(*flow, new_flow))
                                .or_insert(new_flow);
                        }
                    }
                    Action::OPEN => {
                        let flow = current_node.flow_rate * (timer - 1) + possible_states[&state];
                        let (new_state, new_flow) = state.open_valve(flow);
                        dp.entry(new_state)
                            .and_modify(|flow| *flow = i64::max(*flow, new_flow))
                            .or_insert(new_flow);
                    }
                }
            }
        }
        best = dp.values().max().unwrap().clone();
        timer -= 1;
    }
    return best;
}

fn question2(input: &str) -> usize {
    return input.len();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}
