use std::cmp::Ordering;
use std::fmt;
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd)]
pub struct Valve {
    pub flow_rate: i64,
    pub open: bool,
    pub name: String,
    pub next: Vec<String>,
}

#[derive(Hash, Clone, PartialEq, Eq)]
pub struct State {
    pub open_valves: Vec<String>,
    pub position: String,
}

impl fmt::Debug for State {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "At valve {}. ", self.position)?;
        if self.open_valves.len() == 0 {
            write!(f, "No open valves. ")
        } else {
            write!(
                f,
                "Open valves: {}.",
                self.open_valves
                    .clone()
                    .into_iter()
                    .reduce(|acc, s| format!("{acc}, {s}"))
                    .unwrap()
            )
        }
    }
}

impl State {
    pub fn new() -> Self {
        let open_valves = Vec::new();
        let position = "AA".to_owned();
        return Self {
            open_valves,
            position,
        };
    }
    pub fn move_to(&self, node: String, flow: i64) -> (State, i64) {
        let mut new_state = self.clone();
        new_state.position = node;
        return (new_state, flow);
    }
    pub fn open_valve(&self, flow: i64) -> (State, i64) {
        let mut new_state = self.clone();
        new_state.open_valves.push(self.position.clone());
        return (new_state, flow);
    }
}

pub enum Action {
    MOVE(String),
    OPEN,
}

impl Valve {
    pub fn get_actions(&self, state: &State) -> Vec<Action> {
        let mut actions = Vec::from_iter(self.next.iter().map(|n| Action::MOVE(n.clone())));
        if !state.open_valves.contains(&self.name) {
            actions.push(Action::OPEN);
        }
        return actions;
    }
}

impl FromStr for Valve {
    type Err = ParseIntError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let (name, remainder) = input
            .strip_prefix("Valve ")
            .unwrap()
            .split_once(' ')
            .unwrap();
        let (flow_rate, remainder) = remainder
            .strip_prefix("has flow rate=")
            .unwrap()
            .split_once("; ")
            .unwrap();
        let next: Vec<String> = remainder
            .strip_prefix("tunnels lead to valves ")
            .unwrap_or_else(|| remainder.strip_prefix("tunnel leads to valve ").unwrap())
            .split(", ")
            .map(|s| s.to_owned())
            .collect();
        return Ok(Valve {
            name: name.to_owned(),
            flow_rate: flow_rate.parse().unwrap(),
            open: false,
            next,
        });
    }
}

impl Ord for Valve {
    fn cmp(&self, other: &Self) -> Ordering {
        return self.flow_rate.cmp(&other.flow_rate);
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_input() {
        let valve: Valve = "Valve AA has flow rate=0; tunnels lead to valves DD, II, BB"
            .parse()
            .unwrap();
        assert_eq!(
            valve,
            Valve {
                name: "AA".to_owned(),
                flow_rate: 0,
                open: false,
                next: Vec::from(["DD".to_owned(), "II".to_owned(), "BB".to_owned()])
            }
        )
    }

    #[test]
    fn test_question_1() {
        assert_eq!(crate::question1(include_str!("../test.txt")), 1651);
    }
}
