// use std::time::Instant;

use crate::{question1, question2};

#[test]
#[ignore]
fn test_question1() {
    let input = include_str!("../input.txt");
    assert_eq!(question1(input, 2000000), 5083287)
}

#[test]
#[ignore]
fn test_question2() {
    let input = include_str!("../input.txt");
    assert_eq!(question2(input, 4000000), 13134039205729)
}
