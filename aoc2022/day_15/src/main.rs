mod beacon;
use beacon::{Ball, Point};
use std::collections::BTreeSet;

#[cfg(test)]
mod tests;

fn question1(input: &str, y: i32) -> usize {
    let signals: BTreeSet<Ball> = input.lines().map(|l| l.parse().unwrap()).collect();
    let x_min = signals
        .iter()
        .map(|b| b.center.x - b.radius as i32)
        .min()
        .unwrap();
    let x_max = signals
        .iter()
        .map(|b| b.center.x + b.radius as i32)
        .max()
        .unwrap();
    return (x_min..=x_max)
        .map(|x| Point { x, y })
        .filter(|p| signals.iter().any(|s| &s.beacon != p && s.contains(p)))
        .count();
}

fn question2(input: &str, max: i32) -> i64 {
    let signals: BTreeSet<Ball> = input.lines().map(|l| l.parse().unwrap()).collect();
    let points: Vec<Point> = signals.iter().flat_map(|s| s.border()).collect();
    let beacon: Point = points
        .into_iter()
        .find(|p| point_in_range(p, max) && !signals.iter().any(|s| s.contains(p)))
        .unwrap();
    return beacon.x as i64 * 4000000 + beacon.y as i64;
}

fn point_in_range(p: &Point, max: i32) -> bool {
    return p.x >= 0 && p.x <= max && p.y >= 0 && p.y <= max;
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input, 2000000));
    println!("{:?}", question2(input, 4000000));
}
