use std::cmp::Ordering;
use std::num::ParseIntError;
use std::ops::Add;
use std::str::FromStr;

#[derive(Debug, Clone, Copy, Ord, PartialOrd, PartialEq, Eq, Hash)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Add for Point {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialOrd, PartialEq, Eq, Hash)]
pub struct Ball {
    pub center: Point,
    pub beacon: Point,
    pub radius: u32,
}

impl Ord for Ball {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.center.cmp(&other.center) {
            Ordering::Equal => return other.center.cmp(&self.center),
            o => return o,
        }
    }
}

impl Ball {
    pub fn contains(&self, point: &Point) -> bool {
        return distance(&self.center, point) <= self.radius;
    }
    pub fn border(&self) -> Vec<Point> {
        let mut border = Vec::new();
        let bradius = self.radius as i32 + 1;
        for i in 1..bradius {
            border.push(
                Point {
                    x: -i,
                    y: i - bradius,
                } + self.center,
            );
            border.push(
                Point {
                    x: -i,
                    y: bradius - i,
                } + self.center,
            );
            border.push(
                Point {
                    x: i,
                    y: i - bradius,
                } + self.center,
            );
            border.push(
                Point {
                    x: i,
                    y: bradius - i,
                } + self.center,
            );
        }
        border.push(Point { x: bradius, y: 0 } + self.center);
        border.push(Point { x: -bradius, y: 0 } + self.center);
        border.push(Point { x: 0, y: bradius } + self.center);
        border.push(Point { x: 0, y: -bradius } + self.center);
        return border;
    }
}

impl FromStr for Ball {
    type Err = ParseIntError;

    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let coords: Vec<i32> = input
            .split([',', ':', '='])
            .filter_map(|s| {
                if let Ok(i) = s.parse::<i32>() {
                    return Some(i);
                } else {
                    return None;
                }
            })
            .collect();

        let center = Point {
            x: coords[0],
            y: coords[1],
        };
        let beacon = Point {
            x: coords[2],
            y: coords[3],
        };
        let radius = distance(&center, &beacon);
        return Ok(Ball {
            center,
            beacon,
            radius,
        });
    }
}

fn distance(a: &Point, b: &Point) -> u32 {
    return a.x.abs_diff(b.x) + a.y.abs_diff(b.y);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ball() {
        let center = Point { x: 0, y: 0 };
        let beacon = Point { x: 0, y: 2 };
        let ball = Ball {
            center,
            beacon,
            radius: 2,
        };
        assert!(ball.contains(&Point { x: 1, y: 1 }));
        assert!(!ball.contains(&Point { x: 1, y: 2 }));
    }
    #[test]
    fn test_border() {
        let ball = Ball {
            center: Point { x: 1, y: -1 },
            beacon: Point { x: 0, y: 0 },
            radius: 2,
        };
        let border = ball.border();
        for x in -2..=4 {
            for y in -4..=2 {
                let p = Point { x, y };
                if distance(&ball.center, &p) == 3 {
                    assert!(border.contains(&p), "{},{} is not in border", p.x, p.y);
                } else {
                    assert!(!border.contains(&p));
                }
            }
        }
    }
    #[test]
    fn test_question_1() {
        let test = include_str!("../test.txt");
        assert_eq!(crate::question1(test, 10), 26);
    }

    #[test]
    fn test_question_2() {
        let test = include_str!("../test.txt");
        assert_eq!(crate::question2(test, 20), 56000011);
    }
}
