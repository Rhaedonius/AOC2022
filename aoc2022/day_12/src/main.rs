mod nodes;
use nodes::Nodemap;

#[cfg(test)]
mod tests;

fn question1(input: &str) -> u16 {
    eprintln!("-----------------");
    eprintln!("Question 1");
    let mut nodemap = Nodemap::from(input);
    let start = nodemap.start;
    let end = nodemap.end;
    eprintln!("");
    return nodemap.shortest_path(start, end);
}

fn question2(input: &str) -> u16 {
    eprintln!("-----------------");
    eprintln!("Question 2");
    let mut nodemap = Nodemap::from(input);
    let end = nodemap.end;
    let mut best_path = u16::MAX;
    let starting_positions = nodemap.get_positions_from_height(b'a');
    for start in starting_positions {
        let new_path = nodemap.shortest_path(start, end);
        best_path = u16::min(best_path, new_path);
    }
    eprintln!("");
    return best_path;
}

fn main() {
    let input = include_str!("../input.txt");
    println!("Q1 Answer: {:?}", question1(input));
    println!("Q2 Answer: {:?}", question2(input));
}
