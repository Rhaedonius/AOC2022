use std::collections::VecDeque;

pub type Position = (usize, usize);

#[derive(Debug, Copy, Clone)]
struct Node {
    position: Position,
    visited: bool,
    pub weight: u16,
    height: u8,
}

#[derive(Debug, Clone)]
pub struct Nodemap {
    nodes: Vec<Vec<Node>>,
    pub start: Position,
    pub end: Position,
    shape: (usize, usize),
}

impl Node {
    fn from(position: Position, height: u8) -> Self {
        return Self {
            position,
            visited: false,
            weight: u16::MAX,
            height,
        };
    }
}

impl Nodemap {
    pub fn from(input: &str) -> Self {
        let mut nodes: Vec<Vec<Node>> = Vec::new();
        let mut start = (0, 0);
        let mut end = (0, 0);
        for (i, l) in input.lines().enumerate() {
            let mut row: Vec<Node> = Vec::new();
            for (j, c) in l.bytes().enumerate() {
                let pos = (i, j);
                if c == b'S' {
                    start = pos;
                    row.push(Node {
                        position: pos,
                        visited: false,
                        weight: u16::MAX,
                        height: b'a',
                    });
                    continue;
                }
                if c == b'E' {
                    end = pos;
                    row.push(Node {
                        position: pos,
                        visited: false,
                        weight: u16::MAX,
                        height: b'z',
                    });
                    continue;
                }
                row.push(Node::from(pos, c));
            }
            nodes.push(row);
        }
        let shape = (nodes.len(), nodes[0].len());
        return Nodemap {
            nodes,
            start,
            end,
            shape,
        };
    }
    fn neighbours(&self, pos: Position) -> Vec<Node> {
        let mut neighbours: Vec<Node> = Vec::new();
        let (i, j) = pos;
        if i != 0 {
            let new_pos = (i - 1, j);
            let next_node = self.get(new_pos);
            if !next_node.visited {
                neighbours.push(next_node.clone());
            }
        }
        if i != self.shape.0 - 1 {
            let new_pos = (i + 1, j);
            let next_node = self.get(new_pos);
            if !next_node.visited {
                neighbours.push(next_node.clone());
            }
        }
        if j != 0 {
            let new_pos = (i, j - 1);
            let next_node = self.get(new_pos);
            if !next_node.visited {
                neighbours.push(next_node.clone());
            }
        }
        if j != self.shape.1 - 1 {
            let new_pos = (i, j + 1);
            let next_node = self.get(new_pos);
            if !next_node.visited {
                neighbours.push(next_node.clone());
            }
        }
        return neighbours;
    }
    fn get(&self, pos: Position) -> &Node {
        return &self.nodes[pos.0][pos.1];
    }
    fn get_mut(&mut self, pos: Position) -> &mut Node {
        return &mut self.nodes[pos.0][pos.1];
    }
    fn reset_visited(&mut self) {
        for r in self.nodes.iter_mut() {
            for mut n in r {
                n.visited = false;
            }
        }
    }
    pub fn shortest_path(&mut self, start: Position, end: Position) -> u16 {
        self.reset_visited();
        self.get_mut(start).visited = true;
        self.get_mut(start).weight = 0;
        let mut nodes: VecDeque<Node> = VecDeque::from([self.get(start).clone()]);
        //eprintln!("Starting from node at {:?}", self.get(start).position);
        while let Some(node) = nodes.pop_front() {
            // eprintln!(
            //     "Checking node at {:?}, with height {} and weight {}",
            //     node.position, node.height, node.weight
            // );
            let mut neighbours = self.neighbours(node.position);
            while let Some(neighbour) = neighbours.pop() {
                // eprintln!(
                //     "Checking neighbour at {:?}, with height {}",
                //     neighbour.position, neighbour.height
                // );
                if node.height + 1 >= neighbour.height {
                    // eprintln!(
                    //     "Node {:?} can be visited from {:?} and new weight is {}",
                    //     neighbour.position,
                    //     node.position,
                    //     node.weight + 1
                    // );
                    let node_mut = self.get_mut(neighbour.position);
                    node_mut.visited = true;
                    node_mut.weight = node.weight + 1;
                    nodes.push_back(node_mut.clone());
                }
            }
            let end_node = self.get(end);
            if end_node.visited {
                eprintln!(
                    "End node visited from {:?} with weight {}",
                    start, end_node.weight
                );
                return end_node.weight;
            }
        }
        return u16::MAX;
    }
    pub fn get_positions_from_height(&self, height: u8) -> Vec<Position> {
        return self
            .nodes
            .iter()
            .flat_map(|v| v.into_iter().filter(|n| n.height == height))
            .map(|n| n.position)
            .collect();
    }
}
