#[derive(Debug, Clone)]
pub struct Crt {
    clock: i32,
    x: i32,
    pub strength: i32,
    screen: Vec<char>,
}

impl Crt {
    pub fn new() -> Self {
        return Crt {
            clock: 0,
            x: 1,
            strength: 0,
            screen: Vec::new(),
        };
    }
    pub fn noop(&mut self) {
        self.draw();
        self.clock += 1;
        self.update_strength();
    }
    pub fn addx(&mut self, val: i32) {
        self.draw();
        self.clock += 1;
        self.update_strength();
        self.draw();
        self.clock += 1;
        self.update_strength();
        self.x += val;
    }
    #[inline]
    fn update_strength(&mut self) {
        if self.clock % 40 + 20 == 40 {
            self.strength += self.clock * self.x;
        }
    }
    #[inline]
    fn draw(&mut self) {
        let pos = self.clock % 40;
        if vec![self.x - 1, self.x, self.x + 1].contains(&pos) {
            self.screen.push('X');
        } else {
            self.screen.push(' ');
        }
    }
    pub fn print_screen(&self) -> String {
        let mut out: String = String::new();
        for c in self.screen.chunks_exact(40) {
            out = format!("{}\n{}", out, c.iter().collect::<String>());
        }
        return out;
    }
}

#[cfg(test)]
mod tests {
    use super::Crt;

    #[test]
    fn noop_test() {
        let mut cycler = Crt::new();
        cycler.noop();
        assert_eq!(cycler.clock, 1);
        cycler.noop();
        cycler.noop();
        assert_eq!(cycler.clock, 3);
    }

    #[test]
    fn addx_test() {
        let mut cycler = Crt::new();
        cycler.addx(15);
        assert_eq!(cycler.clock, 2);
        assert_eq!(cycler.x, 16);
        cycler.addx(-11);
        cycler.addx(6);
        cycler.addx(-20);
        assert_eq!(cycler.clock, 8);
        assert_eq!(cycler.x, -9);
    }
}
