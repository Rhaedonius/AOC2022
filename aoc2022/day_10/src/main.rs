mod crt;
use crate::crt::Crt;

#[cfg(test)]
mod tests;

fn question1(input: &str) -> i32 {
    /*let mut tokens = input.split_whitespace();
    let mut clock = 0;
    let mut x = 1;
    let mut strength = 0;
    while let Some(s) = tokens.next() {
        clock += 1;
        if clock % 40 + 20 == 40 {
            strength += clock * x;
        }
        if let Ok(v) = s.parse::<i32>() {
            x += v;
        }
        eprintln!(
            "{}, clock: {}, x: {}, strength: {}",
            s,
            &clock,
            &x,
            &clock * &x
        );
    }
    return strength;*/
    let mut crt = Crt::new();
    for line in input.lines() {
        let mut tokens = line.split(' ');
        //eprintln!("{}", line);
        match tokens.next() {
            Some("noop") => crt.noop(),
            Some("addx") => crt.addx(tokens.next().unwrap().parse().unwrap()),
            _ => {}
        }
    }
    return crt.strength;
}

fn question2(input: &str) {
    /*let mut tokens = input.split_whitespace();
    let mut clock = 0;
    let mut x = 1;
    let mut sprite = vec![0, 1, 2];
    let mut screen = String::new();
    while let Some(s) = tokens.next() {
        // draw char
        let pos = clock % 40;
        if sprite.contains(&pos) {
            screen.push('X');
        } else {
            screen.push(' ');
        }
        clock += 1;
        // print row
        if clock % 40 == 0 {
            println!("{}", screen);
            screen = String::new();
        }
        // update sprite
        if let Ok(v) = s.parse::<i32>() {
            x += v;
            sprite = vec![x - 1, x, x + 1];
        }
    }*/
    let mut crt = Crt::new();
    for line in input.lines() {
        let mut tokens = line.split(' ');
        //eprintln!("{}", line);
        match tokens.next() {
            Some("noop") => crt.noop(),
            Some("addx") => crt.addx(tokens.next().unwrap().parse().unwrap()),
            _ => {}
        }
    }
    println!("{}", crt.print_screen());
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    question2(input);
}
