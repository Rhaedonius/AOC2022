use std::fmt::{Display, Formatter};
use std::num::ParseIntError;
use std::str::FromStr;

#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Snafu(usize);

impl std::ops::Add for Snafu {
    type Output = Snafu;

    fn add(self, rhs: Self) -> Self::Output {
        return Snafu(self.0 + rhs.0);
    }
}

impl std::iter::Sum for Snafu {
    fn sum<I: Iterator<Item = Self>>(iter: I) -> Self {
        return iter.reduce(|acc, e| acc + e).unwrap();
    }
}

impl FromStr for Snafu {
    type Err = ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut n = 0;
        for d in s.chars() {
            n = match d {
                '=' => n * 5 - 2,
                '-' => n * 5 - 1,
                '0' => n * 5,
                '1' => n * 5 + 1,
                '2' => n * 5 + 2,
                _ => return Err("".parse::<usize>().err().unwrap()),
            }
        }
        return Ok(Snafu(n));
    }
}

impl Display for Snafu {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        let mut n = self.0;
        let mut buf = String::new();
        while n > 0 {
            n += 2;
            match n % 5 {
                0 => {
                    buf = format!("={}", buf);
                }
                1 => {
                    buf = format!("-{}", buf);
                }
                2 => {
                    buf = format!("0{}", buf);
                }
                3 => {
                    buf = format!("1{}", buf);
                }
                4 => {
                    buf = format!("2{}", buf);
                }
                _ => return Err(std::fmt::Error),
            };
            n /= 5;
        }
        write!(f, "{buf}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_snafu_conversion() {
        assert_eq!("1".parse(), Ok(Snafu(1)));
        assert_eq!("2".parse(), Ok(Snafu(2)));
        assert_eq!("1=".parse(), Ok(Snafu(3)));
        assert_eq!("1-".parse(), Ok(Snafu(4)));
        assert_eq!("10".parse(), Ok(Snafu(5)));
        assert_eq!("11".parse(), Ok(Snafu(6)));
        assert_eq!("12".parse(), Ok(Snafu(7)));
        assert_eq!("2=".parse(), Ok(Snafu(8)));
        assert_eq!("2-".parse(), Ok(Snafu(9)));
        assert_eq!("20".parse(), Ok(Snafu(10)));
        assert_eq!("1=0".parse(), Ok(Snafu(15)));
        assert_eq!("1-0".parse(), Ok(Snafu(20)));
        assert_eq!("1=11-2".parse(), Ok(Snafu(2022)));
        assert_eq!("1-0---0".parse(), Ok(Snafu(12345)));
        assert_eq!("1121-1110-1=0".parse(), Ok(Snafu(314159265)));
    }

    #[test]
    fn test_snafu_display() {
        assert_eq!(format!("{}", Snafu(1747)), "1=-0-2");
        assert_eq!(format!("{}", Snafu(2022)), "1=11-2");
        assert_eq!(format!("{}", Snafu(906)), "12111");
        assert_eq!(format!("{}", Snafu(198)), "2=0=");
        assert_eq!(format!("{}", Snafu(11)), "21");
        assert_eq!(format!("{}", Snafu(1257)), "20012");
        assert_eq!(format!("{}", Snafu(32)), "112");
        assert_eq!(format!("{}", Snafu(353)), "1=-1=");
        assert_eq!(format!("{}", Snafu(107)), "1-12");
        assert_eq!(format!("{}", Snafu(7)), "12");
        assert_eq!(format!("{}", Snafu(3)), "1=");
        assert_eq!(format!("{}", Snafu(37)), "122");
        assert_eq!(format!("{}", Snafu(4890)), "2=-1=0");
    }
}
