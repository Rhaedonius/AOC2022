mod snafu;
use snafu::Snafu;

fn question1(input: &str) -> String {
    let res: Snafu = input.lines().map(|l| l.parse::<Snafu>().unwrap()).sum();
    return format!("{res}");
}

fn question2(input: &str) -> &str {
    return input;
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), "2=-1=0")
    }

    #[test]
    #[ignore]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), "")
    }
    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), "")
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), "")
    }
}
