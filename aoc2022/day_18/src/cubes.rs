use std::{fmt, num::ParseIntError, str::FromStr};

#[derive(Clone, Copy, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Cube {
    pub x: i32,
    pub y: i32,
    pub z: i32,
}

impl Cube {
    pub fn new(x: i32, y: i32, z: i32) -> Self {
        return Cube { x, y, z };
    }
    pub fn adjacent(&self) -> impl Iterator<Item = Self> {
        let (x, y, z) = (self.x, self.y, self.z);
        return [
            Cube { x: x + 1, y, z },
            Cube { x: x - 1, y, z },
            Cube { x, y: y + 1, z },
            Cube { x, y: y - 1, z },
            Cube { x, y, z: z + 1 },
            Cube { x, y, z: z - 1 },
        ]
        .into_iter();
    }
}

impl FromStr for Cube {
    type Err = ParseIntError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let mut token = input.split(",");
        let x = token.next().unwrap().parse().unwrap();
        let y = token.next().unwrap().parse().unwrap();
        let z = token.next().unwrap().parse().unwrap();
        return Ok(Cube { x, y, z });
    }
}

impl fmt::Display for Cube {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "({},{},{})", self.x, self.y, self.z)
    }
}

impl fmt::Debug for Cube {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Cube{}", self)
    }
}
