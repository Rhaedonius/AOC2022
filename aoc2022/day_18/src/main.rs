use std::collections::BTreeSet;

mod cubes;
use cubes::Cube;

#[cfg(test)]
mod tests;

type Lava = BTreeSet<Cube>;

fn free_sides(lava: &Lava, cube: &Cube) -> usize {
    return cube
        .adjacent()
        .map(|o| {
            if lava.contains(&o) {
                return 0;
            } else {
                return 1;
            }
        })
        .sum();
}

fn check_bounds(cube: &Cube) -> bool {
    return cube.x >= -1
        && cube.x <= 20
        && cube.y >= -1
        && cube.y <= 20
        && cube.z >= -1
        && cube.z <= 20;
}

fn external_sides(lava: &Lava) -> usize {
    let mut count = 0;
    let mut to_visit = Lava::from([Cube::new(0, 0, 0)]);
    let mut visited = Lava::new();
    // dijkstra-like diffusion to check external sides
    while let Some(node) = to_visit.pop_first() {
        visited.insert(node.clone());
        for c in node.adjacent() {
            // stay within the cube
            if !check_bounds(&c) {
                continue;
            }
            // if can't move in the square, it's an external side
            if lava.contains(&c) {
                count += 1;
                continue;
            }
            // add to visited to avoid loops
            if !visited.contains(&c) {
                to_visit.insert(c.clone());
            }
        }
    }
    return count;
}

fn question1(input: &str) -> usize {
    let lava = Lava::from_iter(input.lines().map(|l| l.parse().unwrap()));
    return lava.iter().map(|c| free_sides(&lava, c)).sum();
}

fn question2(input: &str) -> usize {
    let lava = Lava::from_iter(input.lines().map(|l| l.parse().unwrap()));
    return external_sides(&lava);
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod example_tests {
    use super::*;

    #[test]
    fn test_free_sides() {
        let cube = Cube::new(2, 2, 2);
        let lava = Lava::from_iter(Cube::new(2, 2, 2).adjacent());
        assert_eq!(free_sides(&lava, &cube), 0);
    }

    #[test]
    fn test_external_sides() {
        let lava = Lava::from([Cube::new(1, 1, 1)]);
        assert_eq!(external_sides(&lava), 6);
        let lava = Lava::from_iter(Cube::new(2, 2, 2).adjacent());
        assert_eq!(external_sides(&lava), 30);
    }

    #[test]
    fn test_question_1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 64);
    }

    #[test]
    fn test_question_2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 58);
    }
}
