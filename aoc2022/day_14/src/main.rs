mod path;

#[cfg(test)]
mod tests;

fn question1(input: &str) -> usize {
    let mut map = path::create_map(input);
    // let mut counter = 0;
    // while map.add_sand() {
    //     //eprint!("Grain {}:", counter + 1);
    //     counter += 1;
    // }
    let counter = map.add_sand_with_memo();
    map.draw();
    return counter;
}

fn question2(input: &str) -> usize {
    let mut map = path::create_map(input);
    map.add_infinite_floor();
    // let mut counter = 0;
    // while map.add_sand_with_infinite_floor() {
    //     //eprint!("Grain {}:", counter + 1);
    //     counter += 1;
    // }
    let counter = map.add_sand_with_memo();
    map.draw();
    return counter;
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}
