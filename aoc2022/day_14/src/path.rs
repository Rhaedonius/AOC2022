use std::collections::HashSet;

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq, Ord, PartialOrd)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(Debug, Clone)]
pub struct Map {
    start: Point,
    rocks: HashSet<Point>,
    sand: HashSet<Point>,
    floor: usize,
}

impl Map {
    fn new() -> Self {
        return Map {
            start: Point { x: 500, y: 0 },
            rocks: HashSet::new(),
            sand: HashSet::new(),
            floor: 0,
        };
    }
    fn insert_rocks(&mut self, points: impl Iterator<Item = Point>) {
        for p in points {
            self.rocks.insert(p);
            if p.y > self.floor {
                self.floor = p.y;
            }
        }
    }
    pub fn add_infinite_floor(&mut self) {
        self.floor += 2;
        let min_x = 500 - self.floor - 1;
        let max_x = 500 + self.floor + 1;
        let y = self.floor;
        for x in min_x..=max_x {
            self.rocks.insert(Point { x, y });
        }
    }
    pub fn draw(&mut self) {
        let min_x = 500 - self.floor - 1;
        let max_x = 500 + self.floor + 1;
        for y in 0..=self.floor + 1 {
            let mut row = Vec::new();
            for x in min_x..=max_x {
                let point = Point { x, y };
                row.push(match point {
                    p if self.rocks.contains(&p) => '#',
                    p if self.sand.contains(&p) => 'o',
                    _ => ' ',
                });
            }
            println!("|{}|", row.iter().collect::<String>());
        }
        println!("")
    }
    fn is_free(&self, point: &Point) -> bool {
        return !self.rocks.contains(point) && !self.sand.contains(point);
    }
    fn get_next_steps(&self, point: &Point) -> Option<Vec<Point>> {
        //eprintln!("Exploring {}, {}", point.x, point.y);
        let v: Vec<Point> = vec![
            Point {
                x: point.x + 1,
                y: point.y + 1,
            },
            Point {
                x: point.x - 1,
                y: point.y + 1,
            },
            Point {
                x: point.x,
                y: point.y + 1,
            },
        ]
        .into_iter()
        .filter(|p| self.is_free(p))
        .collect();
        if v.len() == 0 {
            return None;
        } else {
            return Some(v);
        }
    }
    pub fn add_sand_with_memo(&mut self) -> usize {
        let mut counter = 0;
        let mut path = vec![self.start];
        path.reserve(self.floor * 3);
        while path.len() > 0 {
            self.find_path(&mut path);
            let point = path.pop().unwrap();
            if point.y >= self.floor {
                break;
            }
            self.sand.insert(point);
            counter += 1;
        }
        return counter;
    }

    fn find_path(&mut self, path: &mut Vec<Point>) {
        let mut last_step = path.last().unwrap().clone();
        while let Some(mut next) = self.get_next_steps(&last_step) {
            path.append(&mut next);
            last_step = path.last().unwrap().clone();
            if last_step.y >= self.floor {
                break;
            }
        }
    }
}

fn parse_rocks(path: &str) -> Vec<Point> {
    let mut rocks = Vec::new();
    let mut iter = path.split(" -> ").map(|s| {
        let (x, y) = s.split_once(',').unwrap();
        return (x.parse().unwrap(), y.parse().unwrap());
    });
    let mut prev_point = iter.next().unwrap();
    while let Some(point) = iter.next() {
        for x in prev_point.0..=point.0 {
            for y in prev_point.1..=point.1 {
                rocks.push(Point { x, y });
            }
        }
        for x in point.0..=prev_point.0 {
            for y in point.1..=prev_point.1 {
                rocks.push(Point { x, y });
            }
        }
        prev_point = point;
    }
    return rocks;
}

pub fn create_map(input: &str) -> Map {
    let mut map = Map::new();
    for line in input.lines() {
        map.insert_rocks(parse_rocks(line).into_iter());
    }
    //map.draw_map();
    return map;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_line() {
        let path = parse_rocks("498,4 -> 498,6 -> 496,6");
        assert_eq!(path.len(), 6);
        let path2 = parse_rocks("503,4 -> 502,4 -> 502,9 -> 494,9");
        assert_eq!(path2.len(), 17);
    }

    #[test]
    fn test_create_map() {
        let map = create_map(include_str!("../test.txt"));
        assert_eq!(map.rocks.len(), 20);
    }

    #[test]
    fn test_sand() {
        let mut map = create_map(include_str!("../test.txt"));
        assert_eq!(map.add_sand_with_memo(), 24);
    }

    #[test]
    fn test_sand_with_infinite_floor() {
        let mut map = create_map(include_str!("../test.txt"));
        map.add_infinite_floor();
        assert_eq!(map.add_sand_with_memo(), 93);
    }
}
