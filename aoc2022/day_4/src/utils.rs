use std::vec::Vec;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_line_splitting() {
        let test_str = "2-4,6-8";
        let result = split_line(test_str);
        assert_eq!(result, vec![2, 4, 6, 8]);
    }

    #[test]
    fn test_inclusion() {
        assert!(!included(&vec![2, 4, 6, 8]));
        assert!(included(&vec![2, 8, 3, 7]));
        assert!(included(&vec![6, 6, 4, 6]));
    }

    #[test]
    fn test_overlap() {
        assert!(!overlaps(&vec![2, 4, 6, 8]));
        assert!(!overlaps(&vec![6, 8, 2, 4]));
        assert!(overlaps(&vec![5, 7, 7, 9]));
        assert!(overlaps(&vec![2, 8, 3, 7]));
    }
}

pub fn split_line(s: &str) -> Vec<u32> {
    s.split(",")
        .flat_map(|s| s.split("-"))
        .map(|s| s.parse::<u32>().unwrap_or(0))
        .collect::<Vec<u32>>()
}

pub fn included(a: &Vec<u32>) -> bool {
    let a_in_b = a[0] >= a[2] && a[1] <= a[3];
    let b_in_a = a[2] >= a[0] && a[3] <= a[1];
    a_in_b || b_in_a
}

pub fn overlaps(a: &Vec<u32>) -> bool {
    a[0] <= a[3] && a[1] >= a[2]
}
