#[cfg(test)]
mod tests;
mod utils;

fn question1(input: &str) -> usize {
    return input
        .lines()
        .map(utils::split_line)
        .filter(utils::included)
        .collect::<Vec<_>>()
        .len();
}

fn question2(input: &str) -> usize {
    return input
        .lines()
        .map(utils::split_line)
        .filter(utils::overlaps)
        .collect::<Vec<_>>()
        .len();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("Question 1: {}", question1(input));
    println!("Question 2: {}", question2(input));
}
