#[derive(Debug, Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Hash)]
struct Cypher {
    val: i64,
    index: usize,
}

fn parse_input(input: &str) -> Vec<Cypher> {
    return input
        .lines()
        .enumerate()
        .map(|(index, v)| {
            let val = v.parse().unwrap();
            return Cypher { val, index };
        })
        .collect();
}
fn decryption_round(text: &mut Vec<Cypher>) {
    let l = text.len();
    for index in 0..l {
        let i = text.iter().position(|c| c.index == index).unwrap();
        let offset = text[i].val;
        let insert_pos = (i as i64 + offset).rem_euclid(l as i64 - 1) as usize;
        let el = text.remove(i);
        text.insert(insert_pos, el);
    }
}

fn extract(text: &Vec<Cypher>) -> i64 {
    let start_pos = text.iter().position(|c| c.val == 0).unwrap();
    let l = text.len();
    let i = (start_pos + 1000) % l;
    let j = (start_pos + 2000) % l;
    let k = (start_pos + 3000) % l;
    return text[i].val + text[j].val + text[k].val;
}

fn question1(input: &str) -> i64 {
    let mut text = parse_input(input);
    decryption_round(&mut text);
    return extract(&text);
}

fn question2(input: &str) -> i64 {
    let mut text = parse_input(input);
    let l = text.len();
    for i in 0..l {
        text[i].val *= 811589153;
    }
    for _ in 0..10 {
        decryption_round(&mut text);
    }
    return extract(&text);
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {

    use super::*;
    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 3)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 1623178306)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 3466)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), -16002914918854)
    }
}
