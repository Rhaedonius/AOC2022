use std::collections::BTreeMap;

#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
struct Position(i32, i32);

impl std::fmt::Debug for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "Pos({}, {})", self.0, self.1)
    }
}

#[derive(Debug)]
pub enum Direction {
    Right,
    Up,
    Left,
    Down,
}

#[derive(Debug)]
pub enum Step {
    Move(i32),
    TurnRight,
    TurnLeft,
}

#[derive(Debug)]
pub struct Board {
    pos: Position,
    dir: Direction,
    board: BTreeMap<Position, char>,
}

impl Board {
    pub fn password(&self) -> i32 {
        let col = self.pos.0;
        let row = self.pos.1;
        let facing = match self.dir {
            Direction::Right => 0,
            Direction::Down => 1,
            Direction::Left => 2,
            Direction::Up => 3,
        };
        return 1000 * row + 4 * col + facing;
    }
    fn wrap(&self, pos: Position) -> Position {
        let new_pos = match self.dir {
            Direction::Right => {
                let min_x = self.board.keys().filter(|k| k.1 == pos.1).min().unwrap().0;
                Position(min_x, pos.1)
            }
            Direction::Up => {
                let max_y = self.board.keys().filter(|k| k.0 == pos.0).max().unwrap().1;
                Position(pos.0, max_y)
            }
            Direction::Left => {
                let max_x = self.board.keys().filter(|k| k.1 == pos.1).max().unwrap().0;
                Position(max_x, pos.1)
            }
            Direction::Down => {
                let min_y = self.board.keys().filter(|k| k.0 == pos.0).min().unwrap().1;
                Position(pos.0, min_y)
            }
        };
        match self.board.get(&new_pos) {
            Some('#') => return pos,
            Some('.') => return new_pos,
            _ => panic!("Unknown position {:?}", new_pos),
        }
    }
    pub fn move_right(&mut self, steps: i32) {
        for _ in 0..steps {
            let new_pos = Position(self.pos.0 + 1, self.pos.1);
            match self.board.get(&new_pos) {
                Some('#') => break, // found wall
                Some('.') => {
                    self.pos = new_pos;
                }
                None => self.pos = self.wrap(self.pos),
                _ => panic!("Unknown char from board in move_right"),
            }
        }
    }
    pub fn move_up(&mut self, steps: i32) {
        for _ in 0..steps {
            let new_pos = Position(self.pos.0, self.pos.1 - 1);
            match self.board.get(&new_pos) {
                Some('#') => break, // found wall
                Some('.') => {
                    self.pos = new_pos;
                }
                None => self.pos = self.wrap(self.pos),
                _ => panic!("Unknown char from board in move_up"),
            }
        }
    }
    pub fn move_left(&mut self, steps: i32) {
        for _ in 0..steps {
            let new_pos = Position(self.pos.0 - 1, self.pos.1);
            match self.board.get(&new_pos) {
                Some('#') => break, // found wall
                Some('.') => {
                    self.pos = new_pos;
                }
                None => self.pos = self.wrap(self.pos),
                _ => panic!("Unknown char from board in move_left"),
            }
        }
    }
    pub fn move_down(&mut self, steps: i32) {
        for _ in 0..steps {
            let new_pos = Position(self.pos.0, self.pos.1 + 1);
            match self.board.get(&new_pos) {
                Some('#') => break, // found wall
                Some('.') => {
                    self.pos = new_pos;
                }
                None => self.pos = self.wrap(self.pos),
                _ => panic!("Unknown char from board in move_down"),
            }
        }
    }
    pub fn apply(&mut self, step: Step) {
        eprintln!("{:?}, {:?}, {:?}", &self.pos, &self.dir, &step);
        match step {
            Step::Move(n) => match self.dir {
                Direction::Right => self.move_right(n),
                Direction::Up => self.move_up(n),
                Direction::Left => self.move_left(n),
                Direction::Down => self.move_down(n),
            },
            Step::TurnRight => match self.dir {
                Direction::Right => {
                    self.dir = Direction::Down;
                }
                Direction::Up => self.dir = Direction::Right,
                Direction::Left => self.dir = Direction::Up,
                Direction::Down => self.dir = Direction::Left,
            },
            Step::TurnLeft => match self.dir {
                Direction::Right => {
                    self.dir = Direction::Up;
                }
                Direction::Up => self.dir = Direction::Left,
                Direction::Left => self.dir = Direction::Down,
                Direction::Down => self.dir = Direction::Right,
            },
        }
    }
}

impl std::str::FromStr for Board {
    type Err = std::convert::Infallible;
    fn from_str(input: &str) -> Result<Board, std::convert::Infallible> {
        let mut board = BTreeMap::new();
        input.lines().enumerate().for_each(|(j, l)| {
            l.chars().enumerate().for_each(|(i, c)| {
                match c {
                    '#' => {
                        board.insert(Position(i as i32 + 1, j as i32 + 1), '#');
                    }
                    '.' => {
                        board.insert(Position(i as i32 + 1, j as i32 + 1), '.');
                    }
                    _ => {}
                };
            })
        });
        let pos = board.keys().filter(|k| k.1 == 1).min().unwrap().to_owned();
        dbg!(pos);
        let dir = Direction::Right;
        return Ok(Board { pos, dir, board });
    }
}
