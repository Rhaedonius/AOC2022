mod board;
use board::Board;
use board::Step;

fn parse_path(input: &str) -> Vec<Step> {
    let mut path: Vec<Step> = input
        .split_inclusive(['R', 'L'])
        .flat_map(|l| {
            let mut s = l.to_owned();
            let t = match s.pop() {
                Some('R') => Step::TurnRight,
                Some('L') => Step::TurnLeft,
                _ => Step::TurnRight,
            };
            match s.parse() {
                Ok(n) => return [Step::Move(n), t],
                Err(_) => panic!(),
            }
        })
        .collect();
    path.pop();
    return path;
}

fn question1(input: &str) -> i32 {
    let (part1, part2) = input.split_once("\n\n").unwrap();
    let mut board = part1.parse::<Board>().unwrap();
    let path = parse_path(part2);
    for step in path {
        board.apply(step);
    }
    return board.password();
}

fn question2(input: &str) -> usize {
    return input.len();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 6032)
    }

    #[test]
    #[ignore]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 0)
    }
    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 88226)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 0)
    }
}
