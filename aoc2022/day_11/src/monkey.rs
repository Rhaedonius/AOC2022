use std::collections::VecDeque;

#[derive(Debug, Clone)]
pub struct Monkey {
    count: usize,
    items: VecDeque<usize>,
    operators: (char, String),
    pub test: usize,
    if_true: usize,
    if_false: usize,
}

impl Monkey {
    pub fn from(input: &str) -> Self {
        let mut lines = input.lines();
        let mut buf;
        let mut items: VecDeque<usize> = VecDeque::new();
        buf = lines.next().unwrap();
        buf = buf.strip_prefix("  Starting items: ").unwrap();
        for obj in buf.split(", ") {
            let item = obj.parse().unwrap();
            items.push_back(item);
        }
        buf = lines.next().unwrap();
        buf = buf.strip_prefix("  Operation: new = old ").unwrap();
        let (op, num) = buf.split_once(' ').unwrap();
        let operators = (op.chars().next().unwrap(), num.to_owned());
        buf = lines.next().unwrap();
        buf = buf.strip_prefix("  Test: divisible by ").unwrap();
        let test = buf.parse().unwrap();
        buf = lines.next().unwrap();
        buf = buf.strip_prefix("    If true: throw to monkey ").unwrap();
        let if_true = buf.parse().unwrap();
        buf = lines.next().unwrap();
        buf = buf.strip_prefix("    If false: throw to monkey ").unwrap();
        let if_false = buf.parse().unwrap();
        lines.next();
        return Monkey {
            count: 0,
            items,
            operators,
            test,
            if_true,
            if_false,
        };
    }
    pub fn inspect_items(&mut self) -> Option<(usize, usize)> {
        if let Some(i) = self.items.pop_front() {
            self.count += 1;
            let new_i = self.operation(i) / 3;
            if new_i % self.test == 0 {
                return Some((self.if_true, new_i));
            } else {
                return Some((self.if_false, new_i));
            }
        }
        return None;
    }
    pub fn anxiously_inspect_items(&mut self, modulo: usize) -> Option<(usize, usize)> {
        if let Some(i) = self.items.pop_front() {
            self.count += 1;
            let new_i = self.operation(i) % modulo;
            if new_i % self.test == 0 {
                return Some((self.if_true, new_i));
            } else {
                return Some((self.if_false, new_i));
            }
        }
        return None;
    }
    fn operation(&self, item: usize) -> usize {
        let (op, num) = &self.operators;
        match op {
            '+' => {
                return item + num.parse::<usize>().unwrap();
            }
            '*' => {
                return item
                    * match num.as_str() {
                        "old" => item,
                        n => n.parse().unwrap(),
                    };
            }
            _ => return item,
        };
    }
    pub fn monkey_business(&self) -> usize {
        return self.count;
    }
    pub fn add_object(&mut self, item: usize) {
        self.items.push_back(item);
    }
}

#[cfg(test)]
mod tests {
    use crate::parse_input;

    #[test]
    fn test_one_round() {
        let input = include_str!("../test.txt");
        let mut monkeys = parse_input(input);
        let mut worry;
        for i in 0..monkeys.len() {
            eprintln!("Monkey {}:", i);
            while let Some(thrown_obj) = monkeys[i].items.front() {
                eprintln!(
                    "  Monkey inspects an item with a worry level of {}.",
                    thrown_obj,
                );
                worry = monkeys[i].operation(*thrown_obj);
                eprintln!("    Worry level is increased to {}.", worry);
                worry /= 3;
                eprintln!(
                    "    Monkey gets bored with item. Worry level is divided by 3 to {}",
                    worry
                );
                if worry % monkeys[i].test == 0 {
                    eprintln!(
                        "    Current worry level is divisible by {}.",
                        monkeys[i].test
                    );
                    eprintln!(
                        "    Item with worry level {} is thrown to monkey {}.",
                        worry, monkeys[i].if_true
                    );
                } else {
                    eprintln!(
                        "    Current worry level is not divisible by {}.",
                        monkeys[i].test
                    );
                    eprintln!(
                        "    Item with worry level {} is thrown to monkey {}.",
                        worry, monkeys[i].if_false
                    );
                }
                let (next_monkey, item) = monkeys[i].inspect_items().unwrap();
                monkeys[next_monkey].add_object(item);
            }
            eprintln!("");
        }
        assert_eq!(monkeys[0].items, vec![20, 23, 27, 26]);
        assert_eq!(monkeys[1].items, vec![2080, 25, 167, 207, 401, 1046]);
        assert_eq!(monkeys[2].items.len(), 0);
        assert_eq!(monkeys[3].items.len(), 0);
    }

    #[test]
    fn test_10k_rounds() {
        let input = include_str!("../test.txt");
        let mut monkeys = parse_input(input);
        let modulo: usize = monkeys.iter().map(|m| m.test).product();
        for round in 1..=10000 {
            for i in 0..monkeys.len() {
                while let Some(thrown_obj) = monkeys[i].anxiously_inspect_items(modulo) {
                    let (next_monkey, obj) = thrown_obj;
                    monkeys[next_monkey].add_object(obj);
                }
            }
            if vec![
                1, 20, 1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000,
            ]
            .contains(&round)
            {
                eprintln!(
                    "After round {}, the monkeys are holding items with these worry levels:",
                    round
                );
                for i in 0..monkeys.len() {
                    eprint!("Monkey {}:", i);
                    eprintln!("inspected items {} times.", monkeys[i].monkey_business())
                }
                eprintln!("");
            }
        }
        assert_eq!(monkeys[0].count, 52166);
        assert_eq!(monkeys[1].count, 47830);
        assert_eq!(monkeys[2].count, 1938);
        assert_eq!(monkeys[3].count, 52013);
    }
}
