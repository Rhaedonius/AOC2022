mod monkey;

#[cfg(test)]
mod tests;

fn parse_input(input: &str) -> Vec<monkey::Monkey> {
    let mut lines = input.lines();
    let mut monkeys: Vec<monkey::Monkey> = Vec::new();
    while let Some(buf) = lines.next() {
        if buf.starts_with("Monkey ") {
            let mut s = String::new();
            for _ in 0..4 {
                s.push_str(lines.next().unwrap());
                s.push('\n');
            }
            s.push_str(lines.next().unwrap());
            monkeys.push(monkey::Monkey::from(&s));
        }
    }
    return monkeys;
}

fn question1(input: &str) -> usize {
    let mut monkeys = parse_input(input);
    let num_monkeys = monkeys.len();
    for _ in 1..=20 {
        for i in 0..num_monkeys {
            while let Some(thrown_obj) = monkeys[i].inspect_items() {
                let (next_monkey, obj) = thrown_obj;
                monkeys[next_monkey].add_object(obj);
            }
        }
    }
    let mut monkey_business: Vec<usize> =
        monkeys.into_iter().map(|m| m.monkey_business()).collect();
    monkey_business.sort_by(|a, b| b.cmp(a));
    return monkey_business.into_iter().take(2).product();
}

fn question2(input: &str) -> usize {
    let mut monkeys = parse_input(input);
    let num_monkeys = monkeys.len();
    let modulo: usize = monkeys.iter().map(|m| m.test).product();
    for _ in 1..=10000 {
        for i in 0..num_monkeys {
            while let Some(thrown_obj) = monkeys[i].anxiously_inspect_items(modulo) {
                let (next_monkey, obj) = thrown_obj;
                monkeys[next_monkey].add_object(obj);
            }
        }
    }
    let mut monkey_business: Vec<usize> =
        monkeys.into_iter().map(|m| m.monkey_business()).collect();
    monkey_business.sort_by(|a, b| b.cmp(a));
    return monkey_business.into_iter().take(2).product();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}
