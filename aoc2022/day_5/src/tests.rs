use crate::{board::Board, question1, question2};

#[test]
fn test_question1() {
    let input = include_str!("../input.txt");
    let (board_raw, moves) = input.split_once("\n\n").unwrap();
    let mut board = Board::new(board_raw);
    assert_eq!(question1(&mut board, moves), String::from("VRWBSFZWM"));
}

#[test]
fn test_question2() {
    let input = include_str!("../input.txt");
    let (board_raw, moves) = input.split_once("\n\n").unwrap();
    let mut board = Board::new(board_raw);
    assert_eq!(question2(&mut board, moves), String::from("RBTWJWMCF"));
}
