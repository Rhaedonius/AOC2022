mod board;
#[cfg(test)]
mod tests;

fn question1(board: &mut board::Board, input: &str) -> String {
    for line in input.lines() {
        board.apply(line)
    }
    return board.get_top();
}

fn question2(board: &mut board::Board, input: &str) -> String {
    for line in input.lines() {
        board.apply_multi(line)
    }
    return board.get_top();
}

fn main() {
    let input = include_str!("../input.txt");
    let (board_raw, moves) = input.split_once("\n\n").unwrap();
    let mut board1 = board::Board::new(board_raw);
    let mut board2 = board1.clone();
    println!("Question 1: {}", question1(&mut board1, moves));
    println!("Question 2: {}", question2(&mut board2, moves));
}
