use std::fmt;
use std::vec::Vec;

fn parse_line(line: &str) -> Vec<char> {
    let mut v: Vec<char> = Vec::new();
    for (idx, c) in line.chars().enumerate() {
        if idx % 4 == 1 && !c.is_numeric() {
            v.push(c);
        }
    }
    return v;
}

fn transpose_and_filter(v: BoardStatus) -> BoardStatus {
    assert!(!v.is_empty());
    let len = v[0].len();
    let mut iters: Vec<_> = v.into_iter().map(|c| c.into_iter()).collect();
    (0..len)
        .map(|_| {
            iters
                .iter_mut()
                .rev()
                .map(|c| c.next().unwrap())
                .filter(|c| !c.is_whitespace())
                .collect::<Vec<_>>()
        })
        .collect()
}

pub type BoardStatus = Vec<Vec<char>>;

pub struct Board {
    status: BoardStatus,
}

impl Board {
    pub fn new(board_input: &str) -> Board {
        let status: BoardStatus = board_input
            .lines()
            .map(parse_line)
            .filter(|v| !v.is_empty())
            .collect();
        Board {
            status: transpose_and_filter(status),
        }
    }

    pub fn apply(&mut self, input: &str) {
        let input_tokens: Vec<_> = input.split_whitespace().collect();
        let n: usize = input_tokens[1].parse().unwrap();
        let fr: usize = input_tokens[3].parse::<usize>().unwrap() - 1;
        let to: usize = input_tokens[5].parse::<usize>().unwrap() - 1;
        for _ in 0..n {
            let c: char = self.status[fr].pop().unwrap();
            self.status[to].push(c);
        }
    }

    pub fn apply_multi(&mut self, input: &str) {
        let input_tokens: Vec<_> = input.split_whitespace().collect();
        let n: usize = input_tokens[1].parse().unwrap();
        let fr: usize = input_tokens[3].parse::<usize>().unwrap() - 1;
        let to: usize = input_tokens[5].parse::<usize>().unwrap() - 1;
        let len = self.status[fr].len();
        let mut v: Vec<char> = self.status[fr].split_off(len - n);
        self.status[to].append(&mut v);
    }

    pub fn get_top(&self) -> String {
        let mut top: Vec<char> = Vec::new();
        for v in &self.status {
            top.push(v.last().unwrap().clone());
        }
        String::from_iter(top)
    }
}

impl fmt::Debug for Board {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Board")
            .field("status", &self.status)
            .finish()
    }
}

impl Clone for Board {
    fn clone(&self) -> Self {
        Board {
            status: self.status.clone(),
        }
    }
}
