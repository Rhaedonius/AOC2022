// use std::time::Instant;

use crate::{question1, question2};

#[test]
fn test_example_question1() {
    let input = include_str!("../test.txt");
    assert_eq!(question1(input), 33)
}

#[test]
fn test_example_question2() {
    let input = include_str!("../test.txt");
    assert_eq!(question2(input), 62)
}

#[test]
#[ignore]
fn test_question1() {
    let input = include_str!("../input.txt");
    assert_eq!(question1(input), 0)
}

#[test]
#[ignore]
fn test_question2() {
    let input = include_str!("../input.txt");
    assert_eq!(question2(input), 0)
}
