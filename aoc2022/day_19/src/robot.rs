use std::num::ParseIntError;
use std::ops::{Add, Sub};
use std::str::FromStr;

pub enum Action {
    Nothing,
    BuildOreRobot(Currency),
    BuildClayRobot(Currency),
    BuildObsidianRobot(Currency),
    BuildGeodeRobot(Currency),
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct Currency {
    pub geode: i32,
    pub obsidian: i32,
    pub clay: i32,
    pub ore: i32,
}

impl Currency {
    pub fn new() -> Self {
        return Self {
            ore: 0,
            clay: 0,
            obsidian: 0,
            geode: 0,
        };
    }
    pub fn can_buy(&self, other: &Self) -> bool {
        return self.ore >= other.ore
            && self.clay >= other.clay
            && self.obsidian >= other.obsidian
            && self.geode >= other.geode;
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Blueprint {
    pub id: i32,
    pub ore_robot: Currency,
    pub clay_robot: Currency,
    pub obsidian_robot: Currency,
    pub geode_robot: Currency,
}

impl Blueprint {
    pub fn max_robots(&self) -> State {
        return State {
            ore_robot: [
                self.ore_robot.ore,
                self.clay_robot.ore,
                self.obsidian_robot.ore,
                self.geode_robot.ore,
            ]
            .into_iter()
            .max()
            .unwrap(),
            clay_robot: self.obsidian_robot.clay,
            obsidian_robot: self.geode_robot.obsidian,
            geode_robot: 0,
        };
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash, PartialOrd, Ord)]
pub struct State {
    pub ore_robot: i32,
    pub clay_robot: i32,
    pub obsidian_robot: i32,
    pub geode_robot: i32,
}

impl State {
    pub fn new() -> Self {
        return State {
            ore_robot: 1,
            clay_robot: 0,
            obsidian_robot: 0,
            geode_robot: 0,
        };
    }

    pub fn get_actions(&self, budget: &Currency, bp: &Blueprint) -> Vec<Action> {
        let mut actions = Vec::new();
        if budget.can_buy(&bp.geode_robot) {
            actions.push(Action::BuildGeodeRobot(bp.geode_robot));
        }
        if budget.can_buy(&bp.obsidian_robot) {
            actions.push(Action::BuildObsidianRobot(bp.obsidian_robot));
        }
        if budget.can_buy(&bp.clay_robot) {
            actions.push(Action::BuildClayRobot(bp.clay_robot));
        }
        if budget.can_buy(&bp.ore_robot) {
            actions.push(Action::BuildOreRobot(bp.ore_robot));
        }
        actions.push(Action::Nothing);
        return actions;
    }

    pub fn collect_resources(&self, budget: Currency) -> Currency {
        return budget
            + Currency {
                ore: self.ore_robot,
                clay: self.clay_robot,
                obsidian: self.obsidian_robot,
                geode: self.geode_robot,
            };
    }

    pub fn update(&self, budget: &Currency, action: &Action) -> (Self, Currency) {
        let mut new_state = self.clone();
        let mut new_budget = budget.clone();
        match action {
            Action::Nothing => {}
            Action::BuildOreRobot(c) => {
                new_state.ore_robot += 1;
                new_budget = *budget - *c;
            }
            Action::BuildClayRobot(c) => {
                new_state.clay_robot += 1;
                new_budget = *budget - *c;
            }
            Action::BuildObsidianRobot(c) => {
                new_state.obsidian_robot += 1;
                new_budget = *budget - *c;
            }
            Action::BuildGeodeRobot(c) => {
                new_state.geode_robot += 1;
                new_budget = *budget - *c;
            }
        }
        return (new_state, new_budget);
    }

    pub fn turn(&mut self, mut budget: Currency, bp: &Blueprint) -> Vec<(State, Currency)> {
        let actions = self.get_actions(&budget, bp);
        budget = self.collect_resources(budget);
        return actions.iter().map(|a| self.update(&budget, a)).collect();
    }
}

impl std::fmt::Display for Currency {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "{} ore, {} clay, {} obsidian and {} geodes",
            self.ore, self.clay, self.obsidian, self.geode
        )
    }
}

impl std::fmt::Display for State {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "You have {} ore_robot, {} clay_robot, {} obsidian_robot and {} geode_robot",
            self.ore_robot, self.clay_robot, self.obsidian_robot, self.geode_robot
        )
    }
}

impl Add for Currency {
    type Output = Self;
    fn add(self, other: Self) -> Self {
        return Currency {
            ore: self.ore + other.ore,
            clay: self.clay + other.clay,
            obsidian: self.obsidian + other.obsidian,
            geode: self.geode + other.geode,
        };
    }
}

impl Sub for Currency {
    type Output = Self;
    fn sub(self, other: Self) -> Self {
        return Currency {
            ore: self.ore - other.ore,
            clay: self.clay - other.clay,
            obsidian: self.obsidian - other.obsidian,
            geode: self.geode - other.geode,
        };
    }
}

impl FromStr for Currency {
    type Err = ParseIntError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        fn convert_curr(input: &str) -> Currency {
            let curr = match input.split_once(" ") {
                Some((n, "ore")) => Currency {
                    ore: n.parse().unwrap(),
                    clay: 0,
                    obsidian: 0,
                    geode: 0,
                },
                Some((n, "clay")) => Currency {
                    ore: 0,
                    clay: n.parse().unwrap(),
                    obsidian: 0,
                    geode: 0,
                },
                Some((n, "obsidian")) => Currency {
                    ore: 0,
                    clay: 0,
                    obsidian: n.parse().unwrap(),
                    geode: 0,
                },
                Some((n, "geode")) => Currency {
                    ore: 0,
                    clay: 0,
                    obsidian: 0,
                    geode: n.parse().unwrap(),
                },
                _ => Currency {
                    ore: 0,
                    clay: 0,
                    obsidian: 0,
                    geode: 0,
                },
            };
            return curr;
        }
        return Ok(input
            .split(", ")
            .flat_map(|s| s.split(" and "))
            .map(|s| convert_curr(s))
            .reduce(|acc, c| acc + c)
            .unwrap());
    }
}

impl FromStr for Blueprint {
    type Err = ParseIntError;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let (num, remainder) = input
            .strip_prefix("Blueprint ")
            .unwrap()
            .split_once(": ")
            .unwrap();
        let id = num.parse().unwrap();
        let (curr, remainder) = remainder
            .strip_prefix("Each ore robot costs ")
            .unwrap()
            .split_once(". ")
            .unwrap();
        let ore_robot = curr.parse().unwrap();
        let (curr, remainder) = remainder
            .strip_prefix("Each clay robot costs ")
            .unwrap()
            .split_once(". ")
            .unwrap();
        let clay_robot = curr.parse().unwrap();
        let (curr, remainder) = remainder
            .strip_prefix("Each obsidian robot costs ")
            .unwrap()
            .split_once(". ")
            .unwrap();
        let obsidian_robot = curr.parse().unwrap();
        let (curr, _) = remainder
            .strip_prefix("Each geode robot costs ")
            .unwrap()
            .split_once(".")
            .unwrap();
        let geode_robot = curr.parse().unwrap();
        return Ok(Blueprint {
            id,
            ore_robot,
            clay_robot,
            obsidian_robot,
            geode_robot,
        });
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_blueprint_parse() {
        let input = "Blueprint 1: Each ore robot costs 4 ore. \
                     Each clay robot costs 3 ore. \
                     Each obsidian robot costs 2 ore and 14 clay. \
                     Each geode robot costs 4 ore and 11 obsidian.";
        assert_eq!(
            input.parse::<Blueprint>().unwrap(),
            Blueprint {
                id: 1,
                ore_robot: Currency {
                    ore: 4,
                    clay: 0,
                    obsidian: 0,
                    geode: 0
                },
                clay_robot: Currency {
                    ore: 3,
                    clay: 0,
                    obsidian: 0,
                    geode: 0
                },
                obsidian_robot: Currency {
                    ore: 2,
                    clay: 14,
                    obsidian: 0,
                    geode: 0
                },
                geode_robot: Currency {
                    ore: 4,
                    clay: 0,
                    obsidian: 11,
                    geode: 0
                }
            }
        )
    }
}
