mod robot;

use std::collections::HashMap;

use robot::{Blueprint, Currency, State};

#[cfg(test)]
mod tests;

fn max_geodes(bp: Blueprint, max_time: i32) -> i32 {
    let mut state = HashMap::from([(State::new(), Box::new(vec![Currency::new()]))]);
    let mut max_geodes = 0;
    let mut max_delta = 0;
    for minute in 1..=max_time {
        state = state
            .into_iter()
            .flat_map(|(mut s, b)| {
                // branch to all possible actions
                b.into_iter()
                    .map(move |b| std::thread::spawn(move || s.turn(b, &bp)))
            })
            .flat_map(|j| j.join().unwrap())
            .fold(
                HashMap::<State, Box<Vec<Currency>>>::new(),
                |mut acc, (s, b)| {
                    // prune if too many robot of the same type (produce more than can use)
                    if s > bp.max_robots()
                        // prune if solutions is not growing enough
                        || (s.geode_robot < max_delta && b.geode + max_delta < max_geodes)
                    {
                        return acc;
                    }
                    acc.entry(s)
                        .and_modify(|v| {
                            // check if this solution is worse than others
                            // same robots but less of all the currencies
                            let mut to_insert = true;
                            let mut curr = Vec::new();
                            for e in v.iter() {
                                if b.can_buy(&e) && to_insert {
                                    curr.push(b);
                                    to_insert = false;
                                } else {
                                    curr.push(*e)
                                }
                                if e.can_buy(&b) {
                                    to_insert = false;
                                }
                            }
                            if to_insert {
                                curr.push(b);
                            }
                            *v = Box::new(curr);
                        })
                        .or_insert(Box::new(vec![b]));
                    return acc;
                },
            );
        let new_max_geodes = state
            .values()
            .flat_map(|b| b.iter().map(|b| b.geode))
            .max()
            .unwrap_or(0);
        max_delta = new_max_geodes - max_geodes;
        max_geodes = new_max_geodes;
        eprintln!(
            "Blueprint {}, Minute {minute}: max={max_geodes} - {} cases",
            bp.id,
            &state.len()
        );
    }
    return max_geodes;
}

fn question1(input: &str) -> i32 {
    let blueprints: Vec<Blueprint> = input.lines().map(|s| s.parse().unwrap()).collect();
    return blueprints
        .into_iter()
        .map(|b| b.id * max_geodes(b, 24))
        .sum();
}

fn question2(input: &str) -> i32 {
    let blueprints: Vec<Blueprint> = input.lines().take(3).map(|s| s.parse().unwrap()).collect();
    return blueprints.into_iter().map(|b| max_geodes(b, 32)).product();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}
