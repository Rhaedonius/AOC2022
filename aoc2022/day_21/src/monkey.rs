use std::collections::{HashMap, HashSet};

#[derive(Clone)]
enum Job {
    Number(i64),
    Operation(String),
}

pub struct MonkeyMap(HashMap<String, Monkey>);

pub struct Monkey {
    job: Job,
    left: String,
    right: String,
}

impl Monkey {
    pub fn from_num(n: i64) -> Self {
        return Self {
            job: Job::Number(n),
            left: String::new(),
            right: String::new(),
        };
    }
    pub fn from(left: &str, op: &str, right: &str) -> Self {
        return Self {
            job: Job::Operation(op.to_owned()),
            left: left.to_owned(),
            right: right.to_owned(),
        };
    }
}

impl MonkeyMap {
    pub fn yell(&self, key: &str) -> i64 {
        let m = self.0.get(key).unwrap();
        return match &m.job {
            Job::Number(n) => *n,
            Job::Operation(op) => {
                return match op.as_str() {
                    "+" => self.yell(&m.left) + self.yell(&m.right),
                    "-" => self.yell(&m.left) - self.yell(&m.right),
                    "*" => self.yell(&m.left) * self.yell(&m.right),
                    "/" => self.yell(&m.left) / self.yell(&m.right),
                    _ => panic!("Unknown operation"),
                };
            }
        };
    }

    pub fn find_human(&self) -> Vec<&str> {
        let mut stack = Vec::new();
        let mut visited = HashSet::from(["root"]);
        let mut curr = "root";
        while dbg!(curr) != "humn" {
            let monkey = self.0.get(curr).unwrap();
            match &monkey.job {
                Job::Number(_) => {
                    stack.pop();
                }
                Job::Operation(_) => {
                    let right = monkey.right.as_str();
                    let left = monkey.left.as_str();
                    if !visited.contains(&left) {
                        visited.insert(&left);
                        stack.push(left);
                    } else if !visited.contains(right) {
                        visited.insert(&right);
                        stack.push(right);
                    } else {
                        stack.pop();
                    }
                }
            }
            curr = stack.last().unwrap();
        }
        return stack;
    }

    pub fn get_left(&self, key: &str) -> String {
        return self.0.get(key).unwrap().left.clone();
    }

    pub fn get_right(&self, key: &str) -> String {
        return self.0.get(key).unwrap().right.clone();
    }

    pub fn get_op(&self, key: &str) -> String {
        if let Job::Operation(op) = self.0.get(key).unwrap().job.clone() {
            return op;
        }
        return String::new();
    }
}

impl FromIterator<(String, Monkey)> for MonkeyMap {
    fn from_iter<T: IntoIterator<Item = (String, Monkey)>>(iter: T) -> Self {
        return MonkeyMap(HashMap::from_iter(iter));
    }
}
