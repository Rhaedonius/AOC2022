mod monkey;
use monkey::Monkey;
use monkey::MonkeyMap;

fn parse_input(input: &str) -> MonkeyMap {
    return input
        .lines()
        .map(|l| {
            let (key, value) = l.split_once(": ").unwrap();
            match value.parse::<i64>() {
                Ok(n) => (key.to_owned(), Monkey::from_num(n)),
                Err(_) => {
                    let (left, rem) = value.split_once(' ').unwrap();
                    let (op, right) = rem.split_once(' ').unwrap();
                    return (key.to_owned(), Monkey::from(left, op, right));
                }
            }
        })
        .collect();
}

fn invert_op_left(res: i64, op: &str, val: i64) -> i64 {
    return match op {
        "+" => res - val,
        "-" => res + val,
        "*" => res / val,
        "/" => res * val,
        _ => panic!("Unknown operation"),
    };
}

fn invert_op_right(res: i64, op: &str, val: i64) -> i64 {
    return match op {
        "+" => res - val,
        "-" => val - res,
        "*" => res / val,
        "/" => val / res,
        _ => panic!("Unknown operation"),
    };
}

fn question1(input: &str) -> i64 {
    let monkeys = parse_input(input);
    return monkeys.yell("root");
}

fn question2(input: &str) -> i64 {
    let monkeys = parse_input(input);
    let mut prev = "root";
    let mut result = 0;
    // find_human uses DFS to get the path to "humn"
    for m in monkeys.find_human() {
        let left = monkeys.get_left(prev);
        let right = monkeys.get_right(prev);
        let op = monkeys.get_op(prev);
        // we use yell to get the value of the other side
        // and then invert the operation accordingly
        if m == left {
            let val = monkeys.yell(&right);
            if prev == "root" {
                result = val;
            } else {
                result = invert_op_left(result, &op, val);
            }
        } else if m == right {
            let val = monkeys.yell(&left);
            if prev == "root" {
                result = val;
            } else {
                result = invert_op_right(result, &op, val);
            }
        } else {
            panic!("prev is not in left or right")
        }
        dbg!(prev, result);
        prev = m;
    }
    return result;
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 152)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 301)
    }
    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 223971851179174)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 3379022190351)
    }
}
