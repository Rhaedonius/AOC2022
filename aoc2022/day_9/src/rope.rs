use std::collections::HashSet;

type Position = (u16, u16);

#[derive(Debug, Clone)]
pub struct Rope {
    len: usize,
    nodes: Vec<Position>,
    positions: HashSet<Position>,
}

impl Rope {
    pub fn new(len: usize) -> Self {
        // moving zero to avoid underflow
        let start_pos: Position = (1 << 15, 1 << 15);
        return Rope {
            len,
            nodes: vec![start_pos; len],
            positions: HashSet::from([start_pos]),
        };
    }
    pub fn head(&self) -> Position {
        return self.nodes[0];
    }

    pub fn tail(&self) -> Position {
        return self.nodes[self.len - 1];
    }

    fn length(&self, node: usize) -> u16 {
        let x = u16::abs_diff(self.nodes[node - 1].0, self.nodes[node].0);
        let y = u16::abs_diff(self.nodes[node - 1].1, self.nodes[node].1);
        return u16::max(x, y);
    }

    pub fn next_move(&mut self, dir: &str, step: u16) {
        for _ in 0..step {
            self.move_head(dir);
            for n in 1..self.len {
                self.move_node(n);
            }
            self.positions.insert(self.tail());
            /*eprintln!(
                "move: {} {}, head: ({}, {}), tail: ({}, {}), positions: {}",
                dir,
                step,
                self.head.0,
                self.head.1,
                self.tail.0,
                self.tail.1,
                self.positions.len()
            );*/
        }
    }

    fn move_head(&mut self, dir: &str) {
        self.nodes[0] = match dir {
            "U" => (self.head().0, self.head().1 + 1),
            "D" => (self.head().0, self.head().1 - 1),
            "L" => (self.head().0 - 1, self.head().1),
            "R" => (self.head().0 + 1, self.head().1),
            _ => (self.head().0, self.head().1),
        }
    }
    fn move_node(&mut self, n: usize) {
        if self.length(n) < 2 {
            return;
        }
        if self.nodes[n - 1].0 > self.nodes[n].0 {
            self.nodes[n].0 += 1;
        }
        if self.nodes[n - 1].0 < self.nodes[n].0 {
            self.nodes[n].0 -= 1
        }
        if self.nodes[n - 1].1 > self.nodes[n].1 {
            self.nodes[n].1 += 1;
        }
        if self.nodes[n - 1].1 < self.nodes[n].1 {
            self.nodes[n].1 -= 1;
        }
    }
    pub fn visited_positions(&self) -> &HashSet<Position> {
        return &self.positions;
    }
}

#[cfg(test)]
mod unit_tests {
    use std::collections::HashSet;

    use super::{Position, Rope};

    fn make_rope(start_pos: Position) -> Rope {
        return Rope {
            len: 2,
            nodes: vec![start_pos; 2],
            positions: HashSet::from([start_pos]),
        };
    }
    #[test]
    fn test_movement() {
        let mut rope = make_rope((128, 128));
        rope.next_move("R", 4);
        assert_eq!(rope.head(), (132, 128));
        assert_eq!(rope.tail(), (131, 128));
        assert_eq!(
            rope.positions,
            HashSet::from([(128, 128), (129, 128), (130, 128), (131, 128)])
        );
        rope.next_move("U", 4);
        assert_eq!(rope.head(), (132, 132));
        assert_eq!(rope.tail(), (132, 131));
        assert_eq!(
            rope.positions,
            HashSet::from([
                (128, 128),
                (129, 128),
                (130, 128),
                (131, 128),
                (132, 129),
                (132, 130),
                (132, 131)
            ])
        );
    }

    #[test]
    fn test_move_head() {
        let mut rope = make_rope((128, 128));
        rope.move_head("U");
        assert_eq!(rope.head(), (128, 129));
        rope.move_head("U");
        assert_eq!(rope.head(), (128, 130));
        rope.move_head("R");
        assert_eq!(rope.head(), (129, 130));
        rope.move_head("L");
        assert_eq!(rope.head(), (128, 130));
        rope.move_head("D");
        assert_eq!(rope.head(), (128, 129));
    }

    #[test]
    fn test_move_tail_same_row() {
        let mut rope = make_rope((128, 128));
        rope.nodes[0] = (131, 129);
        rope.nodes[1] = (129, 129);
        rope.move_node(1);
        assert_eq!(rope.nodes[1], (130, 129));
        assert!(rope.length(1) < 2);
    }
    #[test]
    fn test_move_tail_same_col() {
        let mut rope = make_rope((128, 128));
        rope.nodes[0] = (129, 130);
        rope.nodes[1] = (129, 132);
        rope.move_node(1);
        assert_eq!(rope.nodes[1], (129, 131));
        assert!(rope.length(1) < 2);
    }
    #[test]
    fn test_move_tail_diag() {
        let mut rope = make_rope((128, 128));
        rope.nodes[0] = (130, 131);
        rope.nodes[1] = (129, 129);
        rope.move_node(1);
        assert_eq!(rope.tail(), (130, 130));
        assert!(rope.length(1) < 2);
        rope.nodes[0] = (131, 130);
        rope.nodes[1] = (129, 129);
        rope.move_node(1);
        assert_eq!(rope.tail(), (130, 130));
        assert!(rope.length(1) < 2);
    }
}
