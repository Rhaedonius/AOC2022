mod rope;

#[cfg(test)]
mod tests;

fn parse_input(input: &str) -> Vec<(&str, u16)> {
    return input
        .lines()
        .map(|l| {
            let (dir, steps) = match l.split_once(" ") {
                Some(s) => s,
                None => panic!("Parsed line: {}", l),
            };
            return (dir, steps.parse().unwrap());
        })
        .collect();
}

fn question1(input: &str) -> usize {
    let moves = parse_input(input);
    let mut rope = rope::Rope::new(2);
    for (dir, steps) in moves {
        rope.next_move(dir, steps);
    }
    return rope.visited_positions().len();
}

fn question2(input: &str) -> usize {
    let moves = parse_input(input);
    let mut rope = rope::Rope::new(10);
    for (dir, steps) in moves {
        rope.next_move(dir, steps);
    }
    return rope.visited_positions().len();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}
