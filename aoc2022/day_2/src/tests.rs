use crate::{question1, question2};

#[test]
fn test_question1() {
    let input = include_str!("../input.txt");
    assert_eq!(question1(input), 13221);
}

#[test]
fn test_question2() {
    let input = include_str!("../input.txt");
    assert_eq!(question2(input), 13131);
}
