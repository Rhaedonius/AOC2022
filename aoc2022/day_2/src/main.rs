use std::collections::HashMap;
#[cfg(test)]
mod tests;

type ScoreMap<'a> = HashMap<&'a str, u32>;

fn calculate_score(input: &str, scores: ScoreMap) -> u32 {
    let mut total_score: u32 = 0;
    for game in input.lines() {
        if let Some(score) = scores.get(&game) {
            total_score += score;
        }
    }
    return total_score;
}

fn question1(input: &str) -> u32 {
    let scores: ScoreMap = HashMap::from([
        ("A X", 4),
        ("A Y", 8),
        ("A Z", 3),
        ("B X", 1),
        ("B Y", 5),
        ("B Z", 9),
        ("C X", 7),
        ("C Y", 2),
        ("C Z", 6),
    ]);
    return calculate_score(input, scores);
}

fn question2(input: &str) -> u32 {
    let scores: ScoreMap = HashMap::from([
        ("A X", 3), //rock + lose = scissor
        ("A Y", 4), //rock + draw = rock
        ("A Z", 8), //rock + win = paper
        ("B X", 1), //paper + lose = rock
        ("B Y", 5), //paper + draw = paper
        ("B Z", 9), //paper + win = scissor
        ("C X", 2), //scissor + lose = paper
        ("C Y", 6), //scissor + draw = scissor
        ("C Z", 7), //scissor + win = rock
    ]);
    return calculate_score(input, scores);
}

fn main() {
    let input = include_str!("../input.txt");
    println!("Question 1: {}", question1(input));
    println!("Question 2: {}", question2(input));
}
