#[derive(Copy, Clone, PartialEq, Eq, Hash)]
pub struct Position {
    row: usize,
    col: usize,
}

#[derive(Debug, Copy, Clone)]
pub enum Direction {
    Right,
    Up,
    Left,
    Down,
}

#[derive(Debug, Copy, Clone)]
pub struct Blizzard {
    pos: Position,
    dir: Direction,
}

impl Blizzard {
    fn r#move(&mut self) {
        match self.dir {
            Direction::Right => self.pos.col += 1,
            Direction::Up => self.pos.row -= 1,
            Direction::Left => self.pos.col -= 1,
            Direction::Down => self.pos.row += 1,
        }
    }
    fn wrap(&mut self, rows: usize, cols: usize) {
        if self.pos.row == 0 {
            self.pos.row = rows - 2;
        } else if self.pos.row == rows - 1 {
            self.pos.row = 1;
        } else if self.pos.col == 0 {
            self.pos.col = cols - 2;
        } else if self.pos.col == cols - 1 {
            self.pos.col = 1;
        }
    }
}

pub struct Board {
    blizzard: Vec<Blizzard>,
    wall: Vec<Position>,
    rows: usize,
    cols: usize,
    pub entrance: Position,
    pub exit: Position,
}

impl Board {
    pub fn step(&mut self) {
        for b in self.blizzard.iter_mut() {
            b.r#move();
            b.wrap(self.rows, self.cols);
        }
    }
    fn is_pos_free(&self, pos: &Position) -> bool {
        !self.wall.iter().any(|w| w == pos) && !self.blizzard.iter().any(|b| &b.pos == pos)
    }
    pub fn actions(&self, pos: Position) -> Vec<Position> {
        let mut next = Vec::new();
        if pos.row < self.rows - 1 {
            let new_pos = Position {
                row: pos.row + 1,
                col: pos.col,
            };
            next.push(new_pos);
        }
        if pos.col < self.cols - 1 {
            let new_pos = Position {
                row: pos.row,
                col: pos.col + 1,
            };
            next.push(new_pos);
        }
        if pos.row > 0 {
            let new_pos = Position {
                row: pos.row - 1,
                col: pos.col,
            };
            next.push(new_pos);
        }
        if pos.col > 0 {
            let new_pos = Position {
                row: pos.row,
                col: pos.col - 1,
            };
            next.push(new_pos);
        }
        next.push(pos);
        next.into_iter().filter(|p| self.is_pos_free(p)).collect()
    }
    pub fn travel_time(&mut self, start: Position, end: Position) -> usize {
        let mut count = 0;
        let mut pos = vec![start];
        loop {
            // dbg!(&self);
            if count % 50 == 0 {
                eprintln!("minute {count}: {} candidates", pos.len());
            }
            self.step();
            let mut candidate = pos.clone();
            // dbg!(&candidate);
            pos = Vec::new();
            while let Some(p) = candidate.pop() {
                if p == end {
                    return count;
                }
                for a in self.actions(p) {
                    let should_not_prune = a.col.abs_diff(start.col) + 2 >= count / (self.rows - 2);
                    if !pos.contains(&a) && should_not_prune {
                        pos.push(a);
                    }
                }
            }
            if pos.len() == 0 {
                panic!("No more candidates, destination unreachable");
            }
            count += 1;
        }
    }
}

impl std::str::FromStr for Board {
    type Err = std::convert::Infallible;
    fn from_str(input: &str) -> Result<Board, Self::Err> {
        let mut board = Board {
            blizzard: Vec::new(),
            wall: Vec::new(),
            rows: 0,
            cols: 0,
            entrance: Position { row: 0, col: 0 },
            exit: Position { row: 0, col: 0 },
        };
        for (row, line) in input.lines().enumerate() {
            board.rows += 1;
            for (col, c) in line.chars().enumerate() {
                let pos = Position { row, col };
                match c {
                    '.' if row == 0 => board.entrance = pos,
                    '.' => board.exit = pos,
                    '#' => board.wall.push(pos),
                    '>' => board.blizzard.push(Blizzard {
                        pos,
                        dir: Direction::Right,
                    }),
                    '^' => board.blizzard.push(Blizzard {
                        pos,
                        dir: Direction::Up,
                    }),
                    '<' => board.blizzard.push(Blizzard {
                        pos,
                        dir: Direction::Left,
                    }),
                    'v' => board.blizzard.push(Blizzard {
                        pos,
                        dir: Direction::Down,
                    }),
                    _ => {}
                }
                board.cols = col;
            }
        }
        board.cols += 1;
        Ok(board)
    }
}
impl std::fmt::Debug for Position {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "Position({}, {})", self.row, self.col)
    }
}

impl std::fmt::Debug for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        writeln!(f, "Board [{}, {}]:", self.rows, self.cols)?;
        for row in 0..self.rows {
            let mut buf = String::new();
            for col in 0..self.cols {
                let pos = Position { row, col };
                if self.wall.iter().any(|w| *w == pos) {
                    buf.push('#');
                    continue;
                }
                let count = self.blizzard.iter().filter(|b| b.pos == pos).count();
                if count > 1 {
                    buf.push_str(count.to_string().as_str());
                } else if let Some(b) = self.blizzard.iter().find(|b| b.pos == pos) {
                    match b.dir {
                        Direction::Right => buf.push('>'),
                        Direction::Up => buf.push('^'),
                        Direction::Left => buf.push('<'),
                        Direction::Down => buf.push('v'),
                    }
                } else {
                    buf.push('.');
                }
            }
            writeln!(f, "{buf}")?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_board() {
        let mut board: Board = include_str!("../test.txt").parse().unwrap();
        assert!(board.entrance == Position { row: 0, col: 1 });
        assert!(board.exit == Position { row: 5, col: 6 });
        assert!(board.is_pos_free(&Position { row: 0, col: 1 }));
        assert!(!board.is_pos_free(&Position { row: 0, col: 2 }));
        assert!(!board.is_pos_free(&Position { row: 1, col: 1 }));
        assert!(!board.is_pos_free(&Position { row: 1, col: 1 }));
        board.step();
        assert!(board.is_pos_free(&Position { row: 1, col: 1 }));
    }
}
