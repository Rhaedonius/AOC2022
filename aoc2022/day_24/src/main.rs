mod blizzard;

use blizzard::Board;

fn main() {
    let input = include_str!("../input.txt");
    let mut board: Board = input.parse().unwrap();
    let t1 = board.travel_time(board.entrance, board.exit);
    println!("Question 1: {:?}", t1);
    let t2 = board.travel_time(board.exit, board.entrance);
    let t3 = board.travel_time(board.entrance, board.exit);
    println!("Question 2: {:?}", t1 + t2 + t3 + 2);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        let mut board: Board = input.parse().unwrap();
        let t1 = board.travel_time(board.entrance, board.exit);
        assert_eq!(t1, 18)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        let mut board: Board = input.parse().unwrap();
        let t1 = board.travel_time(board.entrance, board.exit);
        let t2 = board.travel_time(board.exit, board.entrance);
        let t3 = board.travel_time(board.entrance, board.exit);
        assert_eq!(t1 + t2 + t3 + 2, 54)
    }
    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../test.txt");
        let mut board: Board = input.parse().unwrap();
        let t1 = board.travel_time(board.entrance, board.exit);
        assert_eq!(t1, 326)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        let mut board: Board = input.parse().unwrap();
        let t1 = board.travel_time(board.entrance, board.exit);
        let t2 = board.travel_time(board.exit, board.entrance);
        let t3 = board.travel_time(board.entrance, board.exit);
        assert_eq!(t1 + t2 + t3 + 2, 976)
    }
}
