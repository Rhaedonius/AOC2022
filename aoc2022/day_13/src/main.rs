use serde_json::Value;
use std::cmp::Ordering;

#[cfg(test)]
mod tests;

fn str_to_array(input: &str) -> Vec<Value> {
    let v = serde_json::from_str::<Value>(input).unwrap();
    return v.as_array().unwrap().clone();
}

fn array(v: &Value) -> &Vec<Value> {
    return v.as_array().unwrap();
}

fn number(n: &Value) -> u64 {
    return n.as_u64().unwrap();
}

fn compare_values(l: &Value, r: &Value) -> Ordering {
    match (l.is_number(), r.is_number()) {
        (true, true) => return number(l).cmp(&number(r)),
        (true, false) => return compare_lists(&Vec::from([l.clone()]), array(r)),
        (false, true) => return compare_lists(array(l), &Vec::from([r.clone()])),
        (false, false) => return compare_lists(array(l), array(r)),
    }
}

fn compare_lists(left: &Vec<Value>, right: &Vec<Value>) -> Ordering {
    for (l, r) in std::iter::zip(left, right) {
        match compare_values(l, r) {
            Ordering::Less => return Ordering::Less,
            Ordering::Greater => return Ordering::Greater,
            Ordering::Equal => {}
        }
    }
    return left.len().cmp(&right.len());
}

fn question1(input: &str) -> usize {
    let pairs = input.split("\n\n");
    let mut count = 0;
    for (i, p) in pairs.enumerate() {
        let (first, second) = p.split_once('\n').unwrap();
        let left = str_to_array(first);
        let right = str_to_array(second);
        let res = compare_lists(&left, &right);
        //eprintln!("Pair {}: {}, {} = {:?}", i + 1, first, second, res);
        if res == Ordering::Less {
            count += i + 1;
        }
    }
    return count;
}

fn question2(input: &str) -> usize {
    let mut packets: Vec<Vec<Value>> = input
        .replace("\n\n", "\n")
        .split('\n')
        .filter(|s| s != &"")
        .map(str_to_array)
        .collect();
    let marker1 = str_to_array("[[2]]");
    let marker2 = str_to_array("[[6]]");
    packets.push(marker1.clone());
    packets.push(marker2.clone());
    packets.sort_by(compare_lists);
    return packets
        .into_iter()
        .enumerate()
        .filter_map(|(i, v)| {
            if v == marker1 || v == marker2 {
                Some(i + 1)
            } else {
                None
            }
        })
        .product();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}
