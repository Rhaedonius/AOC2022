use std::collections::HashSet;
#[cfg(test)]
mod tests;

fn priority(c: &char) -> u32 {
    return match *c {
        c if c.is_lowercase() => u32::from(c) - 0x60,
        c if c.is_uppercase() => u32::from(c) - 0x40 + 26,
        _ => 0,
    };
}

type Rucksack = HashSet<char>;

fn question1(input: &str) -> u32 {
    let mut count: u32 = 0;
    for line in input.lines() {
        let l = line.len();
        let a: Rucksack = line.chars().take(l / 2).collect();
        let b: Rucksack = line.chars().skip(l / 2).collect();
        for c in a.intersection(&b) {
            count += priority(c);
        }
    }
    return count;
}

fn question2(input: &str) -> u32 {
    let mut count: u32 = 0;
    let mut a: Rucksack = HashSet::new();
    let mut b: Rucksack = HashSet::new();
    let mut c: Rucksack = HashSet::new();
    for (i, line) in input.lines().enumerate() {
        match i % 3 {
            0 => a = line.chars().collect(),
            1 => b = line.chars().collect(),
            2 => c = line.chars().collect(),
            _ => {}
        }
        if i % 3 != 2 {
            // skip the next section if all 3 sets are not populated
            continue;
        }
        let common: Rucksack = a.intersection(&b).cloned().collect();
        // cloned was needed to keep reference alive (compared with a one-liner)
        for e in common.intersection(&c) {
            count += priority(e);
        }
    }
    return count;
}

fn main() {
    let input = include_str!("../input.txt");
    println!("Question 1: {}", question1(input));
    println!("Question 2: {}", question2(input));
}
