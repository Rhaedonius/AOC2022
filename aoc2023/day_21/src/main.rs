use std::collections::HashSet;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Tile {
    Garden,
    Rock,
}

type Position = (usize, usize);

type InfinitePosition = (i64, i64);

fn parse_input(input: &'static str) -> (Vec<Vec<Tile>>, Position) {
    let mut start_pos = (0, 0);
    let map = input
        .lines()
        .enumerate()
        .map(|(i, l)| {
            l.chars()
                .enumerate()
                .map(|(j, c)| match c {
                    '#' => Tile::Rock,
                    '.' => Tile::Garden,
                    'S' => {
                        start_pos = (i, j);
                        Tile::Garden
                    }
                    v => panic!("invalid input character: {v}"),
                })
                .collect()
        })
        .collect();
    (map, start_pos)
}

fn visit(map: &Vec<Vec<Tile>>, pos: Position) -> Vec<Position> {
    let mut visit = Vec::new();
    let (row, col) = pos;
    if row > 0 && map[row - 1][col] == Tile::Garden {
        visit.push((row - 1, col));
    }
    if row < map.len() - 1 && map[row + 1][col] == Tile::Garden {
        visit.push((row + 1, col));
    }
    if col > 0 && map[row][col - 1] == Tile::Garden {
        visit.push((row, col - 1));
    }
    if col < map[0].len() - 1 && map[row][col + 1] == Tile::Garden {
        visit.push((row, col + 1));
    }
    visit
}

fn question1(input: &'static str, steps: usize) -> usize {
    let (map, p) = parse_input(input);
    let mut visited: HashSet<Position> = HashSet::from([p]);
    for _ in 0..steps {
        let mut visitor: HashSet<Position> = HashSet::new();
        for p in visited {
            for v in visit(&map, p) {
                _ = visitor.insert(v);
            }
        }
        visited = visitor;
    }
    visited.len()
}

fn visit_infinite(map: &Vec<Vec<Tile>>, pos: InfinitePosition) -> Vec<InfinitePosition> {
    let mut visit = Vec::new();
    let rows = map.len() - 1;
    let cols = map[0].len() - 1;
    let (row, col) = finite_from(pos, (rows, cols));
    if row > 0 && map[row - 1][col] == Tile::Garden || row == 0 && map[rows][col] == Tile::Garden {
        visit.push((pos.0 - 1, pos.1));
    }
    if row < rows && map[row + 1][col] == Tile::Garden || row == rows && map[0][col] == Tile::Garden
    {
        visit.push((pos.0 + 1, pos.1));
    }
    if col > 0 && map[row][col - 1] == Tile::Garden || col == 0 && map[row][cols] == Tile::Garden {
        visit.push((pos.0, pos.1 - 1));
    }
    if col < cols && map[row][col + 1] == Tile::Garden || col == cols && map[row][0] == Tile::Garden
    {
        visit.push((pos.0, pos.1 + 1));
    }
    print!("({},{}) -> ({row},{col}): [", pos.0, pos.1);
    for v in &visit {
        print!("({},{}) ", v.0, v.1)
    }
    println!("]");
    visit
}

fn infinite_from(value: Position) -> InfinitePosition {
    (value.0 as i64, value.1 as i64)
}

fn finite_from(value: InfinitePosition, shape: (usize, usize)) -> Position {
    let (rows, cols) = (shape.0 as i64, shape.1 as i64);
    let mut row = value.0 % rows;
    if row < 0 {
        row += rows
    }
    let mut col = value.1 % cols;
    if col < 0 {
        col += cols
    }
    return (row as usize, col as usize);
}

fn question2(input: &'static str, steps: usize) -> usize {
    let (map, p) = parse_input(input);
    let mut visited: HashSet<InfinitePosition> = HashSet::from([infinite_from(p)]);
    for _ in 0..steps {
        let mut visitor: HashSet<InfinitePosition> = HashSet::new();
        for p in visited {
            for v in visit_infinite(&map, p) {
                _ = visitor.insert(v);
            }
        }
        visited = visitor;
    }
    visited.len()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input, 64));
    println!("{:?}", question2(input, 26501365));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input, 6), 16)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input, 6), 16);
        assert_eq!(question2(input, 10), 50);
        assert_eq!(question2(input, 50), 1594);
        assert_eq!(question2(input, 100), 6536);
        assert_eq!(question2(input, 5000), 167004);
    }
    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input, 64), 0)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input, 26501365), 0)
    }
}
