#[derive(Debug)]
struct Race {
    distance: usize,
    time: usize,
}

fn parse_question1(input: &str) -> Vec<Race> {
    let (t, d) = input.split_once("\n").unwrap();
    let dist = d
        .split(": ")
        .nth(1)
        .unwrap()
        .split_whitespace()
        .filter_map(|n| n.parse::<usize>().ok());
    let time = t
        .split(": ")
        .nth(1)
        .unwrap()
        .split_whitespace()
        .filter_map(|n| n.parse::<usize>().ok());
    dist.zip(time)
        .map(|(distance, time)| Race { distance, time })
        .collect()
}

fn parse_question2(input: &str) -> Race {
    let (t, d) = input.split_once("\n").unwrap();
    let distance = d
        .split(": ")
        .nth(1)
        .unwrap()
        .split_whitespace()
        .collect::<Vec<_>>()
        .join("")
        .parse()
        .unwrap();
    let time = t
        .split(": ")
        .nth(1)
        .unwrap()
        .split_whitespace()
        .collect::<Vec<_>>()
        .join("")
        .parse()
        .unwrap();
    Race { distance, time }
}

fn ways_to_win(race: &Race) -> usize {
    let delta = race.time * race.time - 4 * race.distance;
    let start = 0.5 * (race.time as f64 - (delta as f64).sqrt()) + 1.0;
    let end = 0.5 * (race.time as f64 + (delta as f64).sqrt());
    (end.ceil() - start.floor()) as usize
}

fn question1(input: &str) -> usize {
    let races = parse_question1(input);
    races.iter().map(ways_to_win).product()
}

fn question2(input: &str) -> usize {
    let race = parse_question2(input);
    ways_to_win(&race)
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 288)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 71503)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 252000)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 36992486)
    }
}
