use std::fmt::Display;

struct Mapping {
    input: usize,
    output: usize,
    length: usize,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct SeedRange {
    start: usize,
    end: usize,
}

impl Display for SeedRange {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}..{}]", self.start, self.end)
    }
}

impl From<&str> for Mapping {
    fn from(s: &str) -> Mapping {
        let (output, rest) = s.split_once(" ").unwrap();
        let (input, length) = rest.split_once(" ").unwrap();
        Mapping {
            input: input.parse().unwrap(),
            output: output.parse().unwrap(),
            length: length.parse().unwrap(),
        }
    }
}

impl Mapping {
    fn apply(&self, val: usize) -> Option<usize> {
        if self.input <= val && val <= self.input + self.length {
            let delta = val - self.input;
            return Some(self.output + delta);
        }
        return None;
    }
    fn apply_range(&self, val: &SeedRange) -> (Option<SeedRange>, Vec<SeedRange>) {
        let mut remainder = Vec::new();
        let (mut start, mut end) = (val.start, val.end);
        // range is completely before or after map
        if end < self.input || start >= self.input + self.length {
            return (None, vec![*val]);
        }
        // range overlaps with start of map
        if start < self.input {
            remainder.push(SeedRange {
                start,
                end: self.input,
            });
            start = self.input;
        }
        // range overlaps with end of map
        if end - self.input > self.length {
            remainder.push(SeedRange {
                start: self.input + self.length,
                end,
            });
            end = self.input + self.length;
        }
        (
            Some(SeedRange {
                start: start - self.input + self.output,
                end: end - self.input + self.output,
            }),
            remainder,
        )
    }
}

fn parse_input(input: &str) -> (Vec<usize>, Vec<Vec<Mapping>>) {
    let mut blocks = input.split("\n\n");
    let seeds: Vec<usize> = blocks
        .next()
        .unwrap()
        .split(": ")
        .nth(1)
        .unwrap()
        .split_whitespace()
        .map(|n| n.parse::<usize>().unwrap())
        .collect();
    let maps = blocks
        .map(|l| l.lines().skip(1).map(Mapping::from).collect())
        .collect();
    return (seeds, maps);
}

fn apply_map(map: &Vec<Mapping>, val: usize) -> usize {
    map.iter().find_map(|m| m.apply(val)).unwrap_or(val)
}

fn find_location(seed: usize, maps: &Vec<Vec<Mapping>>) -> usize {
    maps.iter().fold(seed, |acc, v| apply_map(v, acc))
}

fn question1(input: &str) -> usize {
    let (seeds, maps) = parse_input(input);
    seeds
        .into_iter()
        .map(|s| find_location(s, &maps))
        .min()
        .unwrap()
}

fn map_ranges(map: &Mapping, seeds: &Vec<SeedRange>) -> (Vec<SeedRange>, Vec<SeedRange>) {
    let mut mapped = Vec::new();
    let mut remainder = Vec::new();
    for seed in seeds {
        let (maybe_s, mut r) = map.apply_range(seed);
        if let Some(s) = maybe_s {
            mapped.push(s);
        }
        remainder.append(&mut r);
    }
    (mapped, remainder)
}

fn question2(input: &str) -> usize {
    let (seeds, maps) = parse_input(input);
    let mut seed_ranges: Vec<SeedRange> = seeds
        .iter()
        .step_by(2)
        .zip(seeds.iter().skip(1).step_by(2))
        .map(|(&start, &len)| SeedRange {
            start,
            end: start + len,
        })
        .collect();
    for layer in maps {
        let mut mapped = Vec::new();
        for map in &layer {
            let (mut m, r) = map_ranges(map, &seed_ranges);
            mapped.append(&mut m);
            seed_ranges = r;
        }
        seed_ranges.append(&mut mapped);
    }
    seed_ranges.iter().map(|s| s.start).min().unwrap_or(0)
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_apply_range() {
        let map = Mapping {
            input: 11,
            output: 1,
            length: 10,
        };
        let (res, rem) = map.apply_range(&SeedRange { start: 12, end: 15 });
        assert_eq!(res, Some(SeedRange { start: 2, end: 5 }));
        assert!(rem.is_empty());
        let (res, rem) = map.apply_range(&SeedRange { start: 8, end: 13 });
        assert_eq!(res, Some(SeedRange { start: 1, end: 3 }));
        assert_eq!(rem, vec![SeedRange { start: 8, end: 11 }]);
        let (res, rem) = map.apply_range(&SeedRange { start: 17, end: 25 });
        assert_eq!(res, Some(SeedRange { start: 7, end: 11 }));
        assert_eq!(rem, vec![SeedRange { start: 21, end: 25 }]);
    }

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 35)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 46)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 910845529)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 77435348)
    }
}
