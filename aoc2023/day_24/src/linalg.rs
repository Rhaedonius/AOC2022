use std::fmt::Display;

#[derive(Debug, Clone)]
pub struct Matrix {
    pub values: Vec<Vec<f64>>,
    rows: usize,
    cols: usize,
}

impl From<Vec<f64>> for Matrix {
    fn from(values: Vec<f64>) -> Matrix {
        let n = values.len();
        Matrix {
            values: values.iter().map(|&v| vec![v]).collect(),
            rows: n,
            cols: 1,
        }
    }
}

impl From<Vec<Vec<f64>>> for Matrix {
    fn from(values: Vec<Vec<f64>>) -> Matrix {
        let rows = values.len();
        let cols = values[0].len();
        Matrix { values, rows, cols }
    }
}

impl Display for Matrix {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for i in 0..self.rows {
            for j in 0..self.cols {
                write!(f, "{:>5.2}", self.values[i][j])?;
                if j != self.cols - 1 {
                    write!(f, " ")?;
                }
            }
            writeln!(f, "")?;
        }
        Ok(())
    }
}

impl Matrix {
    pub fn zeros(rows: usize, cols: usize) -> Self {
        Matrix {
            values: vec![vec![0.0; cols]; rows],
            rows,
            cols,
        }
    }
    pub fn eye(n: usize) -> Self {
        let mut mat = Matrix {
            values: vec![vec![0.0; n]; n],
            rows: n,
            cols: n,
        };
        for i in 0..n {
            mat.values[i][i] = 1.0;
        }
        mat
    }
    pub fn inverse(&mut self) {
        let n = self.rows;
        let mut mat_a = self.values.clone();
        let mut mat_i = Matrix::eye(n).values;
        for step in 0..n {
            let s = mat_a[step][step];
            for j in 0..n {
                mat_a[step][j] /= s;
                mat_i[step][j] /= s;
            }
            for i in 0..n {
                if i == step {
                    continue;
                }
                let s = mat_a[i][step];
                for j in 0..n {
                    mat_a[i][j] -= mat_a[step][j] * s;
                    mat_i[i][j] -= mat_i[step][j] * s;
                }
            }
        }
        self.values = mat_i;
    }
}

impl std::ops::Mul for Matrix {
    type Output = Matrix;

    fn mul(self, other: Self) -> Self::Output {
        if self.cols != other.rows {
            panic!("invalid shape for matrix multiplication")
        }
        let mut res = Matrix::zeros(self.rows, other.cols);
        for row in 0..self.rows {
            for col in 0..other.cols {
                for i in 0..self.cols {
                    res.values[row][col] += self.values[row][i] * other.values[i][col];
                }
            }
        }
        res
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_inverse() {
        let mut mat = Matrix::from(vec![
            vec![1.0, 2.0, 3.0],
            vec![3.0, 5.0, 6.0],
            vec![7.0, 8.0, 4.0],
        ]);
        mat.inverse();
        assert_eq!(
            mat.values,
            vec![
                vec![28.0, -16.0, 3.0],
                vec![-30.0, 17.0, -3.0],
                vec![11.0, -6.0, 1.0]
            ]
        )
    }
}
