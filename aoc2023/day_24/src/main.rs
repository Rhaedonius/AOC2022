mod linalg;

#[derive(Debug, Clone, Copy)]
struct Coordinates {
    x: f64,
    y: f64,
    z: f64,
}

impl From<&str> for Coordinates {
    fn from(input: &str) -> Self {
        let (sx, rem) = input.split_once(", ").unwrap();
        let (sy, sz) = rem.split_once(", ").unwrap();
        Coordinates {
            x: sx.trim().parse().unwrap(),
            y: sy.trim().parse().unwrap(),
            z: sz.trim().parse().unwrap(),
        }
    }
}

fn check_area(p: (f64, f64), min: f64, max: f64) -> bool {
    let (x, y) = p;
    min <= x && x <= max && min <= y && y <= max
}

#[derive(Debug, Clone, Copy)]
struct Hailstone {
    position: Coordinates,
    velocity: Coordinates,
}

impl From<&str> for Hailstone {
    fn from(input: &str) -> Hailstone {
        let (pos, v) = input.split_once(" @ ").unwrap();
        Hailstone {
            position: Coordinates::from(pos),
            velocity: Coordinates::from(v),
        }
    }
}

impl Hailstone {
    fn coefficients(&self) -> (f64, f64) {
        let x = self.position.x;
        let y = self.position.y;
        let vx = self.velocity.x;
        let vy = self.velocity.y;
        let m = vy / vx;
        let b = y - m * x;
        (m, b)
    }
    fn is_past_crossing(&self, point: f64) -> bool {
        let x = self.position.x;
        (self.velocity.x > 0.0 && point < x) || (self.velocity.x < 0.0 && point > x)
    }
    fn collision(&self, other: Hailstone) -> Option<(f64, f64)> {
        let (m1, b1) = self.coefficients();
        let (m2, b2) = other.coefficients();
        if m1 == m2 {
            // parallel
            return None;
        }
        let x = (b1 - b2) / (m2 - m1);
        if self.is_past_crossing(x) || other.is_past_crossing(x) {
            None
        } else {
            let y = m1 * x + b1;
            Some((x, y))
        }
    }
    fn as_tuple(&self) -> (f64, f64, f64, f64, f64, f64) {
        (
            self.position.x,
            self.position.y,
            self.position.z,
            self.velocity.x,
            self.velocity.y,
            self.velocity.z,
        )
    }
}

fn question1(input: &'static str, min: f64, max: f64) -> usize {
    let hail: Vec<Hailstone> = input.lines().map(Hailstone::from).collect();
    let l = hail.len();
    let mut res = 0;
    for i in 0..l - 1 {
        res += (i + 1..l)
            .filter_map(|j| hail[i].collision(hail[j]))
            .filter(|&p| check_area(p, min, max))
            .count()
    }
    res
}

fn question2(input: &'static str) -> f64 {
    let hail: Vec<Hailstone> = input.lines().map(Hailstone::from).collect();
    // these three work for my input due to rounding errors
    // exampe should use 0, 1, 2
    let (xa, ya, za, vax, vay, vaz) = hail[10].as_tuple();
    let (xb, yb, zb, vbx, vby, vbz) = hail[12].as_tuple();
    let (xc, yc, zc, vcx, vcy, vcz) = hail[13].as_tuple();
    // condition for collision with hail 'a' at time 't': x + v*t = x_a + v_a * t
    // rewrite the condition: (x - x_a) = -t * (v - v_a)
    // since 't' is a scalar, (x - x_a) and (v - v_a) are parallel vectors
    // we can write this as a cross product: (x - x_a) x (v - v_a) = 0
    // the cross product has some mixed terms of type x*v so it's not linear
    // since we want to collide with all hails, we can use hail 'b' and write
    // (x - x_a) x (v - v_a) = (x - x_b) x (v - v_b)
    // this is now a system of 3 linear equation with 6 variables.
    // we can also compare with hail 'c' and get 6 equations.
    let mut mat_a = linalg::Matrix::from(vec![
        vec![vby - vay, vax - vbx, 0.0, ya - yb, xb - xa, 0.0],
        vec![vcy - vay, vax - vcx, 0.0, ya - yc, xc - xa, 0.0],
        vec![vaz - vbz, 0.0, vbx - vax, zb - za, 0.0, xa - xb],
        vec![vaz - vcz, 0.0, vcx - vax, zc - za, 0.0, xa - xc],
        vec![0.0, vbz - vaz, vay - vby, 0.0, za - zb, yb - ya],
        vec![0.0, vcz - vaz, vay - vcy, 0.0, za - zc, yc - ya],
    ]);
    let mat_b = linalg::Matrix::from(vec![
        xb * vby - yb * vbx - xa * vay + ya * vax,
        xc * vcy - yc * vcx - xa * vay + ya * vax,
        zb * vbx - xb * vbz - za * vax + xa * vaz,
        zc * vcx - xc * vcz - za * vax + xa * vaz,
        yb * vbz - zb * vby - ya * vaz + za * vay,
        yc * vcz - zc * vcy - ya * vaz + za * vay,
    ]);
    mat_a.inverse();
    let res = mat_a * mat_b;
    println!("{res}");
    res.values
        .into_iter()
        .map(|v| v[0])
        .take(3)
        .sum::<f64>()
        .round()
}

fn main() {
    let input = include_str!("../input.txt");
    println!(
        "{:?}",
        question1(input, 200_000_000_000_000.0, 400_000_000_000_000.0)
    );
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input, 7.0, 27.0), 2)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 47.0)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(
            question1(input, 200_000_000_000_000.0, 400_000_000_000_000.0),
            13754
        )
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 711031616315001.0)
    }
}
