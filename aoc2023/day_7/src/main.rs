use std::{cmp::Ordering, fmt::Debug};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum HandKind {
    HighCard,
    OnePair,
    TwoPair,
    ThreeKind,
    FullHouse,
    FourKind,
    FiveKind,
}

type HandValues = Vec<usize>;

impl From<&HandValues> for HandKind {
    fn from(value: &HandValues) -> Self {
        let counts = value.into_iter().fold(vec![0usize; 13], |mut acc, &v| {
            acc[v] += 1;
            acc
        });
        if counts.iter().any(|&c| c == 5) {
            return HandKind::FiveKind;
        }
        if counts.iter().any(|&c| c == 4) {
            return HandKind::FourKind;
        }
        if counts.iter().any(|&c| c == 3) {
            if counts.iter().any(|&c| c == 2) {
                return HandKind::FullHouse;
            } else {
                return HandKind::ThreeKind;
            }
        }
        match counts.into_iter().filter(|&c| c == 2).count() {
            2 => return HandKind::TwoPair,
            1 => return HandKind::OnePair,
            _ => return HandKind::HighCard,
        }
    }
}

#[derive(PartialEq, Eq)]
struct Hand {
    cards: HandValues,
    kind: HandKind,
    bet: usize,
}

impl Debug for Hand {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Hand{{ cards: [{}] kind: {:?}, bet: {}}}",
            self.cards
                .iter()
                .fold(String::new(), |acc, c| format!("{}, {}", acc, c)),
            self.kind,
            self.bet
        )
    }
}

impl From<&str> for Hand {
    fn from(value: &str) -> Self {
        let (s_hand, s_bet) = value.split_once(' ').unwrap();
        let cards: Vec<usize> = s_hand
            .chars()
            .filter_map(|c| match c {
                c if c.is_ascii_digit() => Some(c.to_digit(10).unwrap() as usize - 2),
                'T' => Some(8),
                'J' => Some(9),
                'Q' => Some(10),
                'K' => Some(11),
                'A' => Some(12),
                _ => None,
            })
            .collect();
        let kind = HandKind::from(&cards);
        let bet = s_bet.parse().unwrap();
        Hand { cards, kind, bet }
    }
}

impl Hand {
    fn joker_rule(&self) -> Self {
        let kind = match self.cards.iter().filter(|&&v| v == 9).count() {
            1 => match self.kind {
                HandKind::FourKind => HandKind::FiveKind,
                HandKind::ThreeKind => HandKind::FourKind,
                HandKind::TwoPair => HandKind::FullHouse,
                HandKind::OnePair => HandKind::ThreeKind,
                HandKind::HighCard => HandKind::OnePair,
                h => h,
            },
            2 => match self.kind {
                HandKind::FullHouse => HandKind::FiveKind,
                HandKind::TwoPair => HandKind::FourKind,
                HandKind::OnePair => HandKind::ThreeKind,
                h => h,
            },
            3 => match self.kind {
                HandKind::FullHouse => HandKind::FiveKind,
                HandKind::ThreeKind => HandKind::FourKind,
                h => h,
            },
            4 => match self.kind {
                HandKind::FourKind => HandKind::FiveKind,
                h => h,
            },
            _ => self.kind,
        };
        let cards = self
            .cards
            .iter()
            .map(|&c| match c {
                9 => 0,
                c if c < 9 => c + 1,
                c => c,
            })
            .collect();
        Hand {
            cards,
            kind,
            bet: self.bet,
        }
    }
}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        match self.kind.cmp(&other.kind) {
            Ordering::Less => return Ordering::Less,
            Ordering::Greater => return Ordering::Greater,
            Ordering::Equal => return self.cards.cmp(&other.cards),
        }
    }
}

fn question1(input: &str) -> usize {
    let mut hands: Vec<Hand> = input.lines().map(Hand::from).collect();
    hands.sort();
    hands
        .into_iter()
        .enumerate()
        .fold(0, |acc, (i, c)| acc + (i + 1) * c.bet)
}

fn question2(input: &str) -> usize {
    let mut hands: Vec<Hand> = input.lines().map(|l| Hand::from(l).joker_rule()).collect();
    hands.sort();
    dbg!(&hands);
    hands
        .into_iter()
        .enumerate()
        .fold(0, |acc, (i, c)| acc + (i + 1) * c.bet)
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 6440)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 5905)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 253638586)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 253253225)
    }
}
