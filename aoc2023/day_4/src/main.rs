#[derive(Debug)]
struct Card<'a> {
    numbers: Vec<&'a str>,
    winning: Vec<&'a str>,
}

impl Card<'_> {
    fn matches(&self) -> usize {
        self.numbers
            .iter()
            .filter(|n| self.winning.contains(n))
            .count()
    }
    fn winning_points(&self) -> usize {
        let n = self.matches() as u32;
        if n == 0 {
            return 0;
        }
        2usize.pow(n - 1)
    }
}

impl<'a> From<&'a str> for Card<'a> {
    fn from(s: &'a str) -> Card<'a> {
        let (_, card_text) = s.split_once(':').unwrap();
        let (w, n) = card_text.split_once('|').unwrap();
        Card {
            numbers: n.split_whitespace().collect(),
            winning: w.split_whitespace().collect(),
        }
    }
}

fn question1(input: &str) -> usize {
    let cards: Vec<Card> = input.lines().map(Card::from).collect();
    cards.iter().map(Card::winning_points).sum()
}

fn question2(input: &str) -> usize {
    let cards: Vec<Card> = input.lines().map(Card::from).collect();
    let mut won_cards = vec![1; cards.len()];
    for (i, c) in cards.iter().enumerate() {
        let n = c.matches();
        for j in i + 1..=(i + n).min(cards.len() - 1) {
            won_cards[j] += won_cards[i];
        }
    }
    won_cards.iter().sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 13)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 30)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 25004)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 14427616)
    }
}
