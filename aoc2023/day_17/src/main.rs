use std::collections::{HashMap, VecDeque};

type Position = (usize, usize);
type Map = Vec<Vec<usize>>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

enum Turn {
    Left,
    Right,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct Crucible {
    heat: usize,
    position: Position,
    direction: Direction,
    momentum: usize,
}

impl Crucible {
    fn new() -> Self {
        Crucible {
            heat: 0,
            position: (0, 0),
            direction: Direction::Right,
            momentum: 0,
        }
    }
    fn turn(&self, t: Turn) -> Self {
        Crucible {
            heat: self.heat,
            position: self.position,
            direction: match t {
                Turn::Left => match self.direction {
                    Direction::Up => Direction::Left,
                    Direction::Down => Direction::Right,
                    Direction::Left => Direction::Down,
                    Direction::Right => Direction::Up,
                },
                Turn::Right => match self.direction {
                    Direction::Up => Direction::Right,
                    Direction::Right => Direction::Down,
                    Direction::Down => Direction::Left,
                    Direction::Left => Direction::Up,
                },
            },
            momentum: 0,
        }
    }
    fn generate_steps(&self, min: usize, max: usize) -> Vec<Crucible> {
        match self.momentum {
            m if m < min => vec![self.to_owned()],
            m if m < max => vec![
                self.to_owned(),
                self.turn(Turn::Right),
                self.turn(Turn::Left),
            ],
            _ => vec![self.turn(Turn::Left), self.turn(Turn::Right)],
        }
    }
}

fn step(map: &Map, c: &Crucible) -> Option<Crucible> {
    let (i, j) = c.position;
    let rows = map.len();
    let cols = map[0].len();
    match c.direction {
        Direction::Up if i > 0 => Some(Crucible {
            heat: c.heat + map[i - 1][j],
            position: (i - 1, j),
            direction: c.direction,
            momentum: c.momentum + 1,
        }),
        Direction::Down if i < rows - 1 => Some(Crucible {
            heat: c.heat + map[i + 1][j],
            position: (i + 1, j),
            direction: c.direction,
            momentum: c.momentum + 1,
        }),
        Direction::Left if j > 0 => Some(Crucible {
            heat: c.heat + map[i][j - 1],
            position: (i, j - 1),
            direction: c.direction,
            momentum: c.momentum + 1,
        }),
        Direction::Right if j < cols - 1 => Some(Crucible {
            heat: c.heat + map[i][j + 1],
            position: (i, j + 1),
            direction: c.direction,
            momentum: c.momentum + 1,
        }),
        _ => None,
    }
}

fn parse_input(input: &'static str) -> Map {
    input
        .lines()
        .map(|l| {
            l.chars()
                .map(|c| c.to_digit(10).unwrap() as usize)
                .collect()
        })
        .collect()
}

fn find_best_path(map: &Map, min_momentum: usize, max_momentum: usize) -> usize {
    let rows = map.len();
    let cols = map[0].len();
    let mut paths = VecDeque::from([Crucible::new()]);
    let mut dp: HashMap<(Position, Direction, usize), usize> = HashMap::new();
    let mut res = usize::MAX;
    loop {
        let c = match paths.pop_front() {
            Some(c) => c,
            None => break,
        };
        if c.position == (rows - 1, cols - 1) && c.momentum >= min_momentum {
            res = res.min(c.heat);
            continue;
        }
        match dp.get(&(c.position, c.direction, c.momentum)) {
            Some(&h) if c.heat >= h => continue,
            _ => {
                dp.insert((c.position, c.direction, c.momentum), c.heat);
            }
        }
        for p in c
            .generate_steps(min_momentum, max_momentum)
            .iter()
            .filter_map(|s| step(&map, s))
        {
            paths.push_back(p);
        }
    }
    res
}

fn question1(input: &'static str) -> usize {
    let map = parse_input(input);
    find_best_path(&map, 0, 3)
}

fn question2(input: &'static str) -> usize {
    let map = parse_input(input);
    find_best_path(&map, 4, 10)
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ultra_crucible() {
        let input = "111111111111\n999999999991\n999999999991\n999999999991\n999999999991";
        assert_eq!(question2(input), 71)
    }
    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 102)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 94)
    }
    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 694)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 829)
    }
}
