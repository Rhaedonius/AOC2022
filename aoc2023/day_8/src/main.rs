use std::collections::HashMap;

#[derive(Debug, Copy, Clone)]
enum Direction {
    L,
    R,
}

struct Node<'a> {
    name: &'a str,
    left: &'a str,
    right: &'a str,
}

impl<'a> From<&'a str> for Node<'a> {
    fn from(value: &'a str) -> Self {
        let (name, left, right) = (&value[0..3], &value[7..10], &value[12..15]);
        Node { name, left, right }
    }
}

fn parse_input(input: &str) -> (Vec<Direction>, HashMap<&str, Node>) {
    let (p, n) = input.split_once("\n\n").unwrap();
    let pattern = p
        .chars()
        .filter_map(|c| match c {
            'L' => Some(Direction::L),
            'R' => Some(Direction::R),
            _ => None,
        })
        .collect();
    let map: HashMap<&str, Node> = n
        .lines()
        .map(|s| {
            let n = Node::from(s);
            (n.name, n)
        })
        .collect();
    (pattern, map)
}

fn gcd(a: usize, b: usize) -> usize {
    if b == 0 {
        return a;
    } else {
        return gcd(b, a % b);
    }
}

fn lcm(a: usize, b: usize) -> usize {
    return b / gcd(a, b) * a;
}

fn find_z(start: &str, pattern: &Vec<Direction>, map: &HashMap<&str, Node>) -> usize {
    let mut node = start;
    let mut res = 0;
    loop {
        node = match pattern[res % pattern.len()] {
            Direction::L => map[node].left,
            Direction::R => map[node].right,
        };
        res += 1;
        if node.ends_with('Z') {
            break;
        }
    }
    res
}

fn question1(input: &str) -> usize {
    let (pattern, map) = parse_input(input);
    find_z("AAA", &pattern, &map)
}

fn question2(input: &str) -> usize {
    let (pattern, map) = parse_input(input);
    // the mapping is made of cycles
    let starting_nodes: Vec<&str> = map
        .keys()
        .filter_map(|&s| if s.ends_with('A') { Some(s) } else { None })
        .collect();
    let cycles: Vec<usize> = starting_nodes
        .iter()
        .map(|n| find_z(n, &pattern, &map))
        .collect();
    cycles.into_iter().reduce(lcm).unwrap()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test1.txt");
        assert_eq!(question1(input), 2);
        let input2 = "LLR\n\nAAA = (BBB, BBB)\nBBB = (AAA, ZZZ)\nZZZ = (ZZZ, ZZZ)";
        assert_eq!(question1(input2), 6);
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test2.txt");
        assert_eq!(question2(input), 6)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 14257)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 16187743689077)
    }
}
