enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl From<&str> for Direction {
    fn from(input: &str) -> Self {
        match input {
            "U" => Direction::Up,
            "D" => Direction::Down,
            "R" => Direction::Right,
            "L" => Direction::Left,
            c => panic!("invalid direction in input: {c}"),
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Vector {
    x: isize,
    y: isize,
}

impl std::ops::Add for Vector {
    type Output = Vector;

    fn add(self, other: Self) -> Self::Output {
        Vector {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Vector {
    fn new() -> Self {
        Vector { x: 0, y: 0 }
    }
    fn norm(&self) -> isize {
        // we only have right angles, so only one of the two is not zero
        self.x.abs() + self.y.abs()
    }
}

struct Trench {
    line: Vector,
    color: String,
}

impl From<&str> for Trench {
    fn from(input: &str) -> Self {
        let (d, rem) = input.split_once(' ').unwrap();
        let (num, c) = rem.split_once(' ').unwrap();
        let n = num.parse::<isize>().unwrap();
        let line = match Direction::from(d) {
            Direction::Up => Vector { x: 0, y: -n },
            Direction::Down => Vector { x: 0, y: n },
            Direction::Left => Vector { x: -n, y: 0 },
            Direction::Right => Vector { x: n, y: 0 },
        };
        let color = String::from(c);
        Trench { line, color }
    }
}

impl Trench {
    fn from_color(&self) -> Vector {
        let pat: &[_] = &['(', ')', '#'];
        let color = self.color.trim_matches(pat);
        let n: isize = color
            .chars()
            .take(5)
            .fold(0, |acc, c| acc * 16 + c.to_digit(16).unwrap() as isize);
        match color.chars().nth(5) {
            Some('3') => Vector { x: 0, y: -n },
            Some('1') => Vector { x: 0, y: n },
            Some('2') => Vector { x: -n, y: 0 },
            Some('0') => Vector { x: n, y: 0 },
            _ => unreachable!(),
        }
    }
}

fn question1(input: &'static str) -> isize {
    let mut pos = Vector::new();
    let mut trench = Vec::from([pos]);
    let mut border = 0;
    for t in input.lines().map(Trench::from) {
        pos = pos + t.line;
        border += t.line.norm();
        trench.push(pos);
    }
    // apply shoelace formula
    let area = trench
        .iter()
        .zip(trench.iter().skip(1))
        .fold(0, |acc, (a, b)| acc + a.x * b.y - a.y * b.x)
        / 2;
    area + border / 2 + 1
}

fn question2(input: &'static str) -> isize {
    let mut pos = Vector::new();
    let mut trench = Vec::from([pos]);
    let mut border = 0;
    for t in input.lines().map(Trench::from) {
        let line = t.from_color();
        pos = pos + line;
        border += line.norm();
        trench.push(pos);
    }
    // apply shoelace formula
    let area = trench
        .iter()
        .zip(trench.iter().skip(1))
        .fold(0, |acc, (a, b)| acc + a.x * b.y - a.y * b.x)
        / 2;
    area + border / 2 + 1
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 62)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 952408144115)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 49578)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 52885384955882)
    }
}
