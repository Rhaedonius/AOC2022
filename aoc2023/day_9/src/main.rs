fn parse_seq(s: &str) -> Vec<i32> {
    s.split_whitespace().map(|n| n.parse().unwrap()).collect()
}

fn derive_seq(seq: Vec<i32>) -> Vec<i32> {
    let mut diff = Vec::new();
    let mut prev = seq[0];
    for n in seq.into_iter().skip(1) {
        diff.push(n - prev);
        prev = n;
    }
    diff
}

fn predict_sequence(seq: Vec<i32>) -> i32 {
    if seq.iter().all(|&v| v == 0) {
        return 0;
    }
    let last = seq[seq.len() - 1];
    let diff = derive_seq(seq);
    last + predict_sequence(diff)
}

fn predict_backwards(seq: Vec<i32>) -> i32 {
    if seq.iter().all(|&v| v == 0) {
        return 0;
    }
    let first = seq[0];
    let diff = derive_seq(seq);
    first - predict_backwards(diff)
}

fn question1(input: &str) -> i32 {
    input.lines().map(parse_seq).map(predict_sequence).sum()
}

fn question2(input: &str) -> i32 {
    input.lines().map(parse_seq).map(predict_backwards).sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = "0 3 6 9 12 15\n1 3 6 10 15 21\n10 13 16 21 30 45";
        assert_eq!(question1(input), 114)
    }

    #[test]
    fn test_example2() {
        let input = "0 3 6 9 12 15\n1 3 6 10 15 21\n10 13 16 21 30 45";
        assert_eq!(question2(input), 2)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 1969958987)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 1068)
    }
}
