use std::collections::{HashMap, HashSet, VecDeque};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Vector {
    x: usize,
    y: usize,
    z: usize,
}

impl From<&str> for Vector {
    fn from(input: &str) -> Self {
        let mut it = input.split(',');
        let x = it.next().unwrap().parse().unwrap();
        let y = it.next().unwrap().parse().unwrap();
        let z = it.next().unwrap().parse().unwrap();
        Vector { x, y, z }
    }
}

impl From<[usize; 3]> for Vector {
    fn from(value: [usize; 3]) -> Self {
        Vector {
            x: value[0],
            y: value[1],
            z: value[2],
        }
    }
}

impl std::ops::Sub for Vector {
    type Output = Vector;

    fn sub(self, other: Self) -> Self::Output {
        Vector {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Brick {
    position: Vector,
    size: Vector,
}

impl Brick {
    fn z_projection(&self) -> Vec<(usize, usize)> {
        (0..=self.size.x)
            .flat_map(|i| {
                (0..=self.size.y).map(move |j| {
                    let x = self.position.x + i;
                    let y = self.position.y + j;
                    (x, y)
                })
            })
            .collect()
    }
}

impl From<&str> for Brick {
    fn from(input: &str) -> Self {
        let (start, end) = input.split_once('~').unwrap();
        let position = Vector::from(start);
        let size = Vector::from(end) - position;
        Brick { position, size }
    }
}

#[derive(Debug, Clone)]
struct Connection {
    supported: HashSet<usize>,
    supporting: HashSet<usize>,
}

impl Connection {
    fn new() -> Self {
        Self {
            supported: HashSet::new(),
            supporting: HashSet::new(),
        }
    }
}

fn question1(input: &'static str) -> usize {
    let mut bricks: Vec<_> = input.lines().map(Brick::from).collect();
    let n = bricks.len();
    // HashMap: (x, y) -> (brick_index, z)
    // highest brick in that position
    let mut tower: HashMap<(usize, usize), (usize, usize)> = HashMap::new();
    let mut connections = vec![Connection::new(); n];
    bricks.sort_by_key(|b| b.position.z);
    for i in 0..n {
        let b = bricks[i];
        let proj = b.z_projection();
        let mut z = 0;
        let mut r = HashSet::new();
        for p in &proj {
            if let Some((h, j)) = tower.get(p) {
                if *h == z {
                    r.insert(*j);
                    continue;
                }
                if *h > z {
                    z = *h;
                    r = HashSet::from([*j]);
                }
            }
        }
        for c in r {
            connections[c].supporting.insert(i);
            connections[i].supported.insert(c);
        }
        for p in proj {
            let h = z + b.size.z + 1;
            tower.insert(p, (h, i));
        }
        bricks[i].position.z = z + 1;
    }
    let mut res = 0;
    for i in 0..n {
        if connections[i]
            .supporting
            .iter()
            .all(|&c| connections[c].supported.len() > 1)
        {
            res += 1
        }
    }
    res
}

fn chain_reaction(connections: &Vec<Connection>, i: usize) -> usize {
    // Very slow :( probably needs some optimization
    let mut destroyed = vec![i];
    let mut visitor = VecDeque::from_iter(connections[i].supporting.iter());
    while let Some(&c) = visitor.pop_front() {
        if connections[c]
            .supported
            .iter()
            .any(|j| !destroyed.contains(j))
        {
            continue;
        }
        destroyed.push(c);
        for j in &connections[c].supporting {
            visitor.push_back(&j);
        }
    }
    destroyed.sort();
    destroyed.dedup();
    destroyed.len() - 1
}

fn question2(input: &'static str) -> usize {
    let mut bricks: Vec<_> = input.lines().map(Brick::from).collect();
    let n = bricks.len();
    // HashMap: (x, y) -> (brick_index, z)
    // highest brick in that position
    let mut tower: HashMap<(usize, usize), (usize, usize)> = HashMap::new();
    let mut connections = vec![Connection::new(); n];
    bricks.sort_by_key(|b| b.position.z);
    for i in 0..n {
        let b = bricks[i];
        let proj = b.z_projection();
        let mut z = 0;
        let mut r = HashSet::new();
        for p in &proj {
            if let Some((h, j)) = tower.get(p) {
                if *h == z {
                    r.insert(*j);
                    continue;
                }
                if *h > z {
                    z = *h;
                    r = HashSet::from([*j]);
                }
            }
        }
        for c in r {
            connections[c].supporting.insert(i);
            connections[i].supported.insert(c);
        }
        for p in proj {
            let h = z + b.size.z + 1;
            tower.insert(p, (h, i));
        }
        bricks[i].position.z = z + 1;
    }
    let mut res = 0;
    for i in 0..n {
        res += chain_reaction(&connections, i);
    }
    res
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 5)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 7)
    }
    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 0)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 0)
    }
}
