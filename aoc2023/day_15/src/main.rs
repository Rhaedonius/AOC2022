#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Lens<'a> {
    label: &'a str,
    focal: usize,
}

fn hash(input: &str) -> usize {
    let mut res = 0;
    for c in input.bytes() {
        res = res + c as usize;
        res = ((res << 4) + res) & 255;
    }
    res
}

impl<'a> From<&'a str> for Lens<'a> {
    fn from(input: &'a str) -> Self {
        let (label, f) = input.split_once('=').unwrap();
        let focal = f.parse().unwrap();
        Lens { label, focal }
    }
}

fn question1(input: &'static str) -> usize {
    input.trim_end().split(',').map(hash).sum()
}

fn question2(input: &str) -> usize {
    let mut boxes = vec![Vec::<Lens>::new(); 256];
    for op in input.trim_end().split(',') {
        if op.ends_with('-') {
            // remove
            let label = &op[..op.len() - 1];
            let idx = hash(label);
            if let Some(i) = boxes[idx].iter().position(|l| l.label == label) {
                boxes[idx].remove(i);
            }
        } else {
            // add or replace
            let lens = Lens::from(op);
            let idx = hash(lens.label);
            match boxes[idx].iter().position(|l| l.label == lens.label) {
                Some(i) => boxes[idx][i] = lens,
                None => boxes[idx].push(lens),
            }
        }
    }
    boxes
        .iter()
        .enumerate()
        .flat_map(|(i, v)| {
            v.iter()
                .enumerate()
                .map(|(j, l)| (i + 1) * (j + 1) * l.focal)
                .collect::<Vec<usize>>()
        })
        .sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash() {
        assert_eq!(hash("HASH"), 52)
    }

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 1320)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 145)
    }

    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 501680)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 241094)
    }
}
