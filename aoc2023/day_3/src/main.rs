#[derive(Debug, Copy, Clone)]
struct Number {
    num: usize,
    row: usize,
    col: usize,
    len: usize,
}

#[derive(Debug)]
struct Symbol {
    gear: bool,
    row: usize,
    col: usize,
}

impl Number {
    fn from_row(row: usize) -> Self {
        Self {
            num: 0,
            row,
            col: 1,
            len: 0,
        }
    }
    fn is_adjacent(&self, sym: &Symbol) -> bool {
        self.col - 1 <= sym.col
            && sym.col <= self.col + self.len
            && self.row - 1 <= sym.row
            && sym.row <= self.row + 1
    }
    fn part_number(&self, sym: &[Symbol]) -> usize {
        if sym.iter().any(|s| self.is_adjacent(s)) {
            return self.num;
        }
        0
    }
}

impl Symbol {
    fn gear_ratio(&self, num: &[Number]) -> usize {
        if !self.gear {
            return 0;
        }
        let nums: Vec<usize> = num
            .iter()
            .filter(|n| n.is_adjacent(self))
            .map(|n| n.num)
            .collect();
        if nums.len() == 2 {
            return nums[0] * nums[1];
        }
        0
    }
}

fn parse_schematic(input: &str) -> (Vec<Number>, Vec<Symbol>) {
    let (mut num, mut sym) = (Vec::new(), Vec::new());
    for (i, line) in input.lines().enumerate() {
        let mut n = Number::from_row(i + 1);
        for (j, b) in line.bytes().enumerate() {
            match b {
                b'0'..=b'9' => {
                    n.num = n.num * 10 + usize::from(b - b'0');

                    if n.len == 0 {
                        n.col = j + 1;
                    }
                    n.len += 1;
                    continue;
                }
                b'.' => {}
                b'*' => sym.push(Symbol {
                    gear: true,
                    row: i + 1,
                    col: j + 1,
                }),
                _ => sym.push(Symbol {
                    gear: false,
                    row: i + 1,
                    col: j + 1,
                }),
            }
            if n.len > 0 {
                num.push(n)
            }
            n = Number::from_row(i + 1);
        }
        if n.len > 0 {
            num.push(n)
        }
    }
    (num, sym)
}

fn question1(input: &str) -> usize {
    let (num, sym) = parse_schematic(input);
    num.into_iter().map(|n| n.part_number(&sym)).sum()
}

fn question2(input: &str) -> usize {
    let (num, sym) = parse_schematic(input);
    sym.into_iter().map(|s| s.gear_ratio(&num)).sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_adjacent() {
        let n = Number {
            num: 0,
            row: 2,
            col: 3,
            len: 4,
        };
        assert!(n.is_adjacent(&Symbol {
            gear: false,
            row: 1,
            col: 2
        }));
        assert!(n.is_adjacent(&Symbol {
            gear: false,
            row: 1,
            col: 4
        }));
        assert!(n.is_adjacent(&Symbol {
            gear: false,
            row: 1,
            col: 7
        }));
        assert!(n.is_adjacent(&Symbol {
            gear: false,
            row: 3,
            col: 2
        }));
        assert!(n.is_adjacent(&Symbol {
            gear: false,
            row: 3,
            col: 4
        }));
        assert!(n.is_adjacent(&Symbol {
            gear: false,
            row: 3,
            col: 7
        }));
        assert!(!n.is_adjacent(&Symbol {
            gear: false,
            row: 0,
            col: 4
        }));
        assert!(!n.is_adjacent(&Symbol {
            gear: false,
            row: 2,
            col: 1
        }));
        assert!(!n.is_adjacent(&Symbol {
            gear: false,
            row: 2,
            col: 8
        }));
        assert!(!n.is_adjacent(&Symbol {
            gear: false,
            row: 4,
            col: 4
        }));
    }

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 4361)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 467835)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 537832)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 81939900)
    }
}
