#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Tile {
    Ash,
    Rock,
}

impl From<char> for Tile {
    fn from(value: char) -> Self {
        match value {
            '.' => Tile::Ash,
            '#' => Tile::Rock,
            _ => panic!("invalid input character for Tile"),
        }
    }
}

type Pattern = Vec<Vec<Tile>>;

fn find_reflections(p: &Pattern) -> Option<usize> {
    let rows = p.len();
    let cols = p[0].len();
    for i in 1..rows {
        if (0..i).rev().zip(i..rows).all(|(l, r)| p[l] == p[r]) {
            return Some(i * 100);
        }
    }
    for j in 1..cols {
        if (0..j)
            .rev()
            .zip(j..cols)
            .all(|(l, r)| (0..rows).all(|i| p[i][l] == p[i][r]))
        {
            return Some(j);
        }
    }
    None
}

/// find the row or col where difference between reflections is 1
fn find_smudge(p: &Pattern) -> Option<usize> {
    let rows = p.len();
    let cols = p[0].len();
    for i in 1..rows {
        if (0..i)
            .rev()
            .zip(i..rows)
            .map(|(l, r)| (0..cols).filter(|&j| p[l][j] != p[r][j]).count())
            .sum::<usize>()
            == 1
        {
            return Some(i * 100);
        }
    }
    for j in 1..cols {
        if (0..j)
            .rev()
            .zip(j..cols)
            .map(|(l, r)| (0..rows).filter(|&i| p[i][l] != p[i][r]).count())
            .sum::<usize>()
            == 1
        {
            return Some(j);
        }
    }
    None
}

fn parse_input(input: &'static str) -> Vec<Pattern> {
    input
        .split("\n\n")
        .map(|s| {
            s.lines()
                .map(|l| l.chars().map(Tile::from).collect())
                .collect()
        })
        .collect()
}

fn question1(input: &'static str) -> usize {
    parse_input(input).iter().filter_map(find_reflections).sum()
}

fn question2(input: &'static str) -> usize {
    parse_input(input).iter().filter_map(find_smudge).sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 405)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 400)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 27505)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 22906)
    }
}
