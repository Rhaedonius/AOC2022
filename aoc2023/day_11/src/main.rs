#[derive(Debug, Clone, Copy)]
struct Galaxy {
    x: usize,
    y: usize,
}

fn distance(a: &Galaxy, b: &Galaxy) -> usize {
    a.x.abs_diff(b.x) + a.y.abs_diff(b.y)
}

fn parse_input(input: &'static str) -> Vec<Galaxy> {
    input
        .lines()
        .enumerate()
        .flat_map(|(y, l)| {
            l.chars()
                .enumerate()
                .filter_map(|(x, c)| {
                    if c == '#' {
                        Some(Galaxy { x, y })
                    } else {
                        None
                    }
                })
                .collect::<Vec<_>>()
        })
        .collect()
}

fn empty_rows(galaxies: &Vec<Galaxy>) -> Vec<usize> {
    let l = galaxies.iter().map(|g| g.y).max().unwrap_or(0);
    (0..l)
        .filter(|&i| !galaxies.iter().any(|g| g.y == i))
        .collect()
}

fn empty_columns(galaxies: &Vec<Galaxy>) -> Vec<usize> {
    let l = galaxies.iter().map(|g| g.x).max().unwrap_or(0);
    (0..l)
        .filter(|&j| !galaxies.iter().any(|g| g.x == j))
        .collect()
}

fn expansion(galaxies: &Vec<Galaxy>, expansion_factor: usize) -> usize {
    let l = galaxies.len();
    let mut n = empty_rows(&galaxies)
        .into_iter()
        .map(|j| {
            let c = galaxies.iter().filter(|g| g.y < j).count();
            c * (l - c)
        })
        .sum::<usize>();
    n += empty_columns(&galaxies)
        .into_iter()
        .map(|i| {
            let c = galaxies.iter().filter(|g| g.x < i).count();
            c * (l - c)
        })
        .sum::<usize>();
    n * expansion_factor
}

fn total_distance(galaxies: Vec<Galaxy>, expansion_factor: usize) -> usize {
    let mut res = 0;
    let n = galaxies.len();
    for i in 0..(n - 1) {
        for j in (i + 1)..n {
            res += distance(&galaxies[i], &galaxies[j]);
        }
    }
    res + expansion(&galaxies, expansion_factor)
}

fn question1(input: &'static str) -> usize {
    let galaxies = parse_input(input);
    total_distance(galaxies, 1)
}

fn question2(input: &'static str) -> usize {
    let galaxies = parse_input(input);
    total_distance(galaxies, 999_999)
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_distance() {
        let input = "....#........
.........#...
#............
.............
.............
........#....
.#...........
............#
.............
.............
.........#...
#....#.......";
        let galaxies = parse_input(input);
        assert_eq!(total_distance(galaxies, 0), 374)
    }

    #[test]
    fn test_expansion() {
        let input = include_str!("../test.txt");
        let galaxies = parse_input(input);
        assert_eq!(expansion(&galaxies, 1), 82)
    }

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 374)
    }

    #[test]
    fn test_example2() {
        let galaxies = parse_input(include_str!("../test.txt"));
        assert_eq!(total_distance(galaxies.clone(), 9), 1030);
        assert_eq!(total_distance(galaxies, 99), 8410)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 9403026)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 543018317006)
    }
}
