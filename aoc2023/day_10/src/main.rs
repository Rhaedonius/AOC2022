use std::collections::HashSet;

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
enum Tile {
    Horizontal,
    Vertical,
    NEBend,
    NWBend,
    SEBend,
    SWBend,
    Start,
    Empty,
}

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
struct Position {
    x: usize,
    y: usize,
    tile: Tile,
}

type PipeMaze = Vec<Vec<Position>>;
type Loop = Vec<Position>;

impl Position {
    fn new(x: usize, y: usize, tile: Tile) -> Self {
        Self { x, y, tile }
    }
    fn step(&self, prev: &Position, tiles: &PipeMaze) -> Position {
        match self.tile {
            Tile::Horizontal => match prev.tile {
                Tile::NEBend | Tile::SEBend => tiles[self.y][self.x + 1],
                Tile::NWBend | Tile::SWBend => tiles[self.y][self.x - 1],
                Tile::Horizontal | Tile::Start => {
                    if self.x > prev.x {
                        tiles[self.y][self.x + 1]
                    } else {
                        tiles[self.y][self.x - 1]
                    }
                }
                Tile::Vertical | Tile::Empty => {
                    panic!("invalid path from {:?} to {:?}", prev, self)
                }
            },
            Tile::Vertical => match prev.tile {
                Tile::NEBend | Tile::NWBend => tiles[self.y + 1][self.x],
                Tile::SEBend | Tile::SWBend => tiles[self.y - 1][self.x],
                Tile::Vertical | Tile::Start => {
                    if self.y > prev.y {
                        tiles[self.y + 1][self.x]
                    } else {
                        tiles[self.y - 1][self.x]
                    }
                }
                Tile::Horizontal | Tile::Empty => {
                    panic!("invalid path from {:?} to {:?}", prev, self)
                }
            },
            Tile::NEBend => match prev.tile {
                Tile::Vertical | Tile::SEBend | Tile::SWBend | Tile::Start if self.x == prev.x => {
                    tiles[self.y][self.x + 1]
                }
                Tile::Horizontal | Tile::NWBend | Tile::SWBend | Tile::Start
                    if self.y == prev.y =>
                {
                    tiles[self.y + 1][self.x]
                }
                _ => panic!("invalid path from {:?} to {:?}", prev, self),
            },
            Tile::NWBend => match prev.tile {
                Tile::Vertical | Tile::SEBend | Tile::SWBend | Tile::Start if self.x == prev.x => {
                    tiles[self.y][self.x - 1]
                }
                Tile::Horizontal | Tile::NEBend | Tile::SEBend | Tile::Start
                    if self.y == prev.y =>
                {
                    tiles[self.y + 1][self.x]
                }
                _ => panic!("invalid path from {:?} to {:?}", prev, self),
            },
            Tile::SEBend => match prev.tile {
                Tile::Vertical | Tile::NEBend | Tile::NWBend | Tile::Start if self.x == prev.x => {
                    tiles[self.y][self.x + 1]
                }
                Tile::Horizontal | Tile::NWBend | Tile::SWBend | Tile::Start
                    if self.y == prev.y =>
                {
                    tiles[self.y - 1][self.x]
                }
                _ => panic!("invalid path from {:?} to {:?}", prev, self),
            },
            Tile::SWBend => match prev.tile {
                Tile::Vertical | Tile::NEBend | Tile::NWBend | Tile::Start if self.x == prev.x => {
                    tiles[self.y][self.x - 1]
                }
                Tile::Horizontal | Tile::NEBend | Tile::SEBend | Tile::Start
                    if self.y == prev.y =>
                {
                    tiles[self.y - 1][self.x]
                }
                _ => panic!("invalid path from {:?} to {:?}", prev, self),
            },
            Tile::Start => {
                if self.y > 0
                    && vec![Tile::Vertical, Tile::NWBend, Tile::NEBend]
                        .contains(&tiles[self.y - 1][self.x].tile)
                {
                    return tiles[self.y - 1][self.x];
                }
                if self.x < tiles[0].len()
                    && vec![Tile::Horizontal, Tile::SWBend, Tile::NWBend]
                        .contains(&tiles[self.y][self.x + 1].tile)
                {
                    return tiles[self.y][self.x + 1];
                }
                if self.y < tiles.len()
                    && vec![Tile::Vertical, Tile::SEBend, Tile::SWBend]
                        .contains(&tiles[self.y + 1][self.x].tile)
                {
                    return tiles[self.y + 1][self.x];
                }
                if self.x > 0
                    && vec![Tile::Horizontal, Tile::NEBend, Tile::SEBend]
                        .contains(&tiles[self.y][self.x - 1].tile)
                {
                    return tiles[self.y][self.x - 1];
                }
                panic!("invalid path from {:?} to {:?}", prev, self)
            }
            Tile::Empty => panic!("invalid path from {:?} to {:?}", prev, self),
        }
    }
}

impl From<char> for Tile {
    fn from(value: char) -> Self {
        match value {
            '-' => Tile::Horizontal,
            '|' => Tile::Vertical,
            'F' => Tile::NEBend,
            '7' => Tile::NWBend,
            'L' => Tile::SEBend,
            'J' => Tile::SWBend,
            'S' => Tile::Start,
            '.' => Tile::Empty,
            _ => panic!("invalid tile character"),
        }
    }
}

fn parse_input(input: &'static str) -> (Position, PipeMaze) {
    let mut start = Position::new(0, 0, Tile::Empty);
    let mut res = Vec::new();
    for (y, l) in input.lines().enumerate() {
        let mut line = Vec::new();
        for (x, tile) in l.chars().map(Tile::from).enumerate() {
            let c = Position { x, y, tile };
            if tile == Tile::Start {
                start = c;
            }
            line.push(c);
        }
        res.push(line);
    }
    (start, res)
}

fn find_loop_tiles(start: Position, tiles: &PipeMaze) -> Loop {
    let mut res = vec![start];
    let mut pos = start.step(&Position::new(0, 0, Tile::Empty), &tiles);
    while pos != start {
        let prev = res.last().unwrap().to_owned();
        res.push(pos);
        pos = pos.step(&prev, &tiles);
    }
    res
}

fn question1(input: &'static str) -> usize {
    let (start, tiles) = parse_input(input);
    (find_loop_tiles(start, &tiles).len() + 1) / 2
}

fn count_points_inside_loop(tiles: Vec<Vec<Position>>, loop_tiles: HashSet<Position>) -> usize {
    let mut res = 0;
    for row in tiles {
        let mut inside = false;
        for t in row {
            if loop_tiles.contains(&t) {
                inside = match t.tile {
                    // a border of type L------7 should be counted only once
                    // a border like F----7 is enter + exit, so we count only south tile
                    Tile::SEBend | Tile::SWBend | Tile::Vertical | Tile::Start => !inside,
                    _ => inside,
                };
                continue;
            }
            if inside {
                res += 1
            } else {
            }
        }
    }
    res
}

fn question2(input: &'static str) -> usize {
    let (start, tiles) = parse_input(input);
    let loop_tiles: HashSet<Position> =
        HashSet::from_iter(find_loop_tiles(start, &tiles).into_iter());
    count_points_inside_loop(tiles, loop_tiles)
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = "-L|F7\n7S-7|\nL|7||\n-L-J|\nL|-JF";
        assert_eq!(question1(input), 4)
    }

    #[test]
    fn test_example2() {
        let input = "FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJ.F7FJ-
L---JF-JLJ....FJLJJ7
|F|F-JF---7...L7L|7|
|FFJF7L7F-JF7..L---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L";
        assert_eq!(question2(input), 10)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 6613)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 511)
    }
}
