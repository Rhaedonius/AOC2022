use std::collections::HashMap;
use std::collections::VecDeque;
use std::ops::RangeInclusive;

#[derive(Debug, Clone, Copy)]
enum Step<'a> {
    Accepted,
    Rejected,
    Workflow(&'a str),
}

impl<'a> From<&'a str> for Step<'a> {
    fn from(value: &'a str) -> Self {
        match value {
            "A" => Step::Accepted,
            "R" => Step::Rejected,
            v => Step::Workflow(v),
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
struct Part {
    x: usize,
    m: usize,
    a: usize,
    s: usize,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct PartRange {
    x: RangeInclusive<usize>,
    m: RangeInclusive<usize>,
    a: RangeInclusive<usize>,
    s: RangeInclusive<usize>,
}

impl PartRange {
    fn new() -> Self {
        PartRange {
            x: 1..=4000,
            m: 1..=4000,
            a: 1..=4000,
            s: 1..=4000,
        }
    }
    fn get_range(&self, field: char) -> (usize, usize) {
        match field {
            'x' => (*self.x.start(), *self.x.end()),
            'm' => (*self.m.start(), *self.m.end()),
            'a' => (*self.a.start(), *self.a.end()),
            's' => (*self.s.start(), *self.s.end()),
            v => panic!("invalid field name: {v}"),
        }
    }
    fn modify_range(&self, field: char, range: RangeInclusive<usize>) -> PartRange {
        let mut res = self.clone();
        match field {
            'x' => res.x = range,
            'm' => res.m = range,
            'a' => res.a = range,
            's' => res.s = range,
            v => panic!("invalid field name: {v}"),
        }
        res
    }
}

impl Part {
    fn rating(&self) -> usize {
        self.x + self.m + self.a + self.s
    }
}

impl From<&str> for Part {
    fn from(value: &str) -> Self {
        let (mut x, mut m, mut a, mut s) = (0, 0, 0, 0);
        for e in value[1..value.len() - 1].split(',') {
            let (field, value) = e.split_once('=').unwrap();
            match field {
                "x" => x = value.parse().unwrap(),
                "m" => m = value.parse().unwrap(),
                "a" => a = value.parse().unwrap(),
                "s" => s = value.parse().unwrap(),
                v => panic!("invalid field name in part: {v}"),
            }
        }
        Part { x, m, a, s }
    }
}

#[derive(Debug, Clone, Copy)]
struct Condition {
    field: char,
    op: char,
    value: usize,
}

#[derive(Debug, Clone, Copy)]
struct Rule<'a> {
    condition: Option<Condition>,
    next: Step<'a>,
}

fn apply_rule<'a>(rule: &'a Rule, part: &'a Part) -> Option<Step<'a>> {
    if rule.condition.is_none() {
        return Some(rule.next);
    }
    let condition = rule.condition.unwrap();
    let field = match condition.field {
        'x' => part.x,
        'm' => part.m,
        'a' => part.a,
        's' => part.s,
        v => panic!("invalid field value in condition: {}", v),
    };
    match condition.op {
        '<' => {
            if field < condition.value {
                return Some(rule.next);
            }
        }
        '>' => {
            if field > condition.value {
                return Some(rule.next);
            }
        }
        v => panic!("unexpected condition value: {}", v),
    }
    None
}

fn apply_rule_range<'a>(rules: &Vec<Rule<'a>>, mut part: PartRange) -> Vec<(Step<'a>, PartRange)> {
    let mut res = Vec::new();
    for rule in rules {
        if rule.condition.is_none() {
            let next_range = (rule.next, part.clone());
            res.push(next_range);
            continue;
        }
        let condition = rule.condition.unwrap();
        let (range_start, range_end) = part.get_range(condition.field);
        match condition.op {
            '<' => {
                let next_range = (
                    rule.next,
                    part.modify_range(condition.field, range_start..=condition.value - 1),
                );
                res.push(next_range);
                part = part.modify_range(condition.field, condition.value..=range_end);
            }
            '>' => {
                let next_range = (
                    rule.next,
                    part.modify_range(condition.field, condition.value + 1..=range_end),
                );
                res.push(next_range);
                part = part.modify_range(condition.field, range_start..=condition.value)
            }
            v => panic!("invalid comparison operator {v}"),
        };
    }
    res
}

impl From<&str> for Condition {
    fn from(input: &str) -> Self {
        let field = input.chars().nth(0).unwrap();
        let op = input.chars().nth(1).unwrap();
        let value = input[2..].parse().unwrap();
        Condition { field, op, value }
    }
}

impl<'a> From<&'a str> for Rule<'a> {
    fn from(input: &'a str) -> Self {
        if !input.contains(':') {
            return Rule {
                condition: None,
                next: Step::from(input),
            };
        }
        let (cond, step) = input.split_once(':').unwrap();
        Rule {
            condition: Some(Condition::from(cond)),
            next: Step::from(step),
        }
    }
}

fn question1(input: &'static str) -> usize {
    let (wf, p) = input.split_once("\n\n").unwrap();
    let workflows: HashMap<&str, Vec<Rule>> = wf
        .lines()
        .filter_map(|l| l.split_once('{'))
        .map(|(n, r)| {
            let rules: Vec<Rule> = r[..r.len() - 1].split(',').map(Rule::from).collect();
            (n, rules)
        })
        .collect();
    let parts: Vec<Part> = p.lines().map(Part::from).collect();
    let mut res = 0;
    for part in parts {
        let mut step = "in";
        loop {
            if let Some(rules) = workflows.get(step) {
                match rules.iter().find_map(|r| apply_rule(r, &part)) {
                    Some(Step::Accepted) => {
                        res += part.rating();
                        break;
                    }
                    Some(Step::Rejected) => break,
                    Some(Step::Workflow(v)) => step = v,
                    None => panic!("no matching rule found for {:?}", part),
                }
            }
        }
    }
    res
}

fn question2(input: &'static str) -> usize {
    let (wf, _) = input.split_once("\n\n").unwrap();
    let workflows: HashMap<&str, Vec<Rule>> = wf
        .lines()
        .filter_map(|l| l.split_once('{'))
        .map(|(n, r)| {
            let rules: Vec<Rule> = r[..r.len() - 1].split(',').map(Rule::from).collect();
            (n, rules)
        })
        .collect();
    let mut queue = VecDeque::from([("in", PartRange::new())]);
    let mut res = Vec::new();
    while let Some((wf, part)) = queue.pop_front() {
        let rules = workflows.get(wf).unwrap();
        for (s, p) in apply_rule_range(rules, part) {
            match s {
                Step::Accepted => res.push(p),
                Step::Rejected => {}
                Step::Workflow(w) => queue.push_back((w, p)),
            }
        }
    }
    res.into_iter()
        .map(|p| p.x.count() * p.m.count() * p.a.count() * p.s.count())
        .sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_part_parsing() {
        let part = Part::from("{x=787,m=2655,a=1222,s=2876}");
        assert_eq!(
            part,
            Part {
                x: 787,
                m: 2655,
                a: 1222,
                s: 2876
            }
        )
    }

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 19114)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 167409079868000)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 373302)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 130262715574114)
    }
}
