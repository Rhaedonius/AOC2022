#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum SpringStatus {
    Operational,
    Damaged,
    Unknown,
}

#[derive(Debug, Clone)]
struct ConditionRecord {
    springs: Vec<SpringStatus>,
    damaged: Vec<usize>,
    unknown: usize,
}

fn check_record(springs: Vec<SpringStatus>, damaged: &Vec<usize>) -> bool {
    let mut check = Vec::new();
    let mut count = 0;
    for s in springs {
        match s {
            SpringStatus::Damaged => {
                count += 1;
            }
            SpringStatus::Operational if count > 0 => {
                check.push(count);
                count = 0
            }
            _ => {}
        }
    }
    if count > 0 {
        check.push(count)
    }
    check == *damaged
}

impl ConditionRecord {
    fn count_arrangements(&self) -> usize {
        let mut res = 0;
        for i in 0..2usize.pow(self.unknown.try_into().unwrap()) {
            let mut filled = Vec::new();
            let mut fill_count = 0;
            for s in &self.springs {
                match s {
                    SpringStatus::Operational | SpringStatus::Damaged => filled.push(*s),
                    SpringStatus::Unknown => {
                        if (i >> fill_count) % 2 == 0 {
                            filled.push(SpringStatus::Damaged);
                        } else {
                            filled.push(SpringStatus::Operational);
                        }
                        fill_count += 1;
                    }
                }
            }
            if check_record(filled, &self.damaged) {
                res += 1;
            }
        }
        res
    }
}

impl From<&str> for ConditionRecord {
    fn from(value: &str) -> Self {
        let (record, check) = value.split_once(" ").unwrap();
        let mut unknown = 0;
        let springs = record
            .chars()
            .map(|c| match c {
                '#' => SpringStatus::Damaged,
                '.' => SpringStatus::Operational,
                '?' => {
                    unknown += 1;
                    SpringStatus::Unknown
                }
                _ => panic!("Unknown input character"),
            })
            .collect();
        let damaged = check.split(',').map(|d| d.parse().unwrap()).collect();
        Self {
            springs,
            damaged,
            unknown,
        }
    }
}

fn question1(input: &'static str) -> usize {
    input
        .lines()
        .map(|l| ConditionRecord::from(l).count_arrangements())
        .sum()
}

fn question2(input: &str) -> usize {
    return input.len();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_check() {
        let mut record = ConditionRecord::from(".###.##..#. 3,2,1");
        assert!(check_record(record.springs, &record.damaged));
        record = ConditionRecord::from(".###.##..# 3,2,1");
        assert!(check_record(record.springs, &record.damaged));
        record = ConditionRecord::from(".##.###..#. 3,2,1");
        assert!(!check_record(record.springs, &record.damaged));
        record = ConditionRecord::from(".###.##.##.# 3,2");
        assert!(!check_record(record.springs, &record.damaged));
    }

    #[test]
    fn test_generate_filled() {
        let record = ConditionRecord::from("???.### 1,1,3");
        assert_eq!(record.count_arrangements(), 1);
    }

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 21)
    }

    #[test]
    #[ignore]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 0)
    }
    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 0)
    }

    #[test]
    #[ignore]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 0)
    }
}
