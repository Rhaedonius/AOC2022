use std::collections::{HashMap, VecDeque};

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
enum Tile {
    Empty,
    SplitUD,
    SplitLR,
    MirrorLURD,
    MirrorLDRU,
}

impl From<char> for Tile {
    fn from(c: char) -> Self {
        match c {
            '.' => Tile::Empty,
            '-' => Tile::SplitLR,
            '|' => Tile::SplitUD,
            '/' => Tile::MirrorLURD,
            '\\' => Tile::MirrorLDRU,
            _ => panic!("incorrect character in input sequence"),
        }
    }
}

type Contraption = Vec<Vec<Tile>>;

type Position = (usize, usize);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
struct Beam {
    direction: Direction,
    position: Position,
}

impl Beam {
    fn new() -> Self {
        Beam {
            direction: Direction::Right,
            position: (0, 0),
        }
    }
    fn change_direction(&self, direction: Direction) -> Self {
        Beam {
            position: self.position,
            direction,
        }
    }
    fn split_beam(&self, map: &Contraption) -> Vec<Beam> {
        match map[self.position.0][self.position.1] {
            Tile::SplitUD
                if self.direction == Direction::Left || self.direction == Direction::Right =>
            {
                vec![
                    self.change_direction(Direction::Up),
                    self.change_direction(Direction::Down),
                ]
            }
            Tile::SplitLR
                if self.direction == Direction::Up || self.direction == Direction::Down =>
            {
                vec![
                    self.change_direction(Direction::Left),
                    self.change_direction(Direction::Right),
                ]
            }
            Tile::MirrorLURD => match self.direction {
                Direction::Up => vec![self.change_direction(Direction::Right)],
                Direction::Down => vec![self.change_direction(Direction::Left)],
                Direction::Left => vec![self.change_direction(Direction::Down)],
                Direction::Right => vec![self.change_direction(Direction::Up)],
            },
            Tile::MirrorLDRU => match self.direction {
                Direction::Up => vec![self.change_direction(Direction::Left)],
                Direction::Down => vec![self.change_direction(Direction::Right)],
                Direction::Left => vec![self.change_direction(Direction::Up)],
                Direction::Right => vec![self.change_direction(Direction::Down)],
            },
            _ => vec![self.clone()],
        }
    }
}

fn step(map: &Contraption, beam: &Beam) -> Option<Beam> {
    let rows = map.len();
    let cols = map[0].len();
    match beam.direction {
        Direction::Up if beam.position.0 > 0 => Some(Beam {
            direction: beam.direction,
            position: (beam.position.0 - 1, beam.position.1),
        }),
        Direction::Down if beam.position.0 < rows - 1 => Some(Beam {
            direction: beam.direction,
            position: (beam.position.0 + 1, beam.position.1),
        }),
        Direction::Left if beam.position.1 > 0 => Some(Beam {
            direction: beam.direction,
            position: (beam.position.0, beam.position.1 - 1),
        }),
        Direction::Right if beam.position.1 < cols - 1 => Some(Beam {
            direction: beam.direction,
            position: (beam.position.0, beam.position.1 + 1),
        }),
        _ => None,
    }
}

fn parse_input(input: &'static str) -> Contraption {
    input
        .lines()
        .map(|l| l.chars().map(Tile::from).collect())
        .collect()
}

fn count_energized(map: &Contraption, start: Beam) -> usize {
    let mut beams = VecDeque::from([start]);
    let mut dp: HashMap<Position, Vec<Direction>> = HashMap::new();
    loop {
        let beam = match beams.pop_front() {
            Some(b) => b,
            None => break,
        };
        match dp.get_mut(&beam.position) {
            Some(v) => {
                // cycle detected stop beam
                if v.contains(&beam.direction) {
                    continue;
                } else {
                    v.push(beam.direction)
                }
            }
            None => {
                dp.insert(beam.position, vec![beam.direction]);
            }
        }
        for b in beam.split_beam(&map).iter().filter_map(|b| step(&map, b)) {
            beams.push_back(b);
        }
    }
    dp.len()
}

fn question1(input: &'static str) -> usize {
    let map = parse_input(input);
    count_energized(&map, Beam::new())
}

fn question2(input: &'static str) -> usize {
    let map = parse_input(input);
    let rows = map.len();
    let cols = map[0].len();
    let res = (0..rows)
        .flat_map(|i| {
            vec![
                count_energized(
                    &map,
                    Beam {
                        position: (i, 0),
                        direction: Direction::Right,
                    },
                ),
                count_energized(
                    &map,
                    Beam {
                        position: (i, cols - 1),
                        direction: Direction::Left,
                    },
                ),
            ]
        })
        .max()
        .unwrap();
    res.max(
        (0..cols)
            .flat_map(|j| {
                vec![
                    count_energized(
                        &map,
                        Beam {
                            position: (0, j),
                            direction: Direction::Down,
                        },
                    ),
                    count_energized(
                        &map,
                        Beam {
                            position: (rows - 1, j),
                            direction: Direction::Up,
                        },
                    ),
                ]
            })
            .max()
            .unwrap(),
    )
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 46)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 51)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 7199)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 7438)
    }
}
