use std::collections::{HashMap, VecDeque};

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
enum Pulse {
    Low,
    High,
}

#[derive(Debug, Clone, Hash, PartialEq, Eq)]
enum Module {
    FlipFlop {
        on: bool,
    },
    Conjunction {
        inputs: Vec<String>,
        pulses: Vec<Pulse>,
    },
    Broadcaster,
}

#[derive(Debug)]
struct Signal<'a> {
    from: &'a str,
    to: &'a str,
    pulse: Pulse,
}

impl<'a> Signal<'a> {
    /// returns the initial signal `button -low-> broadcaster`
    fn new() -> Self {
        Signal {
            from: "button",
            to: "broadcaster",
            pulse: Pulse::Low,
        }
    }
}

impl<'a> std::fmt::Display for Signal<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(
            f,
            "{} {} {}",
            self.from,
            match self.pulse {
                Pulse::Low => "-low->",
                Pulse::High => "-high->",
            },
            self.to
        )
    }
}

impl Module {
    /// handles reponse to a `signal` for each `Module` variant
    ///
    /// FlipFlop:
    ///     ignores High, flips on/off on Low and output follows
    /// Conjunction:
    ///     with one input doesn't need state as it's an inverter
    ///     with multiple inputs is a nand gate
    /// Broadcaster:
    ///     just copies the input
    fn trigger(&mut self, signal: Signal) -> Option<Pulse> {
        match self {
            Module::FlipFlop { ref mut on } => match signal.pulse {
                Pulse::Low => {
                    *on = !*on;
                    if *on {
                        Some(Pulse::High)
                    } else {
                        Some(Pulse::Low)
                    }
                }
                Pulse::High => None,
            },
            Module::Conjunction {
                inputs,
                ref mut pulses,
            } => {
                if inputs.len() == 1 {
                    // This works as an inverter
                    return match signal.pulse {
                        Pulse::Low => Some(Pulse::High),
                        Pulse::High => Some(Pulse::Low),
                    };
                }
                let idx = (0..inputs.len())
                    .find(|&i| inputs[i] == signal.from)
                    .expect("unexpected input for module");
                pulses[idx] = signal.pulse;
                if pulses.iter().any(|p| p == &Pulse::Low) {
                    Some(Pulse::High)
                } else {
                    Some(Pulse::Low)
                }
            }
            Module::Broadcaster => Some(signal.pulse),
        }
    }
}

type Graph<'a> = HashMap<&'a str, Vec<&'a str>>;

/// Returns a key-value pair for the adjacency list
fn parse_line(l: &str) -> (&str, Vec<&str>) {
    let pat: &[_] = &['%', '&'];
    let (s1, s2) = l.split_once(" -> ").unwrap();
    let k = s1.trim_start_matches(pat);
    let v = s2.split(", ").collect();
    (k, v)
}

/// Based on the graph, creates the mapping of module names to the appropriate Module variant
fn parse_modules(input: &'static str, graph: &Graph) -> HashMap<&'static str, Module> {
    input
        .lines()
        .filter_map(|l| {
            let (k, _) = parse_line(l);
            if !graph.contains_key(k) {
                return None;
            }
            match l.chars().nth(0) {
                Some('b') => Some(("broadcaster", Module::Broadcaster)),
                Some('%') => Some((k, Module::FlipFlop { on: false })),
                Some('&') => {
                    let inputs: Vec<String> = graph
                        .iter()
                        .filter_map(|(i, v)| {
                            if v.contains(&k) {
                                Some(String::from(*i))
                            } else {
                                None
                            }
                        })
                        .collect();
                    let pulses = vec![Pulse::Low; inputs.len()];
                    Some((k, Module::Conjunction { inputs, pulses }))
                }
                _ => unreachable!("unexpected value for module type"),
            }
        })
        .collect()
}

/// simulate the result of a button press, returning the counts of low and high pulses
fn button_press(modules: &mut HashMap<&str, Module>, graph: &Graph) -> (usize, usize) {
    let mut pulses = VecDeque::from([Signal::new()]);
    let (mut low, mut high) = (1, 0);
    while let Some(s) = pulses.pop_front() {
        let from = s.to;
        let m = match modules.get_mut(from) {
            Some(m) => m,
            None => continue,
        };
        let pulse = match m.trigger(s) {
            Some(p) => p,
            None => continue,
        };
        if graph.get(from).is_none() {
            println!("{from}, {pulses:?}")
        }
        for to in graph.get(from).expect("destination module not in graph") {
            match pulse {
                Pulse::Low => low += 1,
                Pulse::High => high += 1,
            }
            pulses.push_back(Signal { from, to, pulse })
        }
    }
    (low, high)
}

/// The `broadcast` module feeds separate graphs that are joined with a `Conjuntion` module to feed `rx`.
/// Split the separate subgraphs so cycles can be detected for each one
fn get_subgraphs<'a>(graph: &'a Graph) -> Vec<Graph<'a>> {
    let mut res = Vec::new();
    let start: Vec<&str> = graph
        .get("broadcaster")
        .expect("broadcast module missing from the graph")
        .clone();
    let end: Vec<&str> = graph
        .iter()
        .filter_map(|(k, v)| if v.contains(&"rx") { Some(*k) } else { None })
        .collect();
    for s in start {
        let mut visitor = VecDeque::from([s]);
        let mut g = Graph::new();
        g.insert("broadcaster", vec![s]);
        while let Some(s) = visitor.pop_front() {
            if end.contains(&s) {
                continue;
            }
            let t = graph.get(s).expect("module missing from graph").to_owned();
            for n in &t {
                if !g.contains_key(n) {
                    visitor.push_back(&n)
                }
            }
            g.insert(s, t);
        }
        res.push(g);
    }
    res
}

/// Each subgraph works like a linear feedback shift register using the FlipFlop state
/// When all the FlipFlop connected to the Nand gate are on the output is triggered
/// and the sequence is reset (all FF to off).
/// The period of the LFSR is equal to the binary number with 1s where the FlipFlops
/// are connected to the Nand gate (first FF is the least significative)
fn cycle_from_graph(graph: &Graph, curr: &str, nand: &str) -> usize {
    let output = graph.get(curr).expect("invalid graph node");
    if curr == "broadcaster" {
        return cycle_from_graph(graph, output[0], nand);
    }
    if output.len() == 1 {
        if output[0] == nand {
            return 1;
        }
        return cycle_from_graph(graph, output[0], nand) << 1;
    }
    if output[0] == nand {
        return 0x01 | cycle_from_graph(graph, output[1], nand) << 1;
    }
    0x01 | cycle_from_graph(graph, output[0], nand) << 1
}

fn question1(input: &'static str) -> usize {
    let graph: Graph = input.lines().map(parse_line).collect();
    let mut modules = parse_modules(input, &graph);
    let (mut low, mut high) = (0, 0);
    // cycle lenght is bigger than 1000 for real input, no need for dp here
    for _ in 0..1000 {
        let (l, h) = button_press(&mut modules, &graph);
        low += l;
        high += h;
    }
    low * high
}

fn question2(input: &'static str) -> usize {
    let graph: Graph = input.lines().map(parse_line).collect();
    let mut res = 1;
    for g in get_subgraphs(&graph) {
        let modules: HashMap<&str, Module> = parse_modules(input, &g);
        // there is only one nand module per subgraph (the other Conjunction is an inverter)
        let nand = modules
            .iter()
            .find_map(|(k, m)| {
                if matches!(m, Module::Conjunction{inputs, pulses:_} if inputs.len() > 1) {
                    Some(*k)
                } else {
                    None
                }
            })
            .expect("no conjunction module found");
        res *= cycle_from_graph(&g, "broadcaster", nand);
    }
    res
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 11687500)
    }

    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 841763884)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 246006621493687)
    }
}
