use std::{
    fmt::Debug,
    time::{SystemTime, UNIX_EPOCH},
};

#[derive(Clone)]
struct Graph<'a> {
    nodes: Vec<&'a str>,
    edges: Vec<(usize, usize)>,
}

impl<'a> Debug for Graph<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "Graph {{")?;
        let n_nodes = self.nodes.len();
        write!(f, "  nodes: [")?;
        for i in 0..n_nodes - 1 {
            write!(f, "{}, ", self.nodes[i])?;
        }
        writeln!(f, "{}]", self.nodes[n_nodes - 1])?;
        write!(f, "  edges: [")?;
        for (i, j) in &self.edges {
            write!(f, "{}-{}", self.nodes[*i], self.nodes[*j])?;
            if Some(&(*i, *j)) != self.edges.last() {
                write!(f, ", ")?;
            }
        }
        writeln!(f, "}}")?;
        writeln!(f, "")
    }
}

struct Rng {
    prev: usize,
}

impl Rng {
    /// Creates a new [`Rng`].
    fn new() -> Self {
        let seed = SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_micros();
        Self {
            prev: seed as usize,
        }
    }
    /// Returns a random usize value.
    fn rand(&mut self) -> usize {
        self.prev = self.prev.wrapping_mul(6364136223846793005);
        self.prev = self.prev.wrapping_add(1);
        return self.prev;
    }
    /// Selects a random item from `v`
    fn choice<T: Copy>(&mut self, v: &Vec<T>) -> T {
        let n = v.len();
        v[self.rand() % n]
    }
}

impl<'a> From<&'a str> for Graph<'a> {
    fn from(input: &'a str) -> Self {
        let mut nodes: Vec<&str> = Vec::new();
        let mut edges: Vec<_> = Vec::new();
        for line in input.lines() {
            let (n, rem) = line.split_once(": ").unwrap();
            let i = match nodes.iter().position(|&e| e == n) {
                Some(i) => i,
                None => {
                    nodes.push(n);
                    nodes.len() - 1
                }
            };
            for v in rem.split_whitespace() {
                let j = match nodes.iter().position(|&e| e == v) {
                    Some(j) => j,
                    None => {
                        nodes.push(v);
                        nodes.len() - 1
                    }
                };
                edges.push((i, j));
            }
        }
        Graph { nodes, edges }
    }
}

impl<'a> Graph<'a> {
    /// Generate a random cut of this [`Graph`] using Karger's algorithm.
    fn random_cut(&self) -> (usize, Vec<usize>) {
        let mut g = self.clone();
        let mut rng = Rng::new();
        let mut counts = vec![1usize; self.nodes.len()];
        while counts.iter().filter(|&&t| t > 0).count() > 2 {
            let (s, t) = rng.choice(&g.edges);
            counts[s] += counts[t];
            counts[t] = 0;
            g.edges = g
                .edges
                .iter()
                .filter_map(|&e| match e {
                    // remove self loop
                    e if e == (s, t) || e == (t, s) => None,
                    // contract t in s
                    e if e.1 == t => Some((e.0, s)),
                    e if e.0 == t => Some((s, e.1)),
                    // leave others untouched
                    e => Some(e),
                })
                .collect();
        }
        (
            g.edges.len(),
            counts.into_iter().filter(|&t| t > 0).collect(),
        )
    }

    /// finds the cut of size `n` by repeating Karger's algorithm
    fn min_cut(&self, n: usize) -> usize {
        loop {
            let (e, v) = self.random_cut();
            if e == n {
                return v.iter().product();
            }
        }
    }
}

fn question1(input: &'static str) -> usize {
    let g = Graph::from(input);
    g.min_cut(3)
}

fn question2(input: &'static str) -> usize {
    return input.len();
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 54)
    }

    #[test]
    #[ignore]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 596376)
    }
}
