#[derive(Debug, Copy, Clone)]
struct Cubes {
    red: usize,
    green: usize,
    blue: usize,
}

impl Cubes {
    fn new() -> Self {
        Cubes {
            red: 0,
            green: 0,
            blue: 0,
        }
    }
    fn contains(&self, other: &Self) -> bool {
        return (self.red >= other.red) & (self.green >= other.green) & (self.blue >= other.blue);
    }
    fn power(&self) -> usize {
        return self.red * self.blue * self.green;
    }
    fn max(&self, other: &Self) -> Self {
        Self {
            red: self.red.max(other.red),
            blue: self.blue.max(other.blue),
            green: self.green.max(other.green),
        }
    }
}

impl From<&str> for Cubes {
    fn from(s: &str) -> Self {
        let (red, green, blue) = s.split(", ").fold((0, 0, 0), |(r, g, b), cube| {
            let (num, color) = cube
                .split_once(' ')
                .expect("invalid input format: missing space between number and color");
            let n = num
                .parse::<usize>()
                .expect("invalid number format in input");
            match color {
                "red" => (r + n, g, b),
                "green" => (r, g + n, b),
                "blue" => (r, g, b + n),
                _ => unreachable!("unknown color find in input"),
            }
        });
        Self { red, green, blue }
    }
}

#[derive(Debug)]
struct Game {
    id: usize,
    turns: Vec<Cubes>,
}

impl Game {
    fn valid_id(&self) -> Option<usize> {
        let bag = Cubes {
            red: 12,
            green: 13,
            blue: 14,
        };
        if self.turns.iter().all(|c| bag.contains(c)) {
            return Some(self.id);
        }
        None
    }
    fn required_cubes(&self) -> Cubes {
        self.turns
            .iter()
            .fold(Cubes::new(), |acc: Cubes, c: &Cubes| acc.max(c))
    }
}

impl From<&str> for Game {
    fn from(s: &str) -> Self {
        let (game, turns) = s
            .split_once(": ")
            .expect("invalid input format: missing ':' between game and turn");
        let id: usize = game
            .split_whitespace()
            .find_map(|s| s.parse().ok())
            .expect("invalid input format: missing game id");
        let turns: Vec<Cubes> = turns.split("; ").map(Cubes::from).collect();
        return Self { id, turns };
    }
}

fn parse_input(input: &'static str) -> Vec<Game> {
    input.lines().map(Game::from).collect()
}

fn question1(input: &'static str) -> usize {
    parse_input(input)
        .into_iter()
        .filter_map(|g| g.valid_id())
        .sum()
}

fn question2(input: &'static str) -> usize {
    parse_input(input)
        .into_iter()
        .map(|g| g.required_cubes().power())
        .sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 8)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 2286)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 2006)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 84911)
    }
}
