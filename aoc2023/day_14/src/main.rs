use std::collections::HashMap;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Rock {
    Empty,
    Square,
    Round,
}

type Platform = Vec<Vec<Rock>>;

impl From<char> for Rock {
    fn from(c: char) -> Self {
        match c {
            'O' => Rock::Round,
            '#' => Rock::Square,
            '.' => Rock::Empty,
            _ => panic!("invalid characted input {}", c),
        }
    }
}

fn parse_input(input: &'static str) -> Platform {
    input
        .lines()
        .map(|l| l.chars().map(Rock::from).collect())
        .collect()
}

fn tilt_north(platform: &mut Platform) {
    let cols = platform[0].len();
    let rows = platform.len();
    for col in 0..cols {
        let mut free = 0;
        for row in 0..rows {
            match platform[row][col] {
                Rock::Square => free = row + 1,
                Rock::Round => {
                    (platform[free][col], platform[row][col]) =
                        (platform[row][col], platform[free][col]);
                    free += 1;
                }
                Rock::Empty => {}
            }
        }
    }
}

fn tilt_south(platform: &mut Platform) {
    let cols = platform[0].len();
    let rows = platform.len();
    for col in 0..cols {
        let mut free = rows - 1;
        for row in (0..rows).rev() {
            match platform[row][col] {
                Rock::Square => free = if row > 0 { row - 1 } else { 0 },
                Rock::Round => {
                    (platform[free][col], platform[row][col]) =
                        (platform[row][col], platform[free][col]);
                    free = if free == 0 { 0 } else { free - 1 };
                }
                Rock::Empty => {}
            }
        }
    }
}

fn tilt_west(platform: &mut Platform) {
    let cols = platform[0].len();
    let rows = platform.len();
    for row in 0..rows {
        let mut free = 0;
        for col in 0..cols {
            match platform[row][col] {
                Rock::Square => free = col + 1,
                Rock::Round => {
                    platform[row].swap(free, col);
                    free += 1;
                }
                Rock::Empty => {}
            }
        }
    }
}

fn tilt_east(platform: &mut Platform) {
    let cols = platform[0].len();
    let rows = platform.len();
    for row in 0..rows {
        let mut free = cols - 1;
        for col in (0..cols).rev() {
            match platform[row][col] {
                Rock::Square => free = if col > 0 { col - 1 } else { 0 },
                Rock::Round => {
                    platform[row].swap(free, col);
                    free = if free == 0 { 0 } else { free - 1 };
                }
                Rock::Empty => {}
            }
        }
    }
}

fn cycle_platform(platform: &mut Platform) {
    tilt_north(platform);
    tilt_west(platform);
    tilt_south(platform);
    tilt_east(platform)
}

fn question1(input: &'static str) -> usize {
    let platform = parse_input(input);
    let mut res = 0;
    let max_load = platform.len();
    let n_cols = platform[0].len();
    for col in 0..n_cols {
        let mut w = max_load;
        for i in 0..platform.len() {
            match platform[i][col] {
                Rock::Square => w = max_load - i - 1,
                Rock::Round => {
                    res = res + w;
                    w -= 1
                }
                Rock::Empty => {}
            }
        }
    }
    res
}

fn question2(input: &'static str) -> usize {
    let mut platform = parse_input(input);
    let mut dp: HashMap<Platform, usize> = HashMap::new();
    let n = 1_000_000_000;
    for i in 0..n {
        let prev = platform.clone();
        if dp.contains_key(&platform) {
            // loop found, exit early
            let cycle_start = dp[&platform];
            let cycle_lenght = i - cycle_start;
            let steps = (n - cycle_start) % cycle_lenght;
            for _ in 0..steps {
                cycle_platform(&mut platform);
            }
            break;
        }
        cycle_platform(&mut platform);
        dp.insert(prev.clone(), i);
    }
    platform
        .iter()
        .rev()
        .enumerate()
        .map(|(w, r)| (w + 1) * r.iter().filter(|&&e| e == Rock::Round).count())
        .sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test.txt");
        assert_eq!(question1(input), 136)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test.txt");
        assert_eq!(question2(input), 64)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 105982)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 85175)
    }
}
