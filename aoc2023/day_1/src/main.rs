fn calibration_value(line: &str) -> usize {
    let first = line
        .bytes()
        .find_map(byte_to_digit)
        .expect("No numbers found in patter");
    let last = line
        .bytes()
        .rev()
        .find_map(byte_to_digit)
        .expect("No numbers found in patter");
    first * 10 + last
}

fn match_digit_str(input: &str) -> Option<usize> {
    let c = input.bytes().take(1).find_map(byte_to_digit);
    if c.is_some() {
        return c;
    }
    [
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
    ]
    .into_iter()
    .zip(1..)
    .find_map(|(k, v)| if input.starts_with(k) { Some(v) } else { None })
}

fn byte_to_digit(c: u8) -> Option<usize> {
    if b'0' < c && c <= b'9' {
        return Some(usize::from(c - b'0'));
    }
    None
}

fn parse_line(line: &str) -> usize {
    let first = (0..line.len())
        .find_map(|i| match_digit_str(&line[i..]))
        .expect("No numbers found in pattern");
    let last = (0..line.len())
        .rev()
        .find_map(|i| match_digit_str(&line[i..]))
        .expect("No numbers found in pattern");
    first * 10 + last
}

fn question1(input: &'static str) -> usize {
    input.lines().map(calibration_value).sum()
}

fn question2(input: &'static str) -> usize {
    input.lines().map(parse_line).sum()
}

fn main() {
    let input = include_str!("../input.txt");
    println!("{:?}", question1(input));
    println!("{:?}", question2(input));
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example1() {
        let input = include_str!("../test1.txt");
        assert_eq!(question1(input), 142)
    }

    #[test]
    fn test_example2() {
        let input = include_str!("../test2.txt");
        assert_eq!(question2(input), 281)
    }
    #[test]
    fn test_question1() {
        let input = include_str!("../input.txt");
        assert_eq!(question1(input), 56042)
    }

    #[test]
    fn test_question2() {
        let input = include_str!("../input.txt");
        assert_eq!(question2(input), 55358)
    }
}
